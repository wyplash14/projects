package Animation;

import javax.swing.*;
import java.awt.*;


public class AnimatedHearth extends JPanel {
    private Image img;

    public AnimatedHearth() {

        ImageIcon imageIcon = new ImageIcon("Resources/hearth_animation.gif");
        img = imageIcon.getImage();
        setOpaque(false);
        setBounds(0, 0, 400, 500);
        img.getScaledInstance(20, 20, Image.SCALE_DEFAULT);

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, this);
    }

}
