package Animation;

import Audio.AudioPlayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class AnimatedDog extends JPanel {

    private Image img;

    public AnimatedDog() {

        ImageIcon imageIcon = new ImageIcon("Resources/dog_animation.gif");

        setOpaque(false);
        setBounds(0, 450, 400, 500);
        imageIcon.setImage(imageIcon.getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT));
        img = imageIcon.getImage();

        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                if ((75 < x) && (x < 135) && (y > 30) && (y < 170)) {

                    new AudioPlayer(2).start(0);
                }

            }
        });

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, this);
    }
}
