package Animation;

import javax.swing.*;
import java.awt.*;


public class AnimatedGraveYard extends JPanel {
    private Image img;

    public AnimatedGraveYard() {

        ImageIcon imageIcon = new ImageIcon("Resources/skeleton.gif");
        img = imageIcon.getImage();
        setOpaque(false);
        setBounds(100, 100, 400, 500);
        imageIcon.setImage(imageIcon.getImage().getScaledInstance(250, 250, Image.SCALE_DEFAULT));
        img = imageIcon.getImage();

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, this);
    }

}
