package Audio;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class AudioPlayer {
    private Clip clip;

    public AudioPlayer(int a) {

        if (a == 1) {
            initiateBacgroundMusic();
        }
        if (a == 2) {
            initiateBark();
        }
        if (a == 3) {
            initiateMoney();
        }
        if (a == 4) {
            initiateSlurp();
        }
        if (a == 5) {
            initiateDead();
        }
        if (a == 6) {
            initiateStartGame();
        }

    }

    private void initiateBacgroundMusic() {
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File("Resources/base_music.wav").getAbsoluteFile());

        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }

        clip = null;
        try {
            clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            assert clip != null;
            clip.open(audioInputStream);
        } catch (LineUnavailableException | IOException e) {
            e.printStackTrace();
        }

    }

    private void initiateBark() {
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File("Resources/dog_woof.wav").getAbsoluteFile());

        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }

        clip = null;
        try {
            clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            assert clip != null;
            clip.open(audioInputStream);
        } catch (LineUnavailableException | IOException e) {
            e.printStackTrace();
        }

    }

    private void initiateMoney() {
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File("Resources/money_sound.wav").getAbsoluteFile());

        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }

        clip = null;
        try {
            clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            assert clip != null;
            clip.open(audioInputStream);
        } catch (LineUnavailableException | IOException e) {
            e.printStackTrace();
        }
    }

    private void initiateSlurp() {
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File("Resources/slurp_audio.wav").getAbsoluteFile());

        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }

        clip = null;
        try {
            clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            assert clip != null;
            clip.open(audioInputStream);
        } catch (LineUnavailableException | IOException e) {
            e.printStackTrace();
        }
    }

    private void initiateDead() {
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File("Resources/dead_sound.wav").getAbsoluteFile());

        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }

        clip = null;
        try {
            clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            assert clip != null;
            clip.open(audioInputStream);
        } catch (LineUnavailableException | IOException e) {
            e.printStackTrace();
        }

    }


    private void initiateStartGame() {
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File("Resources/start_sound.wav").getAbsoluteFile());

        } catch (UnsupportedAudioFileException | IOException e) {
            e.printStackTrace();
        }

        clip = null;
        try {
            clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            assert clip != null;
            clip.open(audioInputStream);
        } catch (LineUnavailableException | IOException e) {
            e.printStackTrace();
        }

    }


    public void start(int a) {
        if (a == 1) {
            clip.loop(clip.LOOP_CONTINUOUSLY);
        }
        clip.start();

    }


    public void stop() {
        clip.stop();
    }

    public void end() {
        clip.close();
    }

}
