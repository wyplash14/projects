package Core;

import Audio.AudioPlayer;
import Person.Person;
import Person.PersonData;
import View.DeadPanel;
import View.StartPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileWriter;
import java.io.IOException;


public class GameRunner extends JFrame {
    private DeadPanel deadPanel;
    private PersonData personData;
    private int GamePaused;

    public GameRunner() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle("My Life");
        setResizable(false);
        CardLayout cardLayout = new CardLayout();
        JPanel cardPanel = new JPanel(); //fontos
        cardPanel.setLayout(cardLayout);
        add(cardPanel);
        setBounds(500, 50, 500, 700);


        JButton restartBtn = new JButton("BACK TO MENU");
        restartBtn.setBackground(Color.WHITE);
        restartBtn.setForeground(Color.BLACK);
        restartBtn.setFont(new Font("serif", Font.BOLD, 15));
        StartPanel startPanel = new StartPanel(cardLayout, cardPanel);
        cardPanel.add(startPanel, "P3");
        cardPanel.add(new DeadPanel(restartBtn), "P2");


        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {

                Person person = startPanel.getPerson();
                try {
                    FileWriter myWriter = new FileWriter("person_data.txt", false);
                    myWriter.write(person.getPersonData().getCurrentAge() + "\n" + person.getPersonData().getMoney() + "\n" + person.getPersonData().getHunger() + " \n" + person.getPersonData().getMaxAge());
                    myWriter.close();
                } catch (IOException exception) {
                    System.out.println("An error occurred.");
                    exception.printStackTrace();
                }

                System.exit(0);
            }
        });


        restartBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AudioPlayer(6).start(1);
                cardLayout.show(cardPanel, "P3");
            }
        });


        setVisible(true);

    }

}
