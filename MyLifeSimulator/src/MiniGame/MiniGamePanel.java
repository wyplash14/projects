package MiniGame;

import Person.HungerController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;


public class MiniGamePanel extends JPanel {
    private final JLabel scoreTxt;
    private int x;
    private int y, xo;
    private MiniGameController miniGameController;
    private int score;
    private Thread thread;
    private int ok;
    private final HungerController hungerController;

    public MiniGamePanel(HungerController hungerController) {
        this.hungerController = hungerController;

        setBounds(70, 200, 350, 350);
        setBackground(Color.BLACK);
        setLayout(null);
        x = 200;
        score = 0;

        JPanel scorePanel = new JPanel();
        scorePanel.setBounds(250, 0, 100, 50);
        scorePanel.setOpaque(false);
        scoreTxt = new JLabel("Score: " + score);
        scoreTxt.setFont(new Font("Serif", Font.BOLD, 10));
        scoreTxt.setForeground(Color.WHITE);
        scorePanel.add(scoreTxt);

        add(scorePanel);


        InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = getActionMap();

        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escape");
        am.put("escape", new AbstractAction() {
            public void actionPerformed(ActionEvent evt) {
                hungerController.setHunger(hungerController.getHunger() - 2 * score);
                score = 0;
                scoreTxt.setText("Score: " + score);
                setVisible(false);
                thread.interrupt();


            }
        });


        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                x = e.getX();
                if (x > 248) x = 248;
                repaint();
            }
        });

        startNewMiniGame();
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.ORANGE);
        g.drawRect(x, 300, 100, 10);


        if (miniGameController.gety() == 320 && miniGameController.getxo() >= x && miniGameController.getxo() <= x + 100 && miniGameController.getOk() == 0) {
            score++;
            scoreTxt.setText("Score: " + score);
            miniGameController.setOk(1);

        }

        if (miniGameController.getOk() == 1) {
            g.setColor(Color.GREEN);
        } else {
            g.setColor(Color.RED);
        }
        g.drawOval(miniGameController.getxo(), miniGameController.gety(), 20, 20);

    }

    public void startNewMiniGame() {
        miniGameController = new MiniGameController(this, ok, hungerController);
        thread = new Thread(miniGameController);
        thread.start();
    }

    public int getScore() {
        return score;
    }

    public void setScore(int a) {
        score = a;
        scoreTxt.setText("Score: " + score);
    }

}