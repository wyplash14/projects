package MiniGame;

import javax.swing.*;

import java.awt.*;

import static java.lang.Thread.sleep;

public class DelayController implements Runnable {
    private int delayState;
    private final JButton miniGameBtn;

    public DelayController(int delayState, JButton miniGameBtn) {
        this.delayState = delayState;
        this.miniGameBtn = miniGameBtn;
    }

    @Override
    public void run() {
        delayState = 1;
        miniGameBtn.setBackground(Color.RED);
        try {
            sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        delayState = 0;
        miniGameBtn.setBackground(new Color(30, 94, 19));
    }

    public int getDelay() {
        return delayState;
    }
}
