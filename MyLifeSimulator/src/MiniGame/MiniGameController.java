package MiniGame;

import Person.HungerController;

import java.util.Random;

public class MiniGameController implements Runnable {
    private final MiniGamePanel miniGamePanel;
    private final HungerController hungerController;
    private int xo;
    private int y;
    private int ok;

    public MiniGameController(MiniGamePanel miniGamePanel, int ok, HungerController hungerController) {
        this.miniGamePanel = miniGamePanel;
        this.hungerController = hungerController;
        this.ok = ok;

    }

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 1; i <= random.nextInt(20) + 1; i++) {
            xo = random.nextInt(310) + 20;
            ok = 0;
            for (y = 1; y <= 350; y++) {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    System.out.println("Mini game interrupted");
                }
                miniGamePanel.repaint();
            }
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        hungerController.setHunger(hungerController.getHunger() - 2 * miniGamePanel.getScore());
        miniGamePanel.setScore(0);
        miniGamePanel.setVisible(false);

    }

    public int getxo() {
        return xo;
    }

    public int gety() {
        return y;
    }

    public int getOk() {
        return ok;
    }

    public void setOk(int a) {
        ok = a;
    }

}
