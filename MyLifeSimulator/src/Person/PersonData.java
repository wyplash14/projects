package Person;


public class PersonData {
    private int currentAge;
    private int hunger;
    private int money;
    private int maxAge;

    public PersonData(int currentAge, int money, int hunger, int maxAge) {
        this.currentAge = currentAge;
        this.hunger = hunger;
        this.money = money;
        this.maxAge = maxAge;

    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int a) {
        maxAge = a;
    }

    public int getCurrentAge() {
        return currentAge;
    }

    public int getHunger() {
        return hunger;
    }

    public int getMoney() {
        return money;
    }

    public void setCurrentAge(int currentAge) {
        this.currentAge = currentAge;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public void setMoney(int money) {
        this.money = money;
    }


}
