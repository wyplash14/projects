package Person;

import Audio.AudioPlayer;
import View.AgeView;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class PersonController implements Runnable {
    private final Person person;
    private final AgeView ageView;
    private final JPanel cardPanel;
    private final CardLayout layout;
    private final AudioPlayer audioPlayer;

    public PersonController(Person person, AgeView ageView, JPanel cardPanel, CardLayout layout, AudioPlayer audioPlayer) {
        this.layout = layout;
        this.person = person;
        this.ageView = ageView;
        this.cardPanel = cardPanel;
        this.audioPlayer = audioPlayer;

    }

    @Override
    public void run() {
        JLabel txtage = new JLabel("Age:" + person.getPersonData().getCurrentAge());

        int start_age = person.getPersonData().getCurrentAge();
        System.out.println(person.getPersonData().getMaxAge());
        for (int i = start_age; i < person.getPersonData().getMaxAge(); i++) {

            PersonData personData = person.getPersonData();
            personData.setCurrentAge(personData.getCurrentAge() + 1);
            person.setPersonData(personData);
            ageView.setTxtage(txtage);
            person.SetPersonImage(personData.getCurrentAge());
            try {
                Thread.sleep(50000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        audioPlayer.end();
        Random random = new Random();

        try {
            FileWriter myWriter = new FileWriter("person_data.txt", false);
            myWriter.write(0 + "\n" + 0 + "\n" + 0 + "\n" + random.nextInt(101));
            myWriter.close();

        } catch (IOException exception) {
            System.out.println("An error occurred.");
            exception.printStackTrace();
        }
        new AudioPlayer(5).start(0);
        layout.show(cardPanel, "P2");

    }

}
