package Person;

import Audio.AudioPlayer;
import View.HungerView;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;

public class HungerController implements Runnable {
    private final HungerView hungerView;
    private final JPanel cardPanel;
    private final CardLayout layout;
    private int hunger;
    private JLabel txtMoney;
    private int money;
    private final Person person;
    private int gamePaused;
    private final AudioPlayer audioPlayer;

    public HungerController(Person person, HungerView hungerView, JButton feedBtn, JPanel cardPanel, CardLayout layout, AudioPlayer audioPlayer) {
        this.person = person;
        this.hungerView = hungerView;
        this.cardPanel = cardPanel;
        this.layout = layout;
        this.hunger = person.getPersonData().getHunger();
        this.audioPlayer = audioPlayer;
        gamePaused = 0;
    }

    @Override
    public void run() {
        int max_Hunger = 100;

        while (hunger < max_Hunger) {
            System.out.println(gamePaused);
            if (gamePaused == 0) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                hungerView.setSlide(hunger);
                hungerView.repaint();

                hunger++;
                PersonData personData;
                personData = person.getPersonData();
                personData.setHunger(hunger);
                person.setPersonData(personData);
            }
        }

        try {
            FileWriter myWriter = new FileWriter("person_data.txt", false);
            myWriter.write(person.getPersonData().getCurrentAge() + "\n" + person.getPersonData().getMoney() + "\n" + person.getPersonData().getHunger() + " \n" + person.getPersonData().getMaxAge());
            myWriter.close();
        } catch (IOException exception) {
            System.out.println("An error occurred.");
            exception.printStackTrace();
        }
        audioPlayer.end();
        layout.show(cardPanel, "P2");

    }


    public int getHunger() {
        return hunger;
    }

    public void setHunger(int a) {
        hunger = a;
    }

    public int getGamePaused() {
        return gamePaused;
    }

    public void setGamePaused(int gamePaused) {
        this.gamePaused = gamePaused;
    }
}
