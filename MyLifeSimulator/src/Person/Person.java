package Person;

import javax.swing.*;

public class Person extends JButton {
    private PersonData personData;

    public Person(PersonData personData) {
        this.personData = personData;
        SetPersonImage(0);

    }

    public void SetPersonImage(int age) {
        ImageIcon personIcon;
        if (age < 14) {
            personIcon = new ImageIcon("Resources/child.png");
        } else if (age < 26) {
            personIcon = new ImageIcon("Resources/youngster.png");
        } else if (age < 60) {
            personIcon = new ImageIcon("Resources/adult.png");
        } else personIcon = new ImageIcon("Resources/old.png");
        setFocusPainted(false);
        setContentAreaFilled(false);
        setOpaque(false);
        setBorder(null);
        setIcon(personIcon);
    }


    public PersonData getPersonData() {
        return personData;
    }

    public void setPersonData(PersonData personData) {
        this.personData = personData;
    }
}
