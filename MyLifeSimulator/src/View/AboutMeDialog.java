package View;

import javax.swing.*;
import java.awt.*;

public class AboutMeDialog extends JDialog {

    public AboutMeDialog() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setBounds(400, 300, 500, 500);
        getContentPane().setBackground(Color.BLACK);

        JTextArea textArea = new JTextArea();

        textArea.setBounds(100, 100, 300, 100);
        textArea.setBackground(Color.BLACK);
        textArea.setForeground(Color.WHITE);
        textArea.setFont(new Font("serif", Font.BOLD, 15));
        textArea.setEditable(false);

        textArea.setText("This game was created by David Lazar\n" +
                "Date: 2020-2021\n" +
                "Student: Babes-Bolyai University\n" +
                "...");


        add(textArea);
        setVisible(true);
    }
}
