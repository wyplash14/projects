package View;

import javax.swing.*;
import java.awt.*;

public class ControlsDialog extends JDialog {

    public ControlsDialog() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setBounds(400, 300, 500, 500);
        setLayout(null);
        getContentPane().setBackground(Color.black);


        JLabel title = new JLabel("GAME CONTROLS");
        title.setFont(new Font("serif", Font.BOLD, 30));
        title.setForeground(Color.WHITE);
        title.setBackground(Color.BLACK);
        title.setBounds(100, 0, 300, 50);


        JTextArea textArea = new JTextArea();

        textArea.setBounds(100, 100, 300, 100);
        textArea.setBackground(Color.BLACK);
        textArea.setForeground(Color.WHITE);
        textArea.setFont(new Font("serif", Font.BOLD, 18));
        textArea.setEditable(false);

        textArea.setText("S-Start game\n" +
                "P-Pause game\n" +
                "Left click on character- +1$\n");


        JTextArea textArea1 = new JTextArea();
        textArea1.setBounds(10, 300, 500, 400);
        textArea1.setBackground(Color.BLACK);
        textArea1.setForeground(Color.WHITE);
        textArea1.setFont(new Font("serif", Font.BOLD, 12));
        textArea1.setEditable(false);

        textArea1.setText("Infos:\n" +
                "*The Character can possibly die in every second permanently\n" +
                "*Click on Feed-to get more health\n" +
                "*You have to click minimum 5 times on Character to unlock Feed($5)\n" +
                "*If the Hunger bar (the red bar) runs out the Character dies too\n" +
                "*If you pause the game the hunger will stop moving,but the Character age will still grow");


        add(textArea);
        add(textArea1);
        add(title);

        setVisible(true);
    }
}
