package View;

import Animation.AnimatedGraveYard;

import javax.swing.*;
import java.awt.*;

public class DeadPanel extends JPanel {

    public DeadPanel(JButton restartBtn) {
        setSize(500, 700);
        setBackground(Color.BLACK);
        setLayout(null);

        JPanel diedpanel = new JPanel();
        diedpanel.setBounds(0, 40, 500, 50);
        JLabel txtdead = new JLabel("YOUR CHARACTER UNFORTUNATELY DIED");
        txtdead.setFont(new Font("serif", Font.BOLD, 20));
        txtdead.setForeground(Color.BLACK);
        diedpanel.add(txtdead);

        JPanel btnpanel = new JPanel();
        btnpanel.setBounds(140, 600, 200, 50);
        btnpanel.setOpaque(false);
        btnpanel.add(restartBtn);

        add(btnpanel);
        add(new AnimatedGraveYard());
        add(diedpanel);


    }


}
