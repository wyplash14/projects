package View;

import Animation.AnimatedHearth;
import Audio.AudioPlayer;
import Person.Person;
import Person.PersonData;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class StartPanel extends JPanel {
    private final Person person;


    public StartPanel(CardLayout layout, JPanel panel) {
        setSize(500, 700);
        setBackground(Color.BLACK);

        setLayout(null);

        JPanel starttxtpanel = new JPanel();
        starttxtpanel.setBounds(0, 40, 500, 50);
        JLabel txtstart = new JLabel("HELLO IN MY LIFE SIMULATOR");
        txtstart.setFont(new Font("serif", Font.BOLD, 25));
        txtstart.setForeground(Color.BLACK);
        starttxtpanel.add(txtstart);

        JPanel spacetxtpanel = new JPanel();
        spacetxtpanel.setBounds(0, 80, 500, 50);
        JLabel spacestart = new JLabel("press S to begin");
        spacestart.setFont(new Font("serif", Font.BOLD, 25));
        spacestart.setForeground(Color.BLACK);
        spacetxtpanel.add(spacestart);


        JButton conntrolsBtn = new JButton("CONTROLS");
        JButton infoBtn = new JButton("ABOUT ME");
        conntrolsBtn.setBackground(Color.WHITE);
        conntrolsBtn.setForeground(Color.BLACK);
        conntrolsBtn.setFont(new Font("serif", Font.BOLD, 15));
        conntrolsBtn.setBounds(140, 500, 200, 50);
        infoBtn.setBounds(140, 570, 200, 50);
        infoBtn.setBackground(Color.WHITE);
        infoBtn.setForeground(Color.BLACK);
        infoBtn.setFont(new Font("serif", Font.BOLD, 15));

        AudioPlayer audioPlayer = new AudioPlayer(6);

        audioPlayer.start(1);


        person = new Person(readPersonDateFromFile());
        System.out.println(person.getPersonData().getCurrentAge() + " " + person.getPersonData().getMaxAge());

        InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = getActionMap();

        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, 0), "s");
        am.put("s", new AbstractAction() {
            public void actionPerformed(ActionEvent evt) {
                audioPlayer.end();
                if ((person.getPersonData().getHunger() >= 100) || (person.getPersonData().getMaxAge() <= person.getPersonData().getCurrentAge())) {
                    Random random = new Random();
                    PersonData personData = new PersonData(0, 0, 0, random.nextInt(101));
                    person.setPersonData(personData);
                }
                panel.add(new GamePanel(person, panel, layout), "P1");
                layout.show(panel, "P1");
            }
        });


        conntrolsBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                new ControlsDialog();
            }
        });
        infoBtn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                new AboutMeDialog();
            }
        });


        add(new AnimatedHearth());
        add(conntrolsBtn);
        add(infoBtn);
        add(spacetxtpanel);
        add(starttxtpanel);
    }


    private PersonData readPersonDateFromFile() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("person_data.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int[] tall = new int[100];
        int i = 0;
        while (true) {
            assert scanner != null;
            if (!scanner.hasNextInt()) break;
            tall[i++] = scanner.nextInt();
        }
        if ((tall[0] == tall[3]) || (tall[2] == 100)) {
            tall[0] = 0;
            tall[1] = 0;
            tall[2] = 0;
            Random random = new Random();
            tall[3] = random.nextInt(101);
        }

        return new PersonData(tall[0], tall[1], tall[2], tall[3]);

    }

    public Person getPerson() {
        return person;
    }

}
