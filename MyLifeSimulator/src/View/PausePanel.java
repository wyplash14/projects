package View;

import Audio.AudioPlayer;
import Person.Person;
import Person.HungerController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class PausePanel extends JPanel {

    private Person person;


    public PausePanel(JPanel panel, CardLayout layout, HungerController hungerController, AudioPlayer audioPlayer) {
        setSize(500, 700);
        setBackground(Color.BLACK);


        JPanel pausetxtpanel = new JPanel();
        pausetxtpanel.setBounds(0, 200, 500, 50);
        JLabel txtstart = new JLabel("GAME PAUSED");
        txtstart.setFont(new Font("serif", Font.BOLD, 25));
        txtstart.setForeground(Color.BLACK);
        pausetxtpanel.add(txtstart);

        JPanel spacetxtpanel = new JPanel();
        spacetxtpanel.setBounds(0, 250, 500, 50);
        JLabel spacestart = new JLabel("press SPACE to continue");
        spacestart.setFont(new Font("serif", Font.BOLD, 25));
        spacestart.setForeground(Color.BLACK);
        spacetxtpanel.add(spacestart);

        InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = getActionMap();

        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "space");
        am.put("space", new AbstractAction() {
            public void actionPerformed(ActionEvent evt) {
                audioPlayer.start(1);
                hungerController.setGamePaused(0);
                System.out.println("Start :" + hungerController.getGamePaused());
                layout.show(panel, "P1");
            }
        });

        add(new SleepyDog());
        add(spacetxtpanel);
        add(pausetxtpanel);

        setLayout(null);
    }


    public static class SleepyDog extends JPanel {

        private final Image img;

        public SleepyDog() {

            ImageIcon imageIcon = new ImageIcon("Resources/sleepy_dog.gif");

            setOpaque(false);
            setBounds(0, 25, 400, 500);
            imageIcon.setImage(imageIcon.getImage().getScaledInstance(350, 200, Image.SCALE_DEFAULT));
            img = imageIcon.getImage();

        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(img, 0, 0, this);
        }
    }

}
