package View;

import Person.Person;

import javax.swing.*;
import java.awt.*;

public class AgeView extends JPanel {
    private final Person person;

    public AgeView(Person person) {
        this.person = person;
    }

    public void setTxtage(JLabel txtage) {
        txtage.setText("Age:" + person.getPersonData().getCurrentAge());
        txtage.setForeground(new Color(224, 30, 0));
        txtage.setFont(new Font("serif", Font.BOLD, 60));
        setOpaque(false);
        setBounds(130, 580, 200, 200);
        add(txtage);
    }
}
