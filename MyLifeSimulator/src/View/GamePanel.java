package View;

import Animation.AnimatedDog;
import Audio.AudioPlayer;
import MiniGame.DelayController;
import MiniGame.MiniGamePanel;
import Person.Person;
import Person.PersonData;
import Person.PersonController;
import Person.HungerController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;


public class GamePanel extends JPanel {
    private PausePanel pausePanel;


    public GamePanel(Person person, JPanel cardPanel, CardLayout layout) {


        setSize(500, 700);
        setLayout(null);

        AudioPlayer audioPlayer = new AudioPlayer(1);
        audioPlayer.start(1);

        ImageIcon bgIcon = new ImageIcon("Resources/room.jpg");
        JLabel bgLabel = new JLabel(bgIcon);
        bgLabel.setSize(500, 700);


        PersonData personData = person.getPersonData();


        JLabel txtmoney = new JLabel("Money: " + person.getPersonData().getMoney() + " $");
        txtmoney.setFont(new Font("serif", Font.BOLD, 20));
        txtmoney.setForeground(new Color(30, 94, 19));


        JLabel txtAge = new JLabel();
        AgeView ageView = new AgeView(person);
        HungerView hungerView = new HungerView();

        JButton feedBtn = new JButton("Feed(5$)");
        if (person.getPersonData().getMoney() >= 5) feedBtn.setBackground(new Color(30, 94, 19));
        else feedBtn.setBackground(Color.RED);
        JButton miniGameBtn = new JButton("Feed");


        PersonController personController = new PersonController(person, ageView, cardPanel, layout, audioPlayer);
        new Thread(personController).start();
        HungerController hungerController = new HungerController(person, hungerView, feedBtn, cardPanel, layout, audioPlayer);
        new Thread(hungerController).start();


        feedBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (hungerController.getHunger() >= 5 && personData.getMoney() >= 5) {
                    new AudioPlayer(4).start(0);
                    hungerController.setHunger(hungerController.getHunger() - 5);
                    personData.setMoney(personData.getMoney() - 5);
                    person.setPersonData(personData);
                    txtmoney.setText("Money: " + personData.getMoney() + " $");
                    if (person.getPersonData().getMoney() < 5) {
                        feedBtn.setBackground(Color.RED);
                    }
                }
            }
        });

        InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = getActionMap();

        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_P, 0), "space");
        am.put("space", new AbstractAction() {
            public void actionPerformed(ActionEvent evt) {
                hungerController.setGamePaused(1);
                System.out.println("Paused" + hungerController.getGamePaused());
                cardPanel.add(new PausePanel(cardPanel, layout, hungerController, audioPlayer), "P4");
                ;
                layout.show(cardPanel, "P4");
                audioPlayer.stop();
            }
        });


        JPanel statPanel = new JPanel();
        statPanel.setOpaque(false);
        statPanel.setBounds(0, 20, 200, 70);
        statPanel.setLayout(new FlowLayout());
        statPanel.add(txtmoney);


        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setOpaque(false);
        buttonsPanel.setBounds(0, 100, 100, 400);
        buttonsPanel.setLayout(new FlowLayout());
        buttonsPanel.add(feedBtn);
        buttonsPanel.add(miniGameBtn);


        JPanel personPanel = new JPanel();
        personPanel.setOpaque(false);
        personPanel.setBounds(200, 200, 200, 400);
        personPanel.add(person);


        MiniGamePanel miniGamePanel = new MiniGamePanel(hungerController);
        miniGamePanel.setVisible(false);
        DelayController delayController = new DelayController(0, miniGameBtn);

        miniGameBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (delayController.getDelay() == 0) {
                    miniGamePanel.setVisible(true);
                    miniGamePanel.startNewMiniGame();
                    new Thread(delayController).start();
                }

            }
        });


        add(hungerView);
        add(miniGamePanel);
        add(buttonsPanel);
        add(new AnimatedDog());
        add(statPanel);
        add(ageView);
        add(personPanel);
        add(bgLabel);

        person.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (person.getPersonData().getMoney() < 100) {
                    new AudioPlayer(3).start(0);
                    personData.setMoney(personData.getMoney() + 1);
                    person.setPersonData(personData);
                    txtmoney.setText("Money: " + personData.getMoney() + " $");
                    if (person.getPersonData().getMoney() == 5) {
                        feedBtn.setBackground(new Color(30, 94, 19));
                    }
                }
            }
        });

        setVisible(true);
    }


}
