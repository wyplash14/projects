import asyncio
from gtts import gTTS
import discord
from discord.ext import commands
from discord import FFmpegPCMAudio
import urllib3
from keep_alive import keep_alive
import os
import random

urllib3.disable_warnings()


def welcome_message(user, num):
    if num == 1:
        return "Helló " + user

    return "Elment " + user


client = commands.Bot(command_prefix='$')
language = 'hu'

nameDictionary = {
    "Wyplash": "Lázár",
    "xenon19": "Wolf",
    "Neghon": "Rihi",
    "Zabu": "Zabu",
    "Senki": "Tamás",
    "Sidius": "Zabu",
    "vwg51": "Norbi",
    "Christopher Gros": "Ádám"
}


@client.event
async def on_ready():
    print('Bot is logged.')


@client.command()
async def hello(ctx):
    await ctx.send("Hello.")




@client.event
async def on_voice_state_update(ctx, before, after):
    if not before.channel == after.channel and not ctx.name == 'Welcome Bot':
        print(before.channel)
        print(after.channel)
        if after.channel is None:
            channel = before.channel
            num = 2
        else:
            num = 1
            channel = after.channel
        voice = discord.utils.get(client.voice_clients, guild=ctx.guild)
        if voice and voice.is_connected():
            await voice.move_to(channel)
        else:
            if not ctx.name in nameDictionary.keys():
                if ctx.name == 'Rythm':
                    name = "Zene indul!"
                    if num == 2:
                        name = "Zene leáll!"
                else:
                    name = "Ó ki ez te drága jó isten?"
                    if num == 2:
                        name = "Elment az ismeretlen."
                output = gTTS(text=name, lang=language, slow=False)
            else:
                name = nameDictionary[ctx.name]
                output = gTTS(text=welcome_message(name, num), lang=language, slow=False)
        output.save("output.mp3")
        voice = await channel.connect()
        voice.play(discord.FFmpegPCMAudio(source="output.mp3"))
        await asyncio.sleep(3)
        await voice.disconnect()


keep_alive()
client.run(os.getenv('TOKEN'))
