let street;
let telephone;

async function collapse(event) {
  const response = await fetch(`/showMoreDetail?id=${event.target.id}`);
  const body = await response.json();
  console.log(body);
  if (response.status < 400) {
    street = `Street:${body.restaurant.Street} Nr: ${body.restaurant.Number}`;
    telephone = `Telephone: ${body.restaurant.TelNumber}`;
    document.getElementById('street').textContent = street;
    document.getElementById('tel').textContent = telephone;
  } else {
    // eslint-disable-next-line no-alert
    alert(body.ErrMessage);
  }
}

async function deleteReservation(event) {
  // eslint-disable-next-line no-restricted-globals
  if (confirm('Are you sure you want to delete?')) {
    try {
      console.log(`/deleteReservation/${event.target.parentNode.id}`);
      await fetch(`/deleteReservation/${event.target.parentNode.id}`, {
        method: 'DELETE',
      });
      event.target.parentNode.remove();
    } catch (err) {
      // eslint-disable-next-line no-alert
      alert(err.message);
    }
  }
}

Array.from(document.getElementsByClassName('deleteButton')).forEach((button) => button.addEventListener('click', deleteReservation));

Array.from(document.getElementsByClassName('restaurant')).forEach((box) => box.addEventListener('click', collapse));

// eslint-disable-next-line no-unused-vars
async function logout() {
  await fetch('/logout');
  document.location.href = '/';
}

// eslint-disable-next-line no-unused-vars
function switchToLogin() {
  document.getElementById('loginForm').style.display = 'inline-block';
  document.getElementById('registerForm').style.display = 'none';
}

// eslint-disable-next-line no-unused-vars
function searchRestaurants() {
  const text = document.getElementById('searchbar').value.toLowerCase();
  Array.from(document.getElementsByClassName('restaurant')).forEach((box) => {
    if (!box.id.toLowerCase().includes(text)) {
      const actual = document.getElementById(box.id);
      actual.style.display = 'none';
    } else {
      const actual = document.getElementById(box.id);
      actual.style.display = 'inline-block';
    }
  });
}

// eslint-disable-next-line no-unused-vars
function filterByTime() {
  const startTime = document.getElementById('opentime').value;
  const closeTime = document.getElementById('closetime').value;
  if (startTime.length !== 0 && closeTime.length !== 0) {
    Array.from(document.getElementsByClassName('restaurant')).forEach((box) => {
      const minOpen = box.childNodes[5].innerHTML.substr(10, 15);
      const maxClose = box.childNodes[7].innerHTML.substr(10, 15);
      if (minOpen > startTime || maxClose < closeTime) {
        const actual = document.getElementById(box.id);
        actual.style.display = 'none';
      }
    });
  }
}

// eslint-disable-next-line no-unused-vars
function undoByTime() {
  console.log('mi van itt:');
  Array.from(document.getElementsByClassName('restaurant')).forEach((box) => {
    const actual = document.getElementById(box.id);
    actual.style.display = 'inline-block';
  });
}

async function deleteImage(event) {
  // eslint-disable-next-line no-restricted-globals
  if (confirm('Are you sure you want to delete this image?')) {
    try {
      console.log(`/deleteImage/${event.target.parentNode.parentNode.id}`);
      await fetch(`/deleteImage/${event.target.parentNode.parentNode.id}`, {
        method: 'DELETE',
      });
      event.target.parentNode.parentNode.remove();
    } catch (err) {
      // eslint-disable-next-line no-alert
      alert(err.message);
    }
  }
}

Array.from(document.getElementsByClassName('imageBtn')).forEach((button) => button.addEventListener('click', deleteImage));

// eslint-disable-next-line no-unused-vars
async function insertAcceptedReservation(target) {
  try {
    await fetch(`/updateStatus/${target.parentNode.id}/1`, {
      method: 'PUT',
    });
    target.parentNode.remove();
  } catch (err) {
    // eslint-disable-next-line no-alert
    alert(err.message);
  }
}

// eslint-disable-next-line no-unused-vars
async function insertDeclineReservation(target) {
  try {
    await fetch(`/updateStatus/${target.parentNode.id}/2`, {
      method: 'PUT',
    });
    target.parentNode.remove();
  } catch (err) {
    // eslint-disable-next-line no-alert
    alert(err.message);
  }
}

function divToOwnerObject() {
  // bepakoljuk az owner listaba a user informaciokat
  const allInfo = document.getElementById('pInf');
  let oszt = 1;
  const owner = [];
  Array.from(allInfo.childNodes).forEach((child) => {
    if (child.nodeName === 'P') {
      if (oszt % 2 === 0 && oszt !== 6) {
        owner.push(child.innerText);
        console.log(child.innerText);
      }
      oszt += 1;
    }
  });
  return owner;
}

function updateFrontEndProfile(owner) {
  // az ownerbe levo valtozasokkal felulirjuk az eddigieket
  const allInfo = document.getElementById('pInf');
  let oszt = 1;
  let i = 0;
  Array.from(allInfo.childNodes).forEach((child) => {
    if (child.nodeName === 'P') {
      if (oszt % 2 === 0 && oszt !== 6) {
        const ch = child;
        ch.innerText = owner[i];
        i += 1;
      }
      oszt += 1;
    }
  });
}

// eslint-disable-next-line no-unused-vars
async function updateUserInfo() {
  document.getElementById('profileupdate').style.display = 'inline-block';
  const owner = divToOwnerObject();
  const form = document.getElementById('profileForm');
  const inputs = Array.from(form.querySelectorAll('input:not([type="submit"])'));
  inputs.forEach((input) => {
    const inp = input;
    inp.value = owner[inputs.indexOf(input)];
  });
  inputs.forEach((input) => input.addEventListener('change', () => { owner[inputs.indexOf(input)] = input.value; })); // ha valtozas van kicsereljuk az ownerbe
  form.querySelector('input[type="submit"]').addEventListener('click', async (submitEvent) => {
    submitEvent.preventDefault();
    console.log(owner);
    try {
      const owner2 = {
        firstname: owner[0], lastname: owner[1], email: owner[2], phone: owner[3],
        // objektumot csinalunk, igy konnyebb hasznalni back-enden
      };
      const resp = await fetch('/updateProfileInformation', {
        method: 'PUT',
        body: JSON.stringify(owner2),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      console.log(resp);
      if (resp.status < 400) { // ha hiba tortent az mind 400 feletti kod
        updateFrontEndProfile(owner);
        document.getElementById('profileupdate').style.display = 'none';
      } else {
        document.getElementById('errInformations').style.display = 'inline-block';
      }
    } catch (err) {
      // eslint-disable-next-line no-alert
      alert('Sikertelen update');
    }
  });
}
