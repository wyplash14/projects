import express from 'express';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import { join } from 'path';
import render from './routes/render.js';
import authRouter from './routes/authrouter.js';
import { decodeToken } from './middleware/token.js';

const app = express();
const port = 5252;

app.use(cookieParser());

app.use(decodeToken);

app.use(express.static(join(process.cwd(),
  'static')));

app.use(express.static(join(process.cwd(),
  'temp_images')));

app.use(express.urlencoded({ extended: true }));
app.use(morgan('tiny'));
app.set('view engine', 'ejs');

app.use('/', authRouter);
app.use('/', render);

app.listen(port, () => {
  console.log('Server is ON.');
  console.log(`Listenin on port ${port}`);
});
