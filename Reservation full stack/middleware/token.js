import jwt from 'jsonwebtoken';
import secret from '../secret/secret.js';

export function  decodeToken(req, resp, next) {
  const r = resp;
  if (req.cookies.token) {
    try {
      const tokenObject = jwt.verify(req.cookies.token, secret);
      r.locals.tokenObject = tokenObject;
    } catch (err) {
      resp.clearCookie('token');
    }
  }
  next();
}

export function checkToken(req, resp, next) {
  if (resp.locals.tokenObject) {
    next();
  } else {
    resp.redirect('login');
  }
}
