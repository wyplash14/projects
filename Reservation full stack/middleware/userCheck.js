import {
  getOwnerIdByImageId, findIdByName, findCostumerByReservationID, findOwnerByReservationID,
} from '../db/db.js';

// ebben a middlewareban azt ellenorizzuk, hogy aki akarja
// modositan az adott elemet az aze is a felhasznaloe

export async function checkImageDelete(req, resp, next) {
  try {
    const { tokenObject } = resp.locals;
    const [ownerOfRest, loggedUserId] = await
    Promise.all([getOwnerIdByImageId(req.params.id), findIdByName(tokenObject.username)]);
    if (ownerOfRest[0].owner === loggedUserId[0].CostumerID) {
      // osszehasonlitja a bejelentkezett felhasznalo es az etterem tuljadonos id-jat akie a foto
      next();
    } else {
      resp.status(403);
    }
  } catch (err) {
    resp.status(500).send('(500) Cant\'t get the owner ID-s');
    console.error(`ERROR:${err}`);
  }
}

export async function checkReservationDelete(req, resp, next) {
  try {
    const { tokenObject } = resp.locals;
    const [costumerId, loggedUserId] = await
    Promise.all([findCostumerByReservationID(req.params.id), findIdByName(tokenObject.username)]);
    if (costumerId[0].CostumerID === loggedUserId[0].CostumerID || tokenObject.role === 'admin') {
      // osszehasonlitja a felhasznalo id-jat akie a foglalas a bejelentkezett felhasznalo id-javal
      next();
    } else {
      resp.status(403);
    }
  } catch (err) {
    resp.status(500).send('(500) Cant\'t get the costumer of this reservation');
    console.error(`ERROR:${err}`);
  }
}

export async function checkReservationSim(req, resp, next) {
  // megnezzuk hogy az az ettrem tulajdonos akihez erkezett a foglalas
  // megeggyezik-e azzal aki be van jelentkezve
  try {
    const { tokenObject } = resp.locals;
    const [costumerId, loggedUserId] = await
    Promise.all([findOwnerByReservationID(req.params.id), findIdByName(tokenObject.username)]);
    if (costumerId[0].Owner === loggedUserId[0].CostumerID) {
      next();
    } else {
      resp.status(403);
    }
  } catch (err) {
    resp.status(500).send('(500) Cant\'t get the owner of this reservation');
    console.error(`ERROR:${err}`);
  }
}
