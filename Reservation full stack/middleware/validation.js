import { response } from 'express';
import {
  findRestaurantsByID, findImagesByRestaurantID, findAllRestaurants, isThisUserExists,
} from '../db/db.js';

export function addRestMiddleware(req, resp, next) {
  // ellenorizzuk az ettermek bemeneti adatainak helyesseget
  if (!/^[A-Z][a-z]+$/.test(req.body.restaurant_name)) {
    resp.status(401).render('addrestaurant', { message: 'The restaurant name should contain only letters' });
  } else if (!/^[A-Z][a-z]+( [A-Z][a-z]+)?$/.test(req.body.city)) {
    resp.status(401).render('addrestaurant', { message: 'The city name is invalid.Please give a valid city' });
  } else if (!/^[A-Z][a-z]+( [A-Z][a-z]+)?$/.test(req.body.street)) {
    resp.status(401).render('addrestaurant', { message: 'The street name is invalid.Please give a valid street' });
  } else if (!/^[0-9]+$/.test(req.body.telnumber) || req.body.telnumber.length === '10') {
    resp.status(401).render('addrestaurant', { message: 'The telephone number is invalid' });
  } else {
    next();
  }
}

export async function addImageUploadMiddleware(req, resp, next) {
  try {
    const [restaurants, restaurantImages] = await Promise.all([findRestaurantsByID(
      { id: req.fields.restaurantID },
    ),
    findImagesByRestaurantID({ id: req.fields.restaurantID })]);
    const restaurant = restaurants[0];
    if (!/^image\/.+$/.test(req.files.fileToUpload.type)) {
      resp.render('uploadimg', {
        restaurant,
        restaurantImages,
        message: 'Please select an image file', // hellenorizzuk hogy a kivalasztott file kep-e
      });
    } else {
      next();
    }
  } catch (err) {
    response.send(err);
  }
}

export async function addCreateReservationMiddleware(req, resp, next) {
  const restaurants = await findAllRestaurants();
  const d = new Date();
  const dt = d.toISOString().substr(0, 10);
  console.log(dt, req.body.date);
  console.log(req.body);
  if (req.body.reservationTime < restaurants[req.body.restaurantID - 1].Open // nyitva van-e
   || req.body.reservationTime > restaurants[req.body.restaurantID - 1].Close) {
    resp.redirect(301, '/reservation?ok=0');
  } else if (dt > req.body.date) { // megfelelo-e a datum
    resp.redirect(301, '/reservation?ok=2');
  } else if (req.body.reservationTime <= d.getHours().toString() && dt === req.body.date) {
    // megfelelo-e az ido
    resp.redirect(301, '/reservation?ok=3');
  } else {
    next();
  }
}

export async function addRegisterMiddleware(req, resp, next) {
  const o = req.query.owner;
  // regisztracios adatok ellenorzzese
  try {
    const res = await isThisUserExists(req.body.username);
    if (res.length !== 0) {
      console.log(res.length);
      resp.status(401).render('login', { message: 'This username is already exists', owner: o });
    } else if (!/^[A-Z][a-z]+$/.test(req.body.firstname)) {
      resp.status(401).render('login', { message: 'The firstname should contain only letters', owner: o });
    } else if (!/^[A-Z][a-z]+$/.test(req.body.lastname)) {
      resp.status(401).render('login', { message: 'The lastname should contain only letters', owner: o });
    } else if (!/^[0-9]+$/.test(req.body.phone) || req.body.phone.length === '10') {
      resp.status(401).render('login', { message: 'The telephone number is invalid', owner: o });
    } else if (req.body.password !== req.body.passwordagain) {
      resp.status(401).render('login', { message: 'The passwords doesn\'t match.', owner: o });
    } else {
      next();
    }
  } catch (err) {
    console.log(err);
    resp.status(500).render('login', { message: 'Server problem, can\'t register this user now.Try again later.', owner: o });
  }
}

export async function checkValidityProfile(req, resp, next) {
  // update pofil bemeneti adatok ellenorzese
  if (!/^[A-Z][a-z]+$/.test(req.body.firstname)) {
    resp.sendStatus(401);
  } else if (!/^[A-Z][a-z]+$/.test(req.body.lastname)) {
    resp.sendStatus(401);
  } else if (!/^[0-9]+$/.test(req.body.phone) || req.body.phone.length === '10') {
    resp.sendStatus(401);
  } else if (!/^[a-zA-z0-9._]+@gmail\.com$|^[a-zA-Z0-9]+@yahoo\.com$/.test(req.body.email)) {
    resp.sendStatus(401);
  } else {
    next();
  }
}
