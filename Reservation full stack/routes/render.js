import { Router, json } from 'express';
import { join } from 'path';
import eformidable from 'express-formidable';
import { existsSync, mkdirSync } from 'fs';
import {
  findAllRestaurants, insertRestaurants, insertReservations, findRestaurantsByID,
  findImagesByRestaurantID,
  insertImage,
  findAllReservations,
  deleteReservation,
  findIdByName,
  findOwnerByRestID,
  deleteImage,
  getCostumerInfoByUsername,
  getRestaurantsByUsername,
  getReservationsByUsername,
  getReservationsOfOwnersRestaurants,
  updateReservationStatus,
  updateProfile,
} from '../db/db.js';
import  {
  addCreateReservationMiddleware, addImageUploadMiddleware, addRestMiddleware, checkValidityProfile,
} from '../middleware/validation.js';
import { checkImageDelete, checkReservationDelete, checkReservationSim } from '../middleware/userCheck.js';
import { checkToken }  from '../middleware/token.js';

const router = Router();

const uploadDir = join(process.cwd(), 'temp_images');
if (!existsSync(uploadDir)) {
  mkdirSync(uploadDir);
}

router.get(['/', '/index'], async (req, resp) => {
  try {
    resp.render('index', {  });
  } catch (err) {
    console.log(`ERROR: ${err}`);
    resp.status(500);
    resp.send('Cannot load restaurants from database!');
  }
});

router.post('/addrest', checkToken, addRestMiddleware, async (req, resp) => {
  try {
    const { tokenObject } = resp.locals;
    const nameID = await findIdByName(tokenObject.username);
    await insertRestaurants(req.body, nameID[0].CostumerID);
    resp.redirect('/');
  } catch (err) {
    console.log(`ERROR: ${err}`);
    resp.status(500).render('addrestaurant', { message: 'Cannot insert restaurant in database!(500)' });
  }
});

router.get('/addrestaurant', checkToken, (req, resp) => {
  resp.render('addrestaurant', { message: '' });
});

router.get('/reservation', checkToken, async (req, resp) => {
  try {
    const { tokenObject } = resp.locals;
    const [restaurants, reservations, nameID] = await Promise.all([findAllRestaurants(),
      findAllReservations(), findIdByName(tokenObject.username)]);
    let succ = '';
    const name = nameID[0].CostumerID;
    if (req.query.ok === '1') {
      succ = 'Reservation was made succesfully';
    } else if (req.query.ok === '0') succ = 'Reservation error--->Restaurant is closed at the given time.';
    else if (req.query.ok === '-1') succ = 'Reservation error(Could not make the reservation.)';
    else if (req.query.ok === '2') succ = 'Please give an upcomming date';
    else if (req.query.ok === '3') succ = 'Please give an upcomming hour';

    resp.render('reservation', {
      restaurants, reservations, success: succ, nameID: name,
    });
  } catch (err) {
    console.log(`ERROR: ${err}`);
    resp.status(500);
    resp.send('Cannot load restauran ID in reservation form!');
  }
});

router.post('/createreservation', addCreateReservationMiddleware, async (req, resp) => {
  try {
    const { tokenObject } = resp.locals;
    const nameID = await findIdByName(tokenObject.username);
    await insertReservations(req.body, nameID[0].CostumerID);
    resp.redirect(301, '/reservation?ok=1');
  } catch (err) {
    console.log(`ERROR: ${err}`);
    resp.status(500);
    resp.redirect(500, '/reservation?ok=-1');
  }
});

router.get('/restaurant', checkToken, async (req, resp) => {
  try {
    const restId = req.query;
    const { tokenObject } = resp.locals;
    const [restaurants, restaurantImages, tokenID] = await Promise.all([findRestaurantsByID(restId),
      findImagesByRestaurantID(restId), findIdByName(tokenObject.username)]);
    const restaurant = restaurants[0];
    const ownerID = tokenID[0].CostumerID;
    resp.render('uploadimg',  {
      restaurant, restaurantImages, ownerID, message: '',
    });
  } catch (err) {
    console.log(`ERROR: ${err}`);
    resp.status(500);
  }
});

router.delete('/deleteReservation/:id', checkToken, checkReservationDelete, async (req, resp) => {
  try {
    await deleteReservation(req.params.id);
    resp.status(204).send();
  } catch (err) {
    resp.status(500).send('Cannot delete reservation!');
    console.error(`ERROR:${err}`);
  }
});

router.delete('/deleteImage/:id', checkToken, checkImageDelete, async (req, resp) => {
  try {
    await deleteImage(req.params.id);
    resp.status(204).send();
  } catch (err) {
    resp.status(500).send('Cannot delete reservation!');
    console.error(`ERROR:${err}`);
  }
});

router.get('/showMoreDetail', async (req, res) => {
  const restId = req.query;
  try {
    const restaurants = await findRestaurantsByID(restId);
    const restaurant = restaurants[0];
    res.send({ restaurant });
  } catch (err) {
    res.status(500).send({ ErrMessage: err });
  }
});

router.get('/userprofile', checkToken, async (req, resp) => {
  const { tokenObject } = resp.locals;
  try {
    const [costumerInfo, costumerRestaurants, costumerReservations, ownersReservationPandig] = await
    Promise.all([
      getCostumerInfoByUsername(tokenObject.username),
      getRestaurantsByUsername(tokenObject.username),
      getReservationsByUsername(tokenObject.username),
      getReservationsOfOwnersRestaurants(tokenObject.username),
    ]);
    const costumer = costumerInfo[0];
    resp.render('userprofile', {
      costumer, costumerRestaurants, costumerReservations, ownersReservationPandig,
    });
  } catch (err) {
    console.log(`ERROR: ${err}`);
    resp.status(500);
  }
});

router.put('/updateStatus/:id/:status', checkToken, checkReservationSim, async (req, resp) => {
  try {
    await updateReservationStatus(req.params.id, req.params.status);
    resp.sendStatus(204);
  } catch (err) {
    console.log(`ERROR: ${err}`);
    resp.sendStatus(500);
  }
});

router.put('/updateProfileInformation', checkToken, json(), checkValidityProfile, async (req, resp) => {
  try {
    const { tokenObject } = resp.locals;
    await updateProfile(req.body, tokenObject.username);
    resp.sendStatus(204);
  } catch (err) {
    console.log(`ERR: ${err}`);
    resp.sendStatus(500);
  }
});

router.use(eformidable({ uploadDir }));

router.post('/upload', checkToken, addImageUploadMiddleware, async (req, res) => {
  try {
    const { tokenObject } = res.locals;
    const img = req.fields;
    const path = req.files.fileToUpload.path.split('\\');
    img.img = path[path.length - 1];
    const [restOwnerID, tOwnerID] = await
    Promise.all([findOwnerByRestID(req.fields.restaurantID), findIdByName(tokenObject.username)]);
    if (restOwnerID[0].Owner === tOwnerID[0].CostumerID) {
      await insertImage(img);
    }
    const [restaurants, restaurantImages] = await Promise.all([findRestaurantsByID(
      { id: req.fields.restaurantID },
    ),
    findImagesByRestaurantID({ id: req.fields.restaurantID })]);
    const restaurant = restaurants[0];
    const ownerID = tOwnerID[0].CostumerID;
    res.render('uploadimg', {
      restaurant, restaurantImages, ownerID, message: 'Image uploaded successfully.',
    });
  } catch (err) {
    console.log(`ERROR: ${err}`);
    res.status(500);
  }
});

export default router;
