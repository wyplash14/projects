import { Router } from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import secret from '../secret/secret.js';
import { isThisUserExists, insertNewUser } from '../db/db.js';
import { addRegisterMiddleware } from '../middleware/validation.js';

const router = Router();

router.get('/logout', (req, resp) => {
  resp.clearCookie('token');
  resp.redirect('/');
});

router.get('/login', async (req, resp) => {
  if (req.query.owner === '1') {
    resp.render('login', { message: ' ', owner: '1' }); // ha owner 1 akkorawner ha nem akkor csak felhasznalo
  } else {
    resp.render('login', { message: ' ', owner: '0' });
  }
});

router.post('/loginu', async (req, resp) => {
  const o = req.query.owner;
  try {
    const res = await isThisUserExists(req.body.username); // letezik-e a user
    if (res.length === 0) {
      console.log(res.length);
      return resp.status(401).render('login', { message: 'Incorrect username or password.', owner: o });
    } if (!await bcrypt.compare(req.body.password, res[0].Password)) {
      // megnezzuk hogy egyezznek-e az enkriptalt jelszavak
      return resp.status(401).render('login', { message: 'Incorrect username or password.', owner: o });
    }
    const token = jwt.sign({
      name: res[0].Firstname,
      role: res[0].Role,
      username: req.body.username,
    }, secret, {
      expiresIn: '30m',
    });
    resp.cookie('token', token, {
      httpOnly: true,
      sameSite: 'strict',
    });
    resp.redirect('/');
  } catch (err) {
    console.log(err);
    return resp.status(500).render('login', { message: 'Can\'t login server problem.', owner: o });
  }
  return 0;
});

router.post('/register', addRegisterMiddleware, async (req, resp) => {
  let role = 'user';
  try {
    const pw = bcrypt.hashSync(req.body.password, 10); // enkriptaljuk a jeszot
    if (req.query.owner === '1') {
      role = 'owner';
    }
    await insertNewUser(req.body, pw, role);
    const token = jwt.sign({
      name: req.body.firstname,
      role: req.body.role,
      username: req.body.username,
    }, secret, {
      expiresIn: '30m',
    });
    resp.cookie('token', token, {
      httpOnly: true,
      sameSite: 'strict',
    });
    resp.redirect('/');
  } catch (err) {
    console.log(err);
    resp.status(500).send('Error in login');
  }
});

export default router;
