// Adatbázis műveleteket végző modul

import mysql from 'mysql';
import util from 'util';
import bcrypt from 'bcrypt';

// Létrehozunk egy connection poolt
const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'someone',
  password: 'webprog',
});

const queryPromise = util.promisify(pool.query).bind(pool);

export function createRestaurantsTable() {
  return queryPromise(`CREATE TABLE IF NOT EXISTS restaurants (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Name varchar(50),
    Location varchar(30),
    Street varchar(30),
    Number INT,
    TelNumber varchar(15),
    Open varchar(10),
    Close varchar(10),
    Owner INT
                    );`);
}

export function createCostumersTable() {
  return queryPromise(`CREATE TABLE IF NOT EXISTS costumers (
    CostumerID INT AUTO_INCREMENT PRIMARY KEY,
    Firstname VARCHAR(30),
    Lastname VARCHAR(30),
    Username VARCHAR(30),
    Password VARCHAR(400),
    Email VARCHAR(100),
    Phone VARCHAR(20),
    Role VARCHAR(30)
    );`);
}

export function createImagesTable() {
  return queryPromise(`CREATE TABLE IF NOT EXISTS images (
    ImageID INT AUTO_INCREMENT PRIMARY KEY,
    ImageName varchar(50),
    RestaurantID INT,
    FOREIGN KEY (RestaurantID) REFERENCES restaurants(ID) );`);
}

export function createReservationsTable() {
  return queryPromise(`CREATE TABLE IF NOT EXISTS reservations (
    ReservationID INT AUTO_INCREMENT PRIMARY KEY,
    RestaurantID INT,
    CostumerID INT,
    Date varchar(15),
    Hour varchar(5),
    State INT,
    --FOREIGN KEY (RestaurantID) REFERENCES restaurants(ID),
    --FOREIGN KEY (CostumerID) REFERENCES costumers(CostumerID) );`);
}

export function insertRestaurants(req, nameID) {
  return queryPromise('INSERT INTO restaurants(Name,Location,Street,Number,TelNumber,Open,Close,Owner) VALUES (?,?,?,?,?,?,?,?)',
    [req.restaurant_name, req.city, req.street, req.number, req.telnumber,
      req.openTime, req.closeTime, nameID]);
}

export function setupCostumers() {
  const pw1 = bcrypt.hashSync('David', 10);
  const pw2 = bcrypt.hashSync('Ervin', 10);
  const pw3 = bcrypt.hashSync('Sandor', 10);
  const pw4 = bcrypt.hashSync('Zsuzsa', 10);
  return queryPromise('INSERT INTO costumers(Firstname,Lastname, Username ,Password ,Email,Phone, Role) VALUES (\'Lazar\', \'David\', \'Lazar\', ?,\'wyplash14@gmail.com\',\'0742586259\' ,\'admin\'),(\'Nagy\',\'Ervin\', \'Nagy\', ?,\'ervinnagy@gmail.com\',\'0742524559\', \'owner\'),(\'Kiraly\', \'Sandor\',\'Kiraly\', ? ,\'sanyiakiraly@gmail.com\',\'0722365259\', \'owner\'),(\'Makk\',\' Zsuzsa\',\'Makk\',?,\'zsuzsika@gmail.com\',\'07152366759\',\'owner\')', [pw1, pw2, pw3, pw4]);
}

export function insertImage(req) {
  return queryPromise('INSERT INTO images(ImageName,RestaurantID) VALUES ( ?, ?)',
    [req.img, req.restaurantID]);
}

export function insertReservations(req, nameID) {
  console.log(nameID);
  return queryPromise('INSERT INTO reservations(RestaurantID,CostumerID,Date,Hour,State) VALUES (?, ?, ?, ?, 0)',
    [req.restaurantID, nameID, req.date, req.reservationTime]);
}

export function findAllRestaurants() {
  return queryPromise('SELECT * FROM restaurants');
}

export function findAllReservations() {
  return queryPromise('SELECT * FROM reservations');
}

export function findRestaurantsByID(req) {
  return queryPromise('SELECT * FROM restaurants WHERE ID = ?', [req.id]);
}

export function findImagesByRestaurantID(req) {
  return queryPromise('SELECT * FROM images WHERE RestaurantID = ?', [req.id]);
}

export function findAllCostumers() {
  return queryPromise('SELECT * FROM costumers');
}

export function findIdByName(username) {
  return queryPromise('SELECT CostumerID FROM costumers WHERE username LIKE ?', [username]);
}

export function findOwnerByRestID(restID) {
  return queryPromise('SELECT Owner FROM restaurants WHERE ID = ?', [restID]);
}

export function findCostumerByReservationID(resID) {
  return queryPromise('SELECT CostumerID FROM reservations WHERE reservationID = ?', [resID]);
}

export function findOwnerByReservationID(resID) {
  return queryPromise(`SELECT r.Owner FROM reservations
   Join restaurants as r on r.ID=reservations.RestaurantID WHERE reservationID = ?`, [resID]);
}

export function deleteReservation(id) {
  return queryPromise('DELETE FROM reservations WHERE reservationID = ?', [id]);
}

export function deleteImage(id) {
  return queryPromise('DELETE FROM images WHERE ImageID = ?', [id]);
}
export function isThisUserExists(username) {
  console.log(username);
  return queryPromise('SELECT Firstname, Password,Role FROM costumers WHERE Username LIKE ?', [username]);
}

export function insertNewUser(req, pw, role) {
  return queryPromise('INSERT INTO COSTUMERS(Firstname,Lastname, Username,Password,Email,Phone,Role) VALUES(?,?,?,?,?,?,?)', [req.firstname, req.lastname, req.username, pw, req.email, req.phone, role]);
}

export function getCostumerInfoByUsername(username) {
  return queryPromise('SELECT * FROM costumers WHERE Username LIKE ?', [username]);
}

export function getRestaurantsByUsername(username) {
  return queryPromise('SELECT * FROM restaurants JOIN costumers ON restaurants.Owner=costumers.CostumerID WHERE costumers.Username LIKE ?', [username]);
}

export function updateReservationStatus(id, status) {
  return queryPromise('UPDATE reservations SET State= ? WHERE ReservationID=?', [status, id]);
}

export function updateProfile(req, username) {
  return queryPromise('UPDATE costumers SET firstname= ?, lastname= ?, email= ?, phone= ? WHERE username LIKE ? ', [req.firstname, req.lastname, req.email, req.phone, username]);
}

export function getReservationsByUsername(username) {
  return queryPromise(`SELECT * FROM reservations
   JOIN costumers ON reservations.CostumerID=costumers.CostumerID
   JOIN restaurants ON reservations.RestaurantID=restaurants.ID
   WHERE costumers.Username LIKE ?`, [username]);
}

export function getOwnerIdByImageId(imageID) {
  return queryPromise(`SELECT owner FROM restaurants
    JOIN images as i on i.RestaurantID=restaurants.ID
    WHERE imageID = ?`, [imageID]);
}

export function getReservationsOfOwnersRestaurants(username) {
  return queryPromise(`SELECT reservations.ReservationID,cos.Firstname,cos.Lastname ,cos.Phone,reservations.Date,reservations.Hour,r.Name from reservations
  join restaurants as r on r.ID=reservations.restaurantID
  join costumers as c on c.CostumerID=r.Owner
  join costumers as cos on cos.CostumerID=reservations.CostumerID
  where c.Username LIKE ? and c.Role='owner' and reservations.State=0`, [username]);
}

/* (async () => {
  try {
    await createCostumersTable();
    await setupCostumers();
  } catch (err) {
    console.log(`Error tortent: ${err}`);
  }
})(); */
