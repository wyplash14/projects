CREATE DATABASE IF NOT EXISTS webprog;

USE webprog;
CREATE USER IF NOT EXISTS 'someone'@'localhost' IDENTIFIED WITH mysql_native_password BY 'webprog';
GRANT ALL PRIVILEGES ON *.* TO
'someone'@'localhost';

CREATE TABLE IF NOT EXISTS restaurants (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Name varchar(50),
    Location varchar(30),
    Street varchar(30),
    Number INT,
    TelNumber varchar(15),
    Open varchar(10),
    Close varchar(10)
                    );
				        
    
CREATE TABLE IF NOT EXISTS images (
    ImageID INT AUTO_INCREMENT PRIMARY KEY,
    ImageName varchar(50),
    RestaurantID INT,
    FOREIGN KEY (RestaurantID) REFERENCES restaurants(ID) );
    
    
CREATE TABLE IF NOT EXISTS reservations (
    ReservationID INT AUTO_INCREMENT PRIMARY KEY,
    RestaurantID INT,
    CostumerID INT,
    Date varchar(15),
    Hour varchar(5),
    FOREIGN KEY (RestaurantID) REFERENCES restaurants(ID),
    FOREIGN KEY (CostumerID) REFERENCES costumers(CostumerID) );
    

