/********************************************************************************
** Form generated from reading UI file 'SideWidget.ui'
**
** Created by: Qt User Interface Compiler version 6.0.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIDEWIDGET_H
#define UI_SIDEWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SideWidget
{
public:
    QGroupBox *groupBox;
    QTabWidget *tabWidget;
    QWidget *tab;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QSlider *rotate_x_slider;
    QLabel *label_2;
    QSlider *rotate_y_slider;
    QLabel *label_3;
    QSlider *rotate_z_slider;
    QLabel *label_4;
    QDoubleSpinBox *zoom_factor_spin_box;
    QLabel *label_5;
    QDoubleSpinBox *trans_x_spin_box;
    QLabel *label_6;
    QDoubleSpinBox *trans_y_spin_box;
    QLabel *label_7;
    QDoubleSpinBox *trans_z_spin_box;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout;
    QLabel *label_12;
    QSlider *curve_slider;
    QLabel *label_8;
    QSlider *rotate_x_slider_2;
    QLabel *label_9;
    QSlider *rotate_y_slider_2;
    QLabel *label_10;
    QSlider *rotate_z_slider_2;
    QLabel *label_11;
    QDoubleSpinBox *zoom_factor_spin_box_2;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QLabel *label_13;
    QDoubleSpinBox *vector_length_spin_box;
    QWidget *tab_3;
    QWidget *layoutWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label_14;
    QSlider *rotate_x_slider_3;
    QLabel *label_15;
    QSlider *rotate_y_slider_3;
    QLabel *label_16;
    QSlider *rotate_z_slider_3;
    QLabel *label_17;
    QDoubleSpinBox *zoom_factor_spin_box_3;
    QLabel *label_18;
    QDoubleSpinBox *trans_x_spin_box_2;
    QLabel *label_19;
    QDoubleSpinBox *trans_y_spin_box_2;
    QLabel *label_20;
    QDoubleSpinBox *trans_z_spin_box_2;
    QWidget *tab_4;
    QLabel *label_21;
    QSlider *curve_slider_2;
    QLabel *label_22;
    QDoubleSpinBox *zoom_factor_spin_box_4;
    QCheckBox *checkBox_3;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QSlider *rotate_x_slider_4;
    QSlider *rotate_y_slider_4;
    QSlider *rotate_z_slider_4;
    QSlider *texture_slider;
    QWidget *tab_5;
    QWidget *layoutWidget_3;
    QFormLayout *formLayout_3;
    QLabel *label_26;
    QSlider *rotate_x_slider_5;
    QLabel *label_27;
    QSlider *rotate_y_slider_5;
    QLabel *label_28;
    QSlider *rotate_z_slider_5;
    QLabel *label_29;
    QDoubleSpinBox *zoom_factor_spin_box_5;
    QLabel *label_30;
    QDoubleSpinBox *trans_x_spin_box_3;
    QLabel *label_31;
    QDoubleSpinBox *trans_y_spin_box_3;
    QLabel *label_32;
    QDoubleSpinBox *trans_z_spin_box_3;
    QLabel *label_33;
    QDoubleSpinBox *point_index_spin_box;
    QLabel *label_34;
    QDoubleSpinBox *point_index_spin_box_x;
    QLabel *label_35;
    QDoubleSpinBox *point_index_spin_box_y;
    QLabel *label_36;
    QDoubleSpinBox *point_index_spin_box_z;
    QWidget *tab_6;
    QToolBox *toolBox_2;
    QWidget *page_4;
    QGroupBox *groupBox_2;
    QWidget *layoutWidget_7;
    QFormLayout *formLayout_7;
    QLabel *label_73;
    QSlider *rotate_x_slider_11;
    QLabel *label_74;
    QSlider *rotate_y_slider_11;
    QLabel *label_75;
    QSlider *rotate_z_slider_11;
    QLabel *label_76;
    QDoubleSpinBox *zoom_factor_spin_box_11;
    QLabel *label_81;
    QSlider *horizontalSlider_x;
    QLabel *label_82;
    QSlider *horizontalSlider_y;
    QLabel *label_83;
    QSlider *horizontalSlider_z;
    QLabel *label_103;
    QDoubleSpinBox *vectorSpinBox;
    QCheckBox *checkBox_6;
    QCheckBox *checkBox_5;
    QCheckBox *checkBox_move_all_arc;
    QWidget *layoutWidget_12;
    QFormLayout *formLayout_12;
    QDoubleSpinBox *vectorSpinBox_cp_x;
    QLabel *label_125;
    QDoubleSpinBox *vectorSpinBox_cp_y;
    QLabel *label_126;
    QDoubleSpinBox *vectorSpinBox_cp_z;
    QDoubleSpinBox *trans_x_spin_box_10;
    QLabel *label_124;
    QDoubleSpinBox *trans_y_spin_box_10;
    QDoubleSpinBox *trans_z_spin_box_10;
    QLabel *label_139;
    QLabel *label_140;
    QLabel *label_141;
    QWidget *page_5;
    QComboBox *comboBox_join;
    QSpinBox *spinBox_arc_index_1;
    QSpinBox *spinBox_arc_index_2;
    QComboBox *comboBox_direction;
    QPushButton *pushButton_submit;
    QLabel *label_123;
    QLabel *label_127;
    QLabel *label_80;
    QDoubleSpinBox *point_index_spin_box_3;
    QLabel *label_112;
    QPushButton *pushButton;
    QSpinBox *spinBox_arc_index;
    QPushButton *pushButton_reset;
    QWidget *tab_7;
    QGroupBox *groupBox_3;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout_4;
    QLabel *label_136;
    QSpinBox *spinBox_patch_index_1;
    QLabel *label_137;
    QSpinBox *spinBox_patch_index_2;
    QComboBox *comboBox_join_2;
    QPushButton *pushButton_submit_2;
    QComboBox *comboBox_direction_2;
    QComboBox *comboBox_direction_extend;
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout_5;
    QLabel *label_119;
    QSpinBox *spinBox_patch_index;
    QLabel *label_84;
    QSpinBox *spinBox_patch_point_index;
    QSpinBox *spinBox_vectorNumber;
    QLabel *label_113;
    QToolBox *toolBox_3;
    QWidget *page;
    QGroupBox *groupBox_4;
    QWidget *layoutWidget_13;
    QFormLayout *formLayout_13;
    QLabel *label_77;
    QSlider *rotate_x_slider_14;
    QLabel *label_78;
    QSlider *rotate_y_slider_14;
    QLabel *label_79;
    QSlider *rotate_z_slider_14;
    QLabel *label_128;
    QDoubleSpinBox *zoom_factor_spin_box_14;
    QLabel *label_129;
    QSlider *horizontalSlider_x_3;
    QLabel *label_130;
    QSlider *horizontalSlider_y_3;
    QLabel *label_131;
    QSlider *horizontalSlider_z_3;
    QLabel *label_132;
    QDoubleSpinBox *vectorSpinBox_pach;
    QLabel *label_133;
    QDoubleSpinBox *vectorSpinBox_cp_x_2;
    QLabel *label_134;
    QDoubleSpinBox *vectorSpinBox_cp_y_2;
    QLabel *label_135;
    QDoubleSpinBox *vectorSpinBox_cp_z_2;
    QLabel *label_88;
    QDoubleSpinBox *trans_x_spin_box_8;
    QLabel *label_89;
    QDoubleSpinBox *trans_y_spin_box_8;
    QLabel *label_90;
    QDoubleSpinBox *trans_z_spin_box_8;
    QLabel *label_95;
    QComboBox *comboBox_material;
    QCheckBox *checkBox_d0u;
    QCheckBox *checkBox_d0v;
    QCheckBox *checkBox_d1u;
    QCheckBox *checkBox_d1v;
    QPushButton *pushButton_reset_2;
    QPushButton *pushButton_newPatch;
    QCheckBox *checkBox_vectors;
    QCheckBox *checkBox_move_all;
    QCheckBox *checkBox_d2u;
    QCheckBox *checkBox_d2v;
    QWidget *page_3;
    QDoubleSpinBox *scale_factor_spinbox;
    QComboBox *shader_combobox;
    QLabel *label_85;
    QDoubleSpinBox *smoothing_spinbox;
    QDoubleSpinBox *b_spinbox;
    QDoubleSpinBox *shading_spinbox;
    QDoubleSpinBox *r_spinbox;
    QDoubleSpinBox *a_spinbox;
    QLabel *label_86;
    QCheckBox *alpha_on_checkbox;
    QLabel *label_87;
    QLabel *label_91;
    QLabel *label_92;
    QLabel *label_93;
    QDoubleSpinBox *g_spinbox;
    QLabel *label_94;
    QLabel *label_122;
    QLabel *label_138;
    QWidget *tab_8;
    QWidget *layoutWidget_9;
    QFormLayout *formLayout_9;
    QLabel *label_96;
    QSlider *rotate_x_slider_13;
    QLabel *label_97;
    QSlider *rotate_y_slider_13;
    QLabel *label_98;
    QSlider *rotate_z_slider_13;
    QLabel *label_99;
    QDoubleSpinBox *zoom_factor_spin_box_13;
    QLabel *label_100;
    QDoubleSpinBox *trans_x_spin_box_9;
    QLabel *label_101;
    QDoubleSpinBox *trans_y_spin_box_9;
    QLabel *label_102;
    QDoubleSpinBox *trans_z_spin_box_9;
    QSlider *shader_slider_2;
    QLabel *label_107;
    QDoubleSpinBox *rgb_spin_1;
    QDoubleSpinBox *rgb_spin_3;
    QDoubleSpinBox *rgb_spin_2;
    QDoubleSpinBox *rgb_spin_4;
    QLabel *label_104;
    QLabel *label_105;
    QLabel *label_106;
    QLabel *label_108;
    QSlider *scale_factor_slider;
    QSlider *smoothing_slider;
    QSlider *shading_slider;
    QLabel *label_109;
    QLabel *label_110;
    QLabel *label_111;

    void setupUi(QWidget *SideWidget)
    {
        if (SideWidget->objectName().isEmpty())
            SideWidget->setObjectName(QString::fromUtf8("SideWidget"));
        SideWidget->resize(319, 806);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SideWidget->sizePolicy().hasHeightForWidth());
        SideWidget->setSizePolicy(sizePolicy);
        SideWidget->setMinimumSize(QSize(269, 0));
        groupBox = new QGroupBox(SideWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 321, 841));
        tabWidget = new QTabWidget(groupBox);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 321, 811));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        layoutWidget = new QWidget(tab);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 251, 331));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        rotate_x_slider = new QSlider(layoutWidget);
        rotate_x_slider->setObjectName(QString::fromUtf8("rotate_x_slider"));
        rotate_x_slider->setMinimum(-180);
        rotate_x_slider->setMaximum(180);
        rotate_x_slider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(0, QFormLayout::FieldRole, rotate_x_slider);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        rotate_y_slider = new QSlider(layoutWidget);
        rotate_y_slider->setObjectName(QString::fromUtf8("rotate_y_slider"));
        rotate_y_slider->setMinimum(-180);
        rotate_y_slider->setMaximum(180);
        rotate_y_slider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(1, QFormLayout::FieldRole, rotate_y_slider);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_3);

        rotate_z_slider = new QSlider(layoutWidget);
        rotate_z_slider->setObjectName(QString::fromUtf8("rotate_z_slider"));
        rotate_z_slider->setMinimum(-180);
        rotate_z_slider->setMaximum(180);
        rotate_z_slider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(2, QFormLayout::FieldRole, rotate_z_slider);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        zoom_factor_spin_box = new QDoubleSpinBox(layoutWidget);
        zoom_factor_spin_box->setObjectName(QString::fromUtf8("zoom_factor_spin_box"));
        zoom_factor_spin_box->setDecimals(5);
        zoom_factor_spin_box->setMinimum(0.000100000000000);
        zoom_factor_spin_box->setMaximum(10000.000000000000000);
        zoom_factor_spin_box->setSingleStep(0.010000000000000);
        zoom_factor_spin_box->setValue(0.070000000000000);

        formLayout->setWidget(3, QFormLayout::FieldRole, zoom_factor_spin_box);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        trans_x_spin_box = new QDoubleSpinBox(layoutWidget);
        trans_x_spin_box->setObjectName(QString::fromUtf8("trans_x_spin_box"));
        trans_x_spin_box->setMinimum(-100.000000000000000);
        trans_x_spin_box->setMaximum(100.000000000000000);
        trans_x_spin_box->setSingleStep(0.100000000000000);

        formLayout->setWidget(4, QFormLayout::FieldRole, trans_x_spin_box);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_6);

        trans_y_spin_box = new QDoubleSpinBox(layoutWidget);
        trans_y_spin_box->setObjectName(QString::fromUtf8("trans_y_spin_box"));
        trans_y_spin_box->setMinimum(-100.000000000000000);
        trans_y_spin_box->setMaximum(100.000000000000000);
        trans_y_spin_box->setSingleStep(0.100000000000000);

        formLayout->setWidget(5, QFormLayout::FieldRole, trans_y_spin_box);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_7);

        trans_z_spin_box = new QDoubleSpinBox(layoutWidget);
        trans_z_spin_box->setObjectName(QString::fromUtf8("trans_z_spin_box"));
        trans_z_spin_box->setMinimum(-100.000000000000000);
        trans_z_spin_box->setMaximum(100.000000000000000);
        trans_z_spin_box->setSingleStep(0.100000000000000);

        formLayout->setWidget(6, QFormLayout::FieldRole, trans_z_spin_box);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout = new QVBoxLayout(tab_2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_12 = new QLabel(tab_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        verticalLayout->addWidget(label_12);

        curve_slider = new QSlider(tab_2);
        curve_slider->setObjectName(QString::fromUtf8("curve_slider"));
        curve_slider->setMaximum(6);
        curve_slider->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(curve_slider);

        label_8 = new QLabel(tab_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        verticalLayout->addWidget(label_8);

        rotate_x_slider_2 = new QSlider(tab_2);
        rotate_x_slider_2->setObjectName(QString::fromUtf8("rotate_x_slider_2"));
        rotate_x_slider_2->setMinimum(-180);
        rotate_x_slider_2->setMaximum(180);
        rotate_x_slider_2->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(rotate_x_slider_2);

        label_9 = new QLabel(tab_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        verticalLayout->addWidget(label_9);

        rotate_y_slider_2 = new QSlider(tab_2);
        rotate_y_slider_2->setObjectName(QString::fromUtf8("rotate_y_slider_2"));
        rotate_y_slider_2->setMinimum(-180);
        rotate_y_slider_2->setMaximum(180);
        rotate_y_slider_2->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(rotate_y_slider_2);

        label_10 = new QLabel(tab_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        verticalLayout->addWidget(label_10);

        rotate_z_slider_2 = new QSlider(tab_2);
        rotate_z_slider_2->setObjectName(QString::fromUtf8("rotate_z_slider_2"));
        rotate_z_slider_2->setMinimum(-180);
        rotate_z_slider_2->setMaximum(180);
        rotate_z_slider_2->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(rotate_z_slider_2);

        label_11 = new QLabel(tab_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        verticalLayout->addWidget(label_11);

        zoom_factor_spin_box_2 = new QDoubleSpinBox(tab_2);
        zoom_factor_spin_box_2->setObjectName(QString::fromUtf8("zoom_factor_spin_box_2"));
        zoom_factor_spin_box_2->setDecimals(5);
        zoom_factor_spin_box_2->setMinimum(0.000100000000000);
        zoom_factor_spin_box_2->setMaximum(10000.000000000000000);
        zoom_factor_spin_box_2->setSingleStep(0.100000000000000);
        zoom_factor_spin_box_2->setValue(1.000000000000000);

        verticalLayout->addWidget(zoom_factor_spin_box_2);

        checkBox = new QCheckBox(tab_2);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        verticalLayout->addWidget(checkBox);

        checkBox_2 = new QCheckBox(tab_2);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));

        verticalLayout->addWidget(checkBox_2);

        label_13 = new QLabel(tab_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        verticalLayout->addWidget(label_13);

        vector_length_spin_box = new QDoubleSpinBox(tab_2);
        vector_length_spin_box->setObjectName(QString::fromUtf8("vector_length_spin_box"));
        vector_length_spin_box->setDecimals(5);
        vector_length_spin_box->setMinimum(0.000000000000000);
        vector_length_spin_box->setMaximum(1.000000000000000);
        vector_length_spin_box->setSingleStep(0.050000000000000);
        vector_length_spin_box->setValue(1.000000000000000);

        verticalLayout->addWidget(vector_length_spin_box);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        layoutWidget_2 = new QWidget(tab_3);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(10, 10, 251, 331));
        formLayout_2 = new QFormLayout(layoutWidget_2);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_14 = new QLabel(layoutWidget_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_14);

        rotate_x_slider_3 = new QSlider(layoutWidget_2);
        rotate_x_slider_3->setObjectName(QString::fromUtf8("rotate_x_slider_3"));
        rotate_x_slider_3->setMinimum(-180);
        rotate_x_slider_3->setMaximum(180);
        rotate_x_slider_3->setOrientation(Qt::Horizontal);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, rotate_x_slider_3);

        label_15 = new QLabel(layoutWidget_2);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_15);

        rotate_y_slider_3 = new QSlider(layoutWidget_2);
        rotate_y_slider_3->setObjectName(QString::fromUtf8("rotate_y_slider_3"));
        rotate_y_slider_3->setMinimum(-180);
        rotate_y_slider_3->setMaximum(180);
        rotate_y_slider_3->setOrientation(Qt::Horizontal);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, rotate_y_slider_3);

        label_16 = new QLabel(layoutWidget_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_16);

        rotate_z_slider_3 = new QSlider(layoutWidget_2);
        rotate_z_slider_3->setObjectName(QString::fromUtf8("rotate_z_slider_3"));
        rotate_z_slider_3->setMinimum(-180);
        rotate_z_slider_3->setMaximum(180);
        rotate_z_slider_3->setOrientation(Qt::Horizontal);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, rotate_z_slider_3);

        label_17 = new QLabel(layoutWidget_2);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_17);

        zoom_factor_spin_box_3 = new QDoubleSpinBox(layoutWidget_2);
        zoom_factor_spin_box_3->setObjectName(QString::fromUtf8("zoom_factor_spin_box_3"));
        zoom_factor_spin_box_3->setDecimals(5);
        zoom_factor_spin_box_3->setMinimum(0.000100000000000);
        zoom_factor_spin_box_3->setMaximum(10000.000000000000000);
        zoom_factor_spin_box_3->setSingleStep(0.100000000000000);
        zoom_factor_spin_box_3->setValue(1.000000000000000);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, zoom_factor_spin_box_3);

        label_18 = new QLabel(layoutWidget_2);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, label_18);

        trans_x_spin_box_2 = new QDoubleSpinBox(layoutWidget_2);
        trans_x_spin_box_2->setObjectName(QString::fromUtf8("trans_x_spin_box_2"));
        trans_x_spin_box_2->setMinimum(-100.000000000000000);
        trans_x_spin_box_2->setMaximum(100.000000000000000);
        trans_x_spin_box_2->setSingleStep(0.100000000000000);

        formLayout_2->setWidget(4, QFormLayout::FieldRole, trans_x_spin_box_2);

        label_19 = new QLabel(layoutWidget_2);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        formLayout_2->setWidget(5, QFormLayout::LabelRole, label_19);

        trans_y_spin_box_2 = new QDoubleSpinBox(layoutWidget_2);
        trans_y_spin_box_2->setObjectName(QString::fromUtf8("trans_y_spin_box_2"));
        trans_y_spin_box_2->setMinimum(-100.000000000000000);
        trans_y_spin_box_2->setMaximum(100.000000000000000);
        trans_y_spin_box_2->setSingleStep(0.100000000000000);

        formLayout_2->setWidget(5, QFormLayout::FieldRole, trans_y_spin_box_2);

        label_20 = new QLabel(layoutWidget_2);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        formLayout_2->setWidget(6, QFormLayout::LabelRole, label_20);

        trans_z_spin_box_2 = new QDoubleSpinBox(layoutWidget_2);
        trans_z_spin_box_2->setObjectName(QString::fromUtf8("trans_z_spin_box_2"));
        trans_z_spin_box_2->setMinimum(-100.000000000000000);
        trans_z_spin_box_2->setMaximum(100.000000000000000);
        trans_z_spin_box_2->setSingleStep(0.100000000000000);

        formLayout_2->setWidget(6, QFormLayout::FieldRole, trans_z_spin_box_2);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        label_21 = new QLabel(tab_4);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setGeometry(QRect(10, 10, 235, 15));
        curve_slider_2 = new QSlider(tab_4);
        curve_slider_2->setObjectName(QString::fromUtf8("curve_slider_2"));
        curve_slider_2->setGeometry(QRect(10, 40, 235, 15));
        curve_slider_2->setMaximum(4);
        curve_slider_2->setOrientation(Qt::Horizontal);
        label_22 = new QLabel(tab_4);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(10, 80, 235, 15));
        zoom_factor_spin_box_4 = new QDoubleSpinBox(tab_4);
        zoom_factor_spin_box_4->setObjectName(QString::fromUtf8("zoom_factor_spin_box_4"));
        zoom_factor_spin_box_4->setGeometry(QRect(10, 100, 235, 23));
        zoom_factor_spin_box_4->setDecimals(5);
        zoom_factor_spin_box_4->setMinimum(0.000100000000000);
        zoom_factor_spin_box_4->setMaximum(10000.000000000000000);
        zoom_factor_spin_box_4->setSingleStep(0.100000000000000);
        zoom_factor_spin_box_4->setValue(1.000000000000000);
        checkBox_3 = new QCheckBox(tab_4);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setGeometry(QRect(10, 310, 235, 18));
        label_23 = new QLabel(tab_4);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setGeometry(QRect(10, 150, 235, 15));
        label_24 = new QLabel(tab_4);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setGeometry(QRect(10, 200, 235, 15));
        label_25 = new QLabel(tab_4);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setGeometry(QRect(10, 250, 235, 15));
        rotate_x_slider_4 = new QSlider(tab_4);
        rotate_x_slider_4->setObjectName(QString::fromUtf8("rotate_x_slider_4"));
        rotate_x_slider_4->setGeometry(QRect(10, 170, 235, 15));
        rotate_x_slider_4->setMinimum(-180);
        rotate_x_slider_4->setMaximum(180);
        rotate_x_slider_4->setOrientation(Qt::Horizontal);
        rotate_y_slider_4 = new QSlider(tab_4);
        rotate_y_slider_4->setObjectName(QString::fromUtf8("rotate_y_slider_4"));
        rotate_y_slider_4->setGeometry(QRect(10, 230, 235, 15));
        rotate_y_slider_4->setMinimum(-180);
        rotate_y_slider_4->setMaximum(180);
        rotate_y_slider_4->setOrientation(Qt::Horizontal);
        rotate_z_slider_4 = new QSlider(tab_4);
        rotate_z_slider_4->setObjectName(QString::fromUtf8("rotate_z_slider_4"));
        rotate_z_slider_4->setGeometry(QRect(10, 280, 235, 15));
        rotate_z_slider_4->setMinimum(-180);
        rotate_z_slider_4->setMaximum(180);
        rotate_z_slider_4->setOrientation(Qt::Horizontal);
        texture_slider = new QSlider(tab_4);
        texture_slider->setObjectName(QString::fromUtf8("texture_slider"));
        texture_slider->setGeometry(QRect(10, 340, 235, 15));
        texture_slider->setMaximum(9);
        texture_slider->setOrientation(Qt::Horizontal);
        tabWidget->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        layoutWidget_3 = new QWidget(tab_5);
        layoutWidget_3->setObjectName(QString::fromUtf8("layoutWidget_3"));
        layoutWidget_3->setGeometry(QRect(0, 10, 261, 591));
        formLayout_3 = new QFormLayout(layoutWidget_3);
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        formLayout_3->setContentsMargins(0, 0, 0, 0);
        label_26 = new QLabel(layoutWidget_3);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_26);

        rotate_x_slider_5 = new QSlider(layoutWidget_3);
        rotate_x_slider_5->setObjectName(QString::fromUtf8("rotate_x_slider_5"));
        rotate_x_slider_5->setMinimum(-180);
        rotate_x_slider_5->setMaximum(180);
        rotate_x_slider_5->setOrientation(Qt::Horizontal);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, rotate_x_slider_5);

        label_27 = new QLabel(layoutWidget_3);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label_27);

        rotate_y_slider_5 = new QSlider(layoutWidget_3);
        rotate_y_slider_5->setObjectName(QString::fromUtf8("rotate_y_slider_5"));
        rotate_y_slider_5->setMinimum(-180);
        rotate_y_slider_5->setMaximum(180);
        rotate_y_slider_5->setOrientation(Qt::Horizontal);

        formLayout_3->setWidget(1, QFormLayout::FieldRole, rotate_y_slider_5);

        label_28 = new QLabel(layoutWidget_3);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        formLayout_3->setWidget(2, QFormLayout::LabelRole, label_28);

        rotate_z_slider_5 = new QSlider(layoutWidget_3);
        rotate_z_slider_5->setObjectName(QString::fromUtf8("rotate_z_slider_5"));
        rotate_z_slider_5->setMinimum(-180);
        rotate_z_slider_5->setMaximum(180);
        rotate_z_slider_5->setOrientation(Qt::Horizontal);

        formLayout_3->setWidget(2, QFormLayout::FieldRole, rotate_z_slider_5);

        label_29 = new QLabel(layoutWidget_3);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        formLayout_3->setWidget(3, QFormLayout::LabelRole, label_29);

        zoom_factor_spin_box_5 = new QDoubleSpinBox(layoutWidget_3);
        zoom_factor_spin_box_5->setObjectName(QString::fromUtf8("zoom_factor_spin_box_5"));
        zoom_factor_spin_box_5->setDecimals(5);
        zoom_factor_spin_box_5->setMinimum(0.100000000000000);
        zoom_factor_spin_box_5->setMaximum(10.000000000000000);
        zoom_factor_spin_box_5->setSingleStep(0.100000000000000);
        zoom_factor_spin_box_5->setValue(0.100000000000000);

        formLayout_3->setWidget(3, QFormLayout::FieldRole, zoom_factor_spin_box_5);

        label_30 = new QLabel(layoutWidget_3);
        label_30->setObjectName(QString::fromUtf8("label_30"));

        formLayout_3->setWidget(4, QFormLayout::LabelRole, label_30);

        trans_x_spin_box_3 = new QDoubleSpinBox(layoutWidget_3);
        trans_x_spin_box_3->setObjectName(QString::fromUtf8("trans_x_spin_box_3"));
        trans_x_spin_box_3->setMinimum(-100.000000000000000);
        trans_x_spin_box_3->setMaximum(100.000000000000000);
        trans_x_spin_box_3->setSingleStep(0.100000000000000);

        formLayout_3->setWidget(4, QFormLayout::FieldRole, trans_x_spin_box_3);

        label_31 = new QLabel(layoutWidget_3);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        formLayout_3->setWidget(5, QFormLayout::LabelRole, label_31);

        trans_y_spin_box_3 = new QDoubleSpinBox(layoutWidget_3);
        trans_y_spin_box_3->setObjectName(QString::fromUtf8("trans_y_spin_box_3"));
        trans_y_spin_box_3->setMinimum(-100.000000000000000);
        trans_y_spin_box_3->setMaximum(100.000000000000000);
        trans_y_spin_box_3->setSingleStep(0.100000000000000);

        formLayout_3->setWidget(5, QFormLayout::FieldRole, trans_y_spin_box_3);

        label_32 = new QLabel(layoutWidget_3);
        label_32->setObjectName(QString::fromUtf8("label_32"));

        formLayout_3->setWidget(6, QFormLayout::LabelRole, label_32);

        trans_z_spin_box_3 = new QDoubleSpinBox(layoutWidget_3);
        trans_z_spin_box_3->setObjectName(QString::fromUtf8("trans_z_spin_box_3"));
        trans_z_spin_box_3->setMinimum(-100.000000000000000);
        trans_z_spin_box_3->setMaximum(100.000000000000000);
        trans_z_spin_box_3->setSingleStep(0.100000000000000);

        formLayout_3->setWidget(6, QFormLayout::FieldRole, trans_z_spin_box_3);

        label_33 = new QLabel(layoutWidget_3);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        formLayout_3->setWidget(7, QFormLayout::LabelRole, label_33);

        point_index_spin_box = new QDoubleSpinBox(layoutWidget_3);
        point_index_spin_box->setObjectName(QString::fromUtf8("point_index_spin_box"));
        point_index_spin_box->setDecimals(0);
        point_index_spin_box->setMinimum(0.000000000000000);
        point_index_spin_box->setMaximum(1.000000000000000);
        point_index_spin_box->setSingleStep(1.000000000000000);

        formLayout_3->setWidget(7, QFormLayout::FieldRole, point_index_spin_box);

        label_34 = new QLabel(layoutWidget_3);
        label_34->setObjectName(QString::fromUtf8("label_34"));

        formLayout_3->setWidget(8, QFormLayout::LabelRole, label_34);

        point_index_spin_box_x = new QDoubleSpinBox(layoutWidget_3);
        point_index_spin_box_x->setObjectName(QString::fromUtf8("point_index_spin_box_x"));
        point_index_spin_box_x->setDecimals(5);
        point_index_spin_box_x->setMinimum(-100.000000000000000);
        point_index_spin_box_x->setMaximum(100.000000000000000);
        point_index_spin_box_x->setSingleStep(0.100000000000000);

        formLayout_3->setWidget(8, QFormLayout::FieldRole, point_index_spin_box_x);

        label_35 = new QLabel(layoutWidget_3);
        label_35->setObjectName(QString::fromUtf8("label_35"));

        formLayout_3->setWidget(9, QFormLayout::LabelRole, label_35);

        point_index_spin_box_y = new QDoubleSpinBox(layoutWidget_3);
        point_index_spin_box_y->setObjectName(QString::fromUtf8("point_index_spin_box_y"));
        point_index_spin_box_y->setDecimals(5);
        point_index_spin_box_y->setMinimum(-100.000000000000000);
        point_index_spin_box_y->setMaximum(100.000000000000000);
        point_index_spin_box_y->setSingleStep(0.100000000000000);

        formLayout_3->setWidget(9, QFormLayout::FieldRole, point_index_spin_box_y);

        label_36 = new QLabel(layoutWidget_3);
        label_36->setObjectName(QString::fromUtf8("label_36"));

        formLayout_3->setWidget(10, QFormLayout::LabelRole, label_36);

        point_index_spin_box_z = new QDoubleSpinBox(layoutWidget_3);
        point_index_spin_box_z->setObjectName(QString::fromUtf8("point_index_spin_box_z"));
        point_index_spin_box_z->setDecimals(5);
        point_index_spin_box_z->setMinimum(-100.000000000000000);
        point_index_spin_box_z->setMaximum(100.000000000000000);
        point_index_spin_box_z->setSingleStep(0.100000000000000);

        formLayout_3->setWidget(10, QFormLayout::FieldRole, point_index_spin_box_z);

        tabWidget->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        toolBox_2 = new QToolBox(tab_6);
        toolBox_2->setObjectName(QString::fromUtf8("toolBox_2"));
        toolBox_2->setGeometry(QRect(0, 80, 311, 501));
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        page_4->setGeometry(QRect(0, 0, 311, 439));
        groupBox_2 = new QGroupBox(page_4);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(0, 0, 301, 261));
        layoutWidget_7 = new QWidget(groupBox_2);
        layoutWidget_7->setObjectName(QString::fromUtf8("layoutWidget_7"));
        layoutWidget_7->setGeometry(QRect(0, 20, 301, 241));
        formLayout_7 = new QFormLayout(layoutWidget_7);
        formLayout_7->setObjectName(QString::fromUtf8("formLayout_7"));
        formLayout_7->setContentsMargins(0, 0, 0, 0);
        label_73 = new QLabel(layoutWidget_7);
        label_73->setObjectName(QString::fromUtf8("label_73"));

        formLayout_7->setWidget(0, QFormLayout::LabelRole, label_73);

        rotate_x_slider_11 = new QSlider(layoutWidget_7);
        rotate_x_slider_11->setObjectName(QString::fromUtf8("rotate_x_slider_11"));
        rotate_x_slider_11->setMinimum(-180);
        rotate_x_slider_11->setMaximum(180);
        rotate_x_slider_11->setOrientation(Qt::Horizontal);

        formLayout_7->setWidget(0, QFormLayout::FieldRole, rotate_x_slider_11);

        label_74 = new QLabel(layoutWidget_7);
        label_74->setObjectName(QString::fromUtf8("label_74"));

        formLayout_7->setWidget(1, QFormLayout::LabelRole, label_74);

        rotate_y_slider_11 = new QSlider(layoutWidget_7);
        rotate_y_slider_11->setObjectName(QString::fromUtf8("rotate_y_slider_11"));
        rotate_y_slider_11->setMinimum(-180);
        rotate_y_slider_11->setMaximum(180);
        rotate_y_slider_11->setOrientation(Qt::Horizontal);

        formLayout_7->setWidget(1, QFormLayout::FieldRole, rotate_y_slider_11);

        label_75 = new QLabel(layoutWidget_7);
        label_75->setObjectName(QString::fromUtf8("label_75"));

        formLayout_7->setWidget(2, QFormLayout::LabelRole, label_75);

        rotate_z_slider_11 = new QSlider(layoutWidget_7);
        rotate_z_slider_11->setObjectName(QString::fromUtf8("rotate_z_slider_11"));
        rotate_z_slider_11->setMinimum(-180);
        rotate_z_slider_11->setMaximum(180);
        rotate_z_slider_11->setOrientation(Qt::Horizontal);

        formLayout_7->setWidget(2, QFormLayout::FieldRole, rotate_z_slider_11);

        label_76 = new QLabel(layoutWidget_7);
        label_76->setObjectName(QString::fromUtf8("label_76"));

        formLayout_7->setWidget(3, QFormLayout::LabelRole, label_76);

        zoom_factor_spin_box_11 = new QDoubleSpinBox(layoutWidget_7);
        zoom_factor_spin_box_11->setObjectName(QString::fromUtf8("zoom_factor_spin_box_11"));
        zoom_factor_spin_box_11->setDecimals(5);
        zoom_factor_spin_box_11->setMinimum(0.100000000000000);
        zoom_factor_spin_box_11->setMaximum(10.000000000000000);
        zoom_factor_spin_box_11->setSingleStep(0.100000000000000);
        zoom_factor_spin_box_11->setValue(0.100000000000000);

        formLayout_7->setWidget(3, QFormLayout::FieldRole, zoom_factor_spin_box_11);

        label_81 = new QLabel(layoutWidget_7);
        label_81->setObjectName(QString::fromUtf8("label_81"));

        formLayout_7->setWidget(4, QFormLayout::LabelRole, label_81);

        horizontalSlider_x = new QSlider(layoutWidget_7);
        horizontalSlider_x->setObjectName(QString::fromUtf8("horizontalSlider_x"));
        horizontalSlider_x->setMinimum(-180);
        horizontalSlider_x->setMaximum(180);
        horizontalSlider_x->setOrientation(Qt::Horizontal);

        formLayout_7->setWidget(4, QFormLayout::FieldRole, horizontalSlider_x);

        label_82 = new QLabel(layoutWidget_7);
        label_82->setObjectName(QString::fromUtf8("label_82"));

        formLayout_7->setWidget(5, QFormLayout::LabelRole, label_82);

        horizontalSlider_y = new QSlider(layoutWidget_7);
        horizontalSlider_y->setObjectName(QString::fromUtf8("horizontalSlider_y"));
        horizontalSlider_y->setMinimum(-180);
        horizontalSlider_y->setMaximum(180);
        horizontalSlider_y->setOrientation(Qt::Horizontal);

        formLayout_7->setWidget(5, QFormLayout::FieldRole, horizontalSlider_y);

        label_83 = new QLabel(layoutWidget_7);
        label_83->setObjectName(QString::fromUtf8("label_83"));

        formLayout_7->setWidget(6, QFormLayout::LabelRole, label_83);

        horizontalSlider_z = new QSlider(layoutWidget_7);
        horizontalSlider_z->setObjectName(QString::fromUtf8("horizontalSlider_z"));
        horizontalSlider_z->setMinimum(-180);
        horizontalSlider_z->setMaximum(180);
        horizontalSlider_z->setOrientation(Qt::Horizontal);

        formLayout_7->setWidget(6, QFormLayout::FieldRole, horizontalSlider_z);

        label_103 = new QLabel(layoutWidget_7);
        label_103->setObjectName(QString::fromUtf8("label_103"));

        formLayout_7->setWidget(7, QFormLayout::LabelRole, label_103);

        vectorSpinBox = new QDoubleSpinBox(layoutWidget_7);
        vectorSpinBox->setObjectName(QString::fromUtf8("vectorSpinBox"));
        vectorSpinBox->setMinimum(0.100000000000000);
        vectorSpinBox->setMaximum(10.000000000000000);
        vectorSpinBox->setSingleStep(0.100000000000000);
        vectorSpinBox->setValue(1.000000000000000);

        formLayout_7->setWidget(7, QFormLayout::FieldRole, vectorSpinBox);

        checkBox_6 = new QCheckBox(layoutWidget_7);
        checkBox_6->setObjectName(QString::fromUtf8("checkBox_6"));

        formLayout_7->setWidget(8, QFormLayout::LabelRole, checkBox_6);

        checkBox_5 = new QCheckBox(layoutWidget_7);
        checkBox_5->setObjectName(QString::fromUtf8("checkBox_5"));

        formLayout_7->setWidget(9, QFormLayout::LabelRole, checkBox_5);

        checkBox_move_all_arc = new QCheckBox(layoutWidget_7);
        checkBox_move_all_arc->setObjectName(QString::fromUtf8("checkBox_move_all_arc"));

        formLayout_7->setWidget(8, QFormLayout::FieldRole, checkBox_move_all_arc);

        layoutWidget_12 = new QWidget(page_4);
        layoutWidget_12->setObjectName(QString::fromUtf8("layoutWidget_12"));
        layoutWidget_12->setGeometry(QRect(10, 260, 281, 181));
        formLayout_12 = new QFormLayout(layoutWidget_12);
        formLayout_12->setObjectName(QString::fromUtf8("formLayout_12"));
        formLayout_12->setContentsMargins(0, 0, 0, 0);
        vectorSpinBox_cp_x = new QDoubleSpinBox(layoutWidget_12);
        vectorSpinBox_cp_x->setObjectName(QString::fromUtf8("vectorSpinBox_cp_x"));
        vectorSpinBox_cp_x->setMinimum(-100.000000000000000);
        vectorSpinBox_cp_x->setMaximum(100.000000000000000);
        vectorSpinBox_cp_x->setSingleStep(0.100000000000000);
        vectorSpinBox_cp_x->setValue(1.000000000000000);

        formLayout_12->setWidget(1, QFormLayout::FieldRole, vectorSpinBox_cp_x);

        label_125 = new QLabel(layoutWidget_12);
        label_125->setObjectName(QString::fromUtf8("label_125"));

        formLayout_12->setWidget(2, QFormLayout::LabelRole, label_125);

        vectorSpinBox_cp_y = new QDoubleSpinBox(layoutWidget_12);
        vectorSpinBox_cp_y->setObjectName(QString::fromUtf8("vectorSpinBox_cp_y"));
        vectorSpinBox_cp_y->setMinimum(-100.000000000000000);
        vectorSpinBox_cp_y->setMaximum(100.000000000000000);
        vectorSpinBox_cp_y->setSingleStep(0.100000000000000);
        vectorSpinBox_cp_y->setValue(1.000000000000000);

        formLayout_12->setWidget(2, QFormLayout::FieldRole, vectorSpinBox_cp_y);

        label_126 = new QLabel(layoutWidget_12);
        label_126->setObjectName(QString::fromUtf8("label_126"));

        formLayout_12->setWidget(3, QFormLayout::LabelRole, label_126);

        vectorSpinBox_cp_z = new QDoubleSpinBox(layoutWidget_12);
        vectorSpinBox_cp_z->setObjectName(QString::fromUtf8("vectorSpinBox_cp_z"));
        vectorSpinBox_cp_z->setMinimum(-100.000000000000000);
        vectorSpinBox_cp_z->setMaximum(100.000000000000000);
        vectorSpinBox_cp_z->setSingleStep(0.100000000000000);
        vectorSpinBox_cp_z->setValue(1.000000000000000);

        formLayout_12->setWidget(3, QFormLayout::FieldRole, vectorSpinBox_cp_z);

        trans_x_spin_box_10 = new QDoubleSpinBox(layoutWidget_12);
        trans_x_spin_box_10->setObjectName(QString::fromUtf8("trans_x_spin_box_10"));
        trans_x_spin_box_10->setMinimum(-100.000000000000000);
        trans_x_spin_box_10->setMaximum(100.000000000000000);
        trans_x_spin_box_10->setSingleStep(0.100000000000000);

        formLayout_12->setWidget(4, QFormLayout::FieldRole, trans_x_spin_box_10);

        label_124 = new QLabel(layoutWidget_12);
        label_124->setObjectName(QString::fromUtf8("label_124"));

        formLayout_12->setWidget(1, QFormLayout::LabelRole, label_124);

        trans_y_spin_box_10 = new QDoubleSpinBox(layoutWidget_12);
        trans_y_spin_box_10->setObjectName(QString::fromUtf8("trans_y_spin_box_10"));
        trans_y_spin_box_10->setMinimum(-100.000000000000000);
        trans_y_spin_box_10->setMaximum(100.000000000000000);
        trans_y_spin_box_10->setSingleStep(0.100000000000000);

        formLayout_12->setWidget(5, QFormLayout::FieldRole, trans_y_spin_box_10);

        trans_z_spin_box_10 = new QDoubleSpinBox(layoutWidget_12);
        trans_z_spin_box_10->setObjectName(QString::fromUtf8("trans_z_spin_box_10"));
        trans_z_spin_box_10->setMinimum(-100.000000000000000);
        trans_z_spin_box_10->setMaximum(100.000000000000000);
        trans_z_spin_box_10->setSingleStep(0.100000000000000);

        formLayout_12->setWidget(6, QFormLayout::FieldRole, trans_z_spin_box_10);

        label_139 = new QLabel(layoutWidget_12);
        label_139->setObjectName(QString::fromUtf8("label_139"));

        formLayout_12->setWidget(4, QFormLayout::LabelRole, label_139);

        label_140 = new QLabel(layoutWidget_12);
        label_140->setObjectName(QString::fromUtf8("label_140"));

        formLayout_12->setWidget(5, QFormLayout::LabelRole, label_140);

        label_141 = new QLabel(layoutWidget_12);
        label_141->setObjectName(QString::fromUtf8("label_141"));

        formLayout_12->setWidget(6, QFormLayout::LabelRole, label_141);

        toolBox_2->addItem(page_4, QString::fromUtf8("Show"));
        page_5 = new QWidget();
        page_5->setObjectName(QString::fromUtf8("page_5"));
        page_5->setGeometry(QRect(0, 0, 311, 479));
        comboBox_join = new QComboBox(page_5);
        comboBox_join->addItem(QString());
        comboBox_join->addItem(QString());
        comboBox_join->addItem(QString());
        comboBox_join->setObjectName(QString::fromUtf8("comboBox_join"));
        comboBox_join->setGeometry(QRect(10, 60, 61, 24));
        spinBox_arc_index_1 = new QSpinBox(page_5);
        spinBox_arc_index_1->setObjectName(QString::fromUtf8("spinBox_arc_index_1"));
        spinBox_arc_index_1->setGeometry(QRect(80, 10, 71, 25));
        spinBox_arc_index_2 = new QSpinBox(page_5);
        spinBox_arc_index_2->setObjectName(QString::fromUtf8("spinBox_arc_index_2"));
        spinBox_arc_index_2->setGeometry(QRect(240, 10, 71, 25));
        comboBox_direction = new QComboBox(page_5);
        comboBox_direction->addItem(QString());
        comboBox_direction->addItem(QString());
        comboBox_direction->addItem(QString());
        comboBox_direction->addItem(QString());
        comboBox_direction->setObjectName(QString::fromUtf8("comboBox_direction"));
        comboBox_direction->setGeometry(QRect(80, 60, 111, 24));
        pushButton_submit = new QPushButton(page_5);
        pushButton_submit->setObjectName(QString::fromUtf8("pushButton_submit"));
        pushButton_submit->setGeometry(QRect(200, 60, 101, 21));
        label_123 = new QLabel(page_5);
        label_123->setObjectName(QString::fromUtf8("label_123"));
        label_123->setGeometry(QRect(0, 10, 71, 25));
        label_127 = new QLabel(page_5);
        label_127->setObjectName(QString::fromUtf8("label_127"));
        label_127->setGeometry(QRect(160, 10, 71, 25));
        toolBox_2->addItem(page_5, QString::fromUtf8("Add new"));
        label_80 = new QLabel(tab_6);
        label_80->setObjectName(QString::fromUtf8("label_80"));
        label_80->setGeometry(QRect(20, 40, 62, 25));
        point_index_spin_box_3 = new QDoubleSpinBox(tab_6);
        point_index_spin_box_3->setObjectName(QString::fromUtf8("point_index_spin_box_3"));
        point_index_spin_box_3->setGeometry(QRect(110, 40, 201, 25));
        point_index_spin_box_3->setDecimals(0);
        point_index_spin_box_3->setMinimum(0.000000000000000);
        point_index_spin_box_3->setMaximum(1.000000000000000);
        point_index_spin_box_3->setSingleStep(1.000000000000000);
        label_112 = new QLabel(tab_6);
        label_112->setObjectName(QString::fromUtf8("label_112"));
        label_112->setGeometry(QRect(20, 10, 62, 25));
        pushButton = new QPushButton(tab_6);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(10, 580, 101, 51));
        spinBox_arc_index = new QSpinBox(tab_6);
        spinBox_arc_index->setObjectName(QString::fromUtf8("spinBox_arc_index"));
        spinBox_arc_index->setGeometry(QRect(110, 10, 201, 25));
        pushButton_reset = new QPushButton(tab_6);
        pushButton_reset->setObjectName(QString::fromUtf8("pushButton_reset"));
        pushButton_reset->setGeometry(QRect(220, 600, 81, 31));
        tabWidget->addTab(tab_6, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        groupBox_3 = new QGroupBox(tab_7);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(0, 660, 301, 111));
        formLayoutWidget = new QWidget(groupBox_3);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 20, 161, 59));
        formLayout_4 = new QFormLayout(formLayoutWidget);
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        formLayout_4->setContentsMargins(0, 0, 0, 0);
        label_136 = new QLabel(formLayoutWidget);
        label_136->setObjectName(QString::fromUtf8("label_136"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_136);

        spinBox_patch_index_1 = new QSpinBox(formLayoutWidget);
        spinBox_patch_index_1->setObjectName(QString::fromUtf8("spinBox_patch_index_1"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, spinBox_patch_index_1);

        label_137 = new QLabel(formLayoutWidget);
        label_137->setObjectName(QString::fromUtf8("label_137"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, label_137);

        spinBox_patch_index_2 = new QSpinBox(formLayoutWidget);
        spinBox_patch_index_2->setObjectName(QString::fromUtf8("spinBox_patch_index_2"));

        formLayout_4->setWidget(1, QFormLayout::FieldRole, spinBox_patch_index_2);

        comboBox_join_2 = new QComboBox(groupBox_3);
        comboBox_join_2->addItem(QString());
        comboBox_join_2->addItem(QString());
        comboBox_join_2->addItem(QString());
        comboBox_join_2->setObjectName(QString::fromUtf8("comboBox_join_2"));
        comboBox_join_2->setGeometry(QRect(180, 20, 111, 21));
        pushButton_submit_2 = new QPushButton(groupBox_3);
        pushButton_submit_2->setObjectName(QString::fromUtf8("pushButton_submit_2"));
        pushButton_submit_2->setEnabled(false);
        pushButton_submit_2->setGeometry(QRect(180, 50, 111, 21));
        comboBox_direction_2 = new QComboBox(groupBox_3);
        comboBox_direction_2->addItem(QString());
        comboBox_direction_2->addItem(QString());
        comboBox_direction_2->addItem(QString());
        comboBox_direction_2->addItem(QString());
        comboBox_direction_2->addItem(QString());
        comboBox_direction_2->addItem(QString());
        comboBox_direction_2->addItem(QString());
        comboBox_direction_2->addItem(QString());
        comboBox_direction_2->setObjectName(QString::fromUtf8("comboBox_direction_2"));
        comboBox_direction_2->setGeometry(QRect(10, 80, 131, 24));
        comboBox_direction_extend = new QComboBox(groupBox_3);
        comboBox_direction_extend->addItem(QString());
        comboBox_direction_extend->addItem(QString());
        comboBox_direction_extend->addItem(QString());
        comboBox_direction_extend->addItem(QString());
        comboBox_direction_extend->addItem(QString());
        comboBox_direction_extend->addItem(QString());
        comboBox_direction_extend->addItem(QString());
        comboBox_direction_extend->addItem(QString());
        comboBox_direction_extend->setObjectName(QString::fromUtf8("comboBox_direction_extend"));
        comboBox_direction_extend->setEnabled(false);
        comboBox_direction_extend->setGeometry(QRect(150, 80, 141, 24));
        formLayoutWidget_2 = new QWidget(tab_7);
        formLayoutWidget_2->setObjectName(QString::fromUtf8("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(10, 10, 301, 91));
        formLayout_5 = new QFormLayout(formLayoutWidget_2);
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        formLayout_5->setContentsMargins(0, 0, 0, 0);
        label_119 = new QLabel(formLayoutWidget_2);
        label_119->setObjectName(QString::fromUtf8("label_119"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_119);

        spinBox_patch_index = new QSpinBox(formLayoutWidget_2);
        spinBox_patch_index->setObjectName(QString::fromUtf8("spinBox_patch_index"));

        formLayout_5->setWidget(0, QFormLayout::FieldRole, spinBox_patch_index);

        label_84 = new QLabel(formLayoutWidget_2);
        label_84->setObjectName(QString::fromUtf8("label_84"));

        formLayout_5->setWidget(1, QFormLayout::LabelRole, label_84);

        spinBox_patch_point_index = new QSpinBox(formLayoutWidget_2);
        spinBox_patch_point_index->setObjectName(QString::fromUtf8("spinBox_patch_point_index"));
        spinBox_patch_point_index->setMaximum(3);

        formLayout_5->setWidget(1, QFormLayout::FieldRole, spinBox_patch_point_index);

        spinBox_vectorNumber = new QSpinBox(formLayoutWidget_2);
        spinBox_vectorNumber->setObjectName(QString::fromUtf8("spinBox_vectorNumber"));
        spinBox_vectorNumber->setMaximum(2);

        formLayout_5->setWidget(2, QFormLayout::FieldRole, spinBox_vectorNumber);

        label_113 = new QLabel(formLayoutWidget_2);
        label_113->setObjectName(QString::fromUtf8("label_113"));

        formLayout_5->setWidget(2, QFormLayout::LabelRole, label_113);

        toolBox_3 = new QToolBox(tab_7);
        toolBox_3->setObjectName(QString::fromUtf8("toolBox_3"));
        toolBox_3->setGeometry(QRect(10, 90, 291, 541));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        page->setGeometry(QRect(0, 0, 291, 479));
        groupBox_4 = new QGroupBox(page);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(0, -20, 301, 501));
        layoutWidget_13 = new QWidget(groupBox_4);
        layoutWidget_13->setObjectName(QString::fromUtf8("layoutWidget_13"));
        layoutWidget_13->setGeometry(QRect(10, 20, 261, 565));
        formLayout_13 = new QFormLayout(layoutWidget_13);
        formLayout_13->setObjectName(QString::fromUtf8("formLayout_13"));
        formLayout_13->setContentsMargins(0, 0, 0, 0);
        label_77 = new QLabel(layoutWidget_13);
        label_77->setObjectName(QString::fromUtf8("label_77"));

        formLayout_13->setWidget(1, QFormLayout::LabelRole, label_77);

        rotate_x_slider_14 = new QSlider(layoutWidget_13);
        rotate_x_slider_14->setObjectName(QString::fromUtf8("rotate_x_slider_14"));
        rotate_x_slider_14->setMinimum(-180);
        rotate_x_slider_14->setMaximum(180);
        rotate_x_slider_14->setOrientation(Qt::Horizontal);

        formLayout_13->setWidget(1, QFormLayout::FieldRole, rotate_x_slider_14);

        label_78 = new QLabel(layoutWidget_13);
        label_78->setObjectName(QString::fromUtf8("label_78"));

        formLayout_13->setWidget(2, QFormLayout::LabelRole, label_78);

        rotate_y_slider_14 = new QSlider(layoutWidget_13);
        rotate_y_slider_14->setObjectName(QString::fromUtf8("rotate_y_slider_14"));
        rotate_y_slider_14->setMinimum(-180);
        rotate_y_slider_14->setMaximum(180);
        rotate_y_slider_14->setOrientation(Qt::Horizontal);

        formLayout_13->setWidget(2, QFormLayout::FieldRole, rotate_y_slider_14);

        label_79 = new QLabel(layoutWidget_13);
        label_79->setObjectName(QString::fromUtf8("label_79"));

        formLayout_13->setWidget(3, QFormLayout::LabelRole, label_79);

        rotate_z_slider_14 = new QSlider(layoutWidget_13);
        rotate_z_slider_14->setObjectName(QString::fromUtf8("rotate_z_slider_14"));
        rotate_z_slider_14->setMinimum(-180);
        rotate_z_slider_14->setMaximum(180);
        rotate_z_slider_14->setOrientation(Qt::Horizontal);

        formLayout_13->setWidget(3, QFormLayout::FieldRole, rotate_z_slider_14);

        label_128 = new QLabel(layoutWidget_13);
        label_128->setObjectName(QString::fromUtf8("label_128"));

        formLayout_13->setWidget(4, QFormLayout::LabelRole, label_128);

        zoom_factor_spin_box_14 = new QDoubleSpinBox(layoutWidget_13);
        zoom_factor_spin_box_14->setObjectName(QString::fromUtf8("zoom_factor_spin_box_14"));
        zoom_factor_spin_box_14->setDecimals(5);
        zoom_factor_spin_box_14->setMinimum(0.100000000000000);
        zoom_factor_spin_box_14->setMaximum(10.000000000000000);
        zoom_factor_spin_box_14->setSingleStep(0.100000000000000);
        zoom_factor_spin_box_14->setValue(1.000000000000000);

        formLayout_13->setWidget(4, QFormLayout::FieldRole, zoom_factor_spin_box_14);

        label_129 = new QLabel(layoutWidget_13);
        label_129->setObjectName(QString::fromUtf8("label_129"));

        formLayout_13->setWidget(5, QFormLayout::LabelRole, label_129);

        horizontalSlider_x_3 = new QSlider(layoutWidget_13);
        horizontalSlider_x_3->setObjectName(QString::fromUtf8("horizontalSlider_x_3"));
        horizontalSlider_x_3->setMinimum(-180);
        horizontalSlider_x_3->setMaximum(180);
        horizontalSlider_x_3->setOrientation(Qt::Horizontal);

        formLayout_13->setWidget(5, QFormLayout::FieldRole, horizontalSlider_x_3);

        label_130 = new QLabel(layoutWidget_13);
        label_130->setObjectName(QString::fromUtf8("label_130"));

        formLayout_13->setWidget(6, QFormLayout::LabelRole, label_130);

        horizontalSlider_y_3 = new QSlider(layoutWidget_13);
        horizontalSlider_y_3->setObjectName(QString::fromUtf8("horizontalSlider_y_3"));
        horizontalSlider_y_3->setMinimum(-180);
        horizontalSlider_y_3->setMaximum(180);
        horizontalSlider_y_3->setOrientation(Qt::Horizontal);

        formLayout_13->setWidget(6, QFormLayout::FieldRole, horizontalSlider_y_3);

        label_131 = new QLabel(layoutWidget_13);
        label_131->setObjectName(QString::fromUtf8("label_131"));

        formLayout_13->setWidget(7, QFormLayout::LabelRole, label_131);

        horizontalSlider_z_3 = new QSlider(layoutWidget_13);
        horizontalSlider_z_3->setObjectName(QString::fromUtf8("horizontalSlider_z_3"));
        horizontalSlider_z_3->setMinimum(-180);
        horizontalSlider_z_3->setMaximum(180);
        horizontalSlider_z_3->setOrientation(Qt::Horizontal);

        formLayout_13->setWidget(7, QFormLayout::FieldRole, horizontalSlider_z_3);

        label_132 = new QLabel(layoutWidget_13);
        label_132->setObjectName(QString::fromUtf8("label_132"));

        formLayout_13->setWidget(8, QFormLayout::LabelRole, label_132);

        vectorSpinBox_pach = new QDoubleSpinBox(layoutWidget_13);
        vectorSpinBox_pach->setObjectName(QString::fromUtf8("vectorSpinBox_pach"));
        vectorSpinBox_pach->setMinimum(0.100000000000000);
        vectorSpinBox_pach->setMaximum(10.000000000000000);
        vectorSpinBox_pach->setSingleStep(0.100000000000000);
        vectorSpinBox_pach->setValue(1.000000000000000);

        formLayout_13->setWidget(8, QFormLayout::FieldRole, vectorSpinBox_pach);

        label_133 = new QLabel(layoutWidget_13);
        label_133->setObjectName(QString::fromUtf8("label_133"));

        formLayout_13->setWidget(9, QFormLayout::LabelRole, label_133);

        vectorSpinBox_cp_x_2 = new QDoubleSpinBox(layoutWidget_13);
        vectorSpinBox_cp_x_2->setObjectName(QString::fromUtf8("vectorSpinBox_cp_x_2"));
        vectorSpinBox_cp_x_2->setMinimum(-100.000000000000000);
        vectorSpinBox_cp_x_2->setMaximum(100.000000000000000);
        vectorSpinBox_cp_x_2->setSingleStep(0.100000000000000);
        vectorSpinBox_cp_x_2->setValue(1.000000000000000);

        formLayout_13->setWidget(9, QFormLayout::FieldRole, vectorSpinBox_cp_x_2);

        label_134 = new QLabel(layoutWidget_13);
        label_134->setObjectName(QString::fromUtf8("label_134"));

        formLayout_13->setWidget(10, QFormLayout::LabelRole, label_134);

        vectorSpinBox_cp_y_2 = new QDoubleSpinBox(layoutWidget_13);
        vectorSpinBox_cp_y_2->setObjectName(QString::fromUtf8("vectorSpinBox_cp_y_2"));
        vectorSpinBox_cp_y_2->setMinimum(-100.000000000000000);
        vectorSpinBox_cp_y_2->setMaximum(100.000000000000000);
        vectorSpinBox_cp_y_2->setSingleStep(0.100000000000000);
        vectorSpinBox_cp_y_2->setValue(1.000000000000000);

        formLayout_13->setWidget(10, QFormLayout::FieldRole, vectorSpinBox_cp_y_2);

        label_135 = new QLabel(layoutWidget_13);
        label_135->setObjectName(QString::fromUtf8("label_135"));

        formLayout_13->setWidget(11, QFormLayout::LabelRole, label_135);

        vectorSpinBox_cp_z_2 = new QDoubleSpinBox(layoutWidget_13);
        vectorSpinBox_cp_z_2->setObjectName(QString::fromUtf8("vectorSpinBox_cp_z_2"));
        vectorSpinBox_cp_z_2->setMinimum(-100.000000000000000);
        vectorSpinBox_cp_z_2->setMaximum(100.000000000000000);
        vectorSpinBox_cp_z_2->setSingleStep(0.100000000000000);
        vectorSpinBox_cp_z_2->setValue(1.000000000000000);

        formLayout_13->setWidget(11, QFormLayout::FieldRole, vectorSpinBox_cp_z_2);

        label_88 = new QLabel(layoutWidget_13);
        label_88->setObjectName(QString::fromUtf8("label_88"));

        formLayout_13->setWidget(12, QFormLayout::LabelRole, label_88);

        trans_x_spin_box_8 = new QDoubleSpinBox(layoutWidget_13);
        trans_x_spin_box_8->setObjectName(QString::fromUtf8("trans_x_spin_box_8"));
        trans_x_spin_box_8->setMinimum(-100.000000000000000);
        trans_x_spin_box_8->setMaximum(100.000000000000000);
        trans_x_spin_box_8->setSingleStep(0.100000000000000);

        formLayout_13->setWidget(12, QFormLayout::FieldRole, trans_x_spin_box_8);

        label_89 = new QLabel(layoutWidget_13);
        label_89->setObjectName(QString::fromUtf8("label_89"));

        formLayout_13->setWidget(13, QFormLayout::LabelRole, label_89);

        trans_y_spin_box_8 = new QDoubleSpinBox(layoutWidget_13);
        trans_y_spin_box_8->setObjectName(QString::fromUtf8("trans_y_spin_box_8"));
        trans_y_spin_box_8->setMinimum(-100.000000000000000);
        trans_y_spin_box_8->setMaximum(100.000000000000000);
        trans_y_spin_box_8->setSingleStep(0.100000000000000);

        formLayout_13->setWidget(13, QFormLayout::FieldRole, trans_y_spin_box_8);

        label_90 = new QLabel(layoutWidget_13);
        label_90->setObjectName(QString::fromUtf8("label_90"));

        formLayout_13->setWidget(14, QFormLayout::LabelRole, label_90);

        trans_z_spin_box_8 = new QDoubleSpinBox(layoutWidget_13);
        trans_z_spin_box_8->setObjectName(QString::fromUtf8("trans_z_spin_box_8"));
        trans_z_spin_box_8->setMinimum(-100.000000000000000);
        trans_z_spin_box_8->setMaximum(100.000000000000000);
        trans_z_spin_box_8->setSingleStep(0.100000000000000);

        formLayout_13->setWidget(14, QFormLayout::FieldRole, trans_z_spin_box_8);

        label_95 = new QLabel(layoutWidget_13);
        label_95->setObjectName(QString::fromUtf8("label_95"));

        formLayout_13->setWidget(20, QFormLayout::LabelRole, label_95);

        comboBox_material = new QComboBox(layoutWidget_13);
        comboBox_material->addItem(QString());
        comboBox_material->addItem(QString());
        comboBox_material->addItem(QString());
        comboBox_material->addItem(QString());
        comboBox_material->addItem(QString());
        comboBox_material->addItem(QString());
        comboBox_material->addItem(QString());
        comboBox_material->setObjectName(QString::fromUtf8("comboBox_material"));

        formLayout_13->setWidget(20, QFormLayout::FieldRole, comboBox_material);

        checkBox_d0u = new QCheckBox(layoutWidget_13);
        checkBox_d0u->setObjectName(QString::fromUtf8("checkBox_d0u"));

        formLayout_13->setWidget(15, QFormLayout::FieldRole, checkBox_d0u);

        checkBox_d0v = new QCheckBox(layoutWidget_13);
        checkBox_d0v->setObjectName(QString::fromUtf8("checkBox_d0v"));

        formLayout_13->setWidget(15, QFormLayout::LabelRole, checkBox_d0v);

        checkBox_d1u = new QCheckBox(layoutWidget_13);
        checkBox_d1u->setObjectName(QString::fromUtf8("checkBox_d1u"));

        formLayout_13->setWidget(16, QFormLayout::LabelRole, checkBox_d1u);

        checkBox_d1v = new QCheckBox(layoutWidget_13);
        checkBox_d1v->setObjectName(QString::fromUtf8("checkBox_d1v"));

        formLayout_13->setWidget(16, QFormLayout::FieldRole, checkBox_d1v);

        pushButton_reset_2 = new QPushButton(layoutWidget_13);
        pushButton_reset_2->setObjectName(QString::fromUtf8("pushButton_reset_2"));

        formLayout_13->setWidget(18, QFormLayout::LabelRole, pushButton_reset_2);

        pushButton_newPatch = new QPushButton(layoutWidget_13);
        pushButton_newPatch->setObjectName(QString::fromUtf8("pushButton_newPatch"));

        formLayout_13->setWidget(18, QFormLayout::FieldRole, pushButton_newPatch);

        checkBox_vectors = new QCheckBox(layoutWidget_13);
        checkBox_vectors->setObjectName(QString::fromUtf8("checkBox_vectors"));

        formLayout_13->setWidget(0, QFormLayout::LabelRole, checkBox_vectors);

        checkBox_move_all = new QCheckBox(layoutWidget_13);
        checkBox_move_all->setObjectName(QString::fromUtf8("checkBox_move_all"));

        formLayout_13->setWidget(0, QFormLayout::FieldRole, checkBox_move_all);

        checkBox_d2u = new QCheckBox(groupBox_4);
        checkBox_d2u->setObjectName(QString::fromUtf8("checkBox_d2u"));
        checkBox_d2u->setGeometry(QRect(110, 500, 51, 19));
        checkBox_d2v = new QCheckBox(groupBox_4);
        checkBox_d2v->setObjectName(QString::fromUtf8("checkBox_d2v"));
        checkBox_d2v->setGeometry(QRect(170, 510, 51, 19));
        toolBox_3->addItem(page, QString::fromUtf8("Patch operations"));
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        page_3->setGeometry(QRect(0, 0, 100, 30));
        scale_factor_spinbox = new QDoubleSpinBox(page_3);
        scale_factor_spinbox->setObjectName(QString::fromUtf8("scale_factor_spinbox"));
        scale_factor_spinbox->setGeometry(QRect(100, 150, 62, 22));
        shader_combobox = new QComboBox(page_3);
        shader_combobox->addItem(QString());
        shader_combobox->addItem(QString());
        shader_combobox->addItem(QString());
        shader_combobox->addItem(QString());
        shader_combobox->setObjectName(QString::fromUtf8("shader_combobox"));
        shader_combobox->setEnabled(true);
        shader_combobox->setGeometry(QRect(80, 0, 100, 20));
        shader_combobox->setMaximumSize(QSize(100, 16777215));
        shader_combobox->setEditable(false);
        label_85 = new QLabel(page_3);
        label_85->setObjectName(QString::fromUtf8("label_85"));
        label_85->setGeometry(QRect(10, 170, 60, 13));
        smoothing_spinbox = new QDoubleSpinBox(page_3);
        smoothing_spinbox->setObjectName(QString::fromUtf8("smoothing_spinbox"));
        smoothing_spinbox->setGeometry(QRect(100, 170, 62, 22));
        b_spinbox = new QDoubleSpinBox(page_3);
        b_spinbox->setObjectName(QString::fromUtf8("b_spinbox"));
        b_spinbox->setGeometry(QRect(30, 80, 100, 20));
        b_spinbox->setMaximumSize(QSize(100, 16777215));
        b_spinbox->setMaximum(1.000000000000000);
        b_spinbox->setSingleStep(0.100000000000000);
        shading_spinbox = new QDoubleSpinBox(page_3);
        shading_spinbox->setObjectName(QString::fromUtf8("shading_spinbox"));
        shading_spinbox->setGeometry(QRect(100, 190, 62, 22));
        r_spinbox = new QDoubleSpinBox(page_3);
        r_spinbox->setObjectName(QString::fromUtf8("r_spinbox"));
        r_spinbox->setGeometry(QRect(30, 40, 100, 20));
        r_spinbox->setMaximumSize(QSize(100, 16777215));
        r_spinbox->setMaximum(1.000000000000000);
        r_spinbox->setSingleStep(0.100000000000000);
        a_spinbox = new QDoubleSpinBox(page_3);
        a_spinbox->setObjectName(QString::fromUtf8("a_spinbox"));
        a_spinbox->setGeometry(QRect(30, 100, 100, 20));
        a_spinbox->setMaximumSize(QSize(100, 16777215));
        a_spinbox->setMaximum(1.000000000000000);
        a_spinbox->setSingleStep(0.100000000000000);
        label_86 = new QLabel(page_3);
        label_86->setObjectName(QString::fromUtf8("label_86"));
        label_86->setGeometry(QRect(10, 190, 60, 13));
        alpha_on_checkbox = new QCheckBox(page_3);
        alpha_on_checkbox->setObjectName(QString::fromUtf8("alpha_on_checkbox"));
        alpha_on_checkbox->setGeometry(QRect(80, 20, 50, 17));
        alpha_on_checkbox->setMaximumSize(QSize(50, 16777215));
        label_87 = new QLabel(page_3);
        label_87->setObjectName(QString::fromUtf8("label_87"));
        label_87->setEnabled(true);
        label_87->setGeometry(QRect(10, 20, 36, 17));
        label_91 = new QLabel(page_3);
        label_91->setObjectName(QString::fromUtf8("label_91"));
        label_91->setGeometry(QRect(10, 100, 11, 18));
        label_91->setMaximumSize(QSize(20, 16777215));
        label_92 = new QLabel(page_3);
        label_92->setObjectName(QString::fromUtf8("label_92"));
        label_92->setGeometry(QRect(10, 130, 47, 13));
        label_93 = new QLabel(page_3);
        label_93->setObjectName(QString::fromUtf8("label_93"));
        label_93->setGeometry(QRect(10, 80, 10, 18));
        label_93->setMaximumSize(QSize(20, 16777215));
        g_spinbox = new QDoubleSpinBox(page_3);
        g_spinbox->setObjectName(QString::fromUtf8("g_spinbox"));
        g_spinbox->setGeometry(QRect(30, 60, 100, 20));
        g_spinbox->setMaximumSize(QSize(100, 16777215));
        g_spinbox->setMaximum(1.000000000000000);
        g_spinbox->setSingleStep(0.100000000000000);
        label_94 = new QLabel(page_3);
        label_94->setObjectName(QString::fromUtf8("label_94"));
        label_94->setGeometry(QRect(10, 60, 11, 17));
        label_94->setMaximumSize(QSize(20, 16777215));
        label_122 = new QLabel(page_3);
        label_122->setObjectName(QString::fromUtf8("label_122"));
        label_122->setGeometry(QRect(10, 40, 11, 16));
        label_122->setMaximumSize(QSize(20, 16777215));
        label_138 = new QLabel(page_3);
        label_138->setObjectName(QString::fromUtf8("label_138"));
        label_138->setGeometry(QRect(10, 150, 60, 13));
        toolBox_3->addItem(page_3, QString::fromUtf8("Shaders"));
        tabWidget->addTab(tab_7, QString());
        tab_8 = new QWidget();
        tab_8->setObjectName(QString::fromUtf8("tab_8"));
        layoutWidget_9 = new QWidget(tab_8);
        layoutWidget_9->setObjectName(QString::fromUtf8("layoutWidget_9"));
        layoutWidget_9->setGeometry(QRect(10, 10, 291, 611));
        formLayout_9 = new QFormLayout(layoutWidget_9);
        formLayout_9->setObjectName(QString::fromUtf8("formLayout_9"));
        formLayout_9->setContentsMargins(0, 0, 0, 0);
        label_96 = new QLabel(layoutWidget_9);
        label_96->setObjectName(QString::fromUtf8("label_96"));

        formLayout_9->setWidget(0, QFormLayout::LabelRole, label_96);

        rotate_x_slider_13 = new QSlider(layoutWidget_9);
        rotate_x_slider_13->setObjectName(QString::fromUtf8("rotate_x_slider_13"));
        rotate_x_slider_13->setMinimum(-180);
        rotate_x_slider_13->setMaximum(180);
        rotate_x_slider_13->setOrientation(Qt::Horizontal);

        formLayout_9->setWidget(0, QFormLayout::FieldRole, rotate_x_slider_13);

        label_97 = new QLabel(layoutWidget_9);
        label_97->setObjectName(QString::fromUtf8("label_97"));

        formLayout_9->setWidget(1, QFormLayout::LabelRole, label_97);

        rotate_y_slider_13 = new QSlider(layoutWidget_9);
        rotate_y_slider_13->setObjectName(QString::fromUtf8("rotate_y_slider_13"));
        rotate_y_slider_13->setMinimum(-180);
        rotate_y_slider_13->setMaximum(180);
        rotate_y_slider_13->setOrientation(Qt::Horizontal);

        formLayout_9->setWidget(1, QFormLayout::FieldRole, rotate_y_slider_13);

        label_98 = new QLabel(layoutWidget_9);
        label_98->setObjectName(QString::fromUtf8("label_98"));

        formLayout_9->setWidget(2, QFormLayout::LabelRole, label_98);

        rotate_z_slider_13 = new QSlider(layoutWidget_9);
        rotate_z_slider_13->setObjectName(QString::fromUtf8("rotate_z_slider_13"));
        rotate_z_slider_13->setMinimum(-180);
        rotate_z_slider_13->setMaximum(180);
        rotate_z_slider_13->setOrientation(Qt::Horizontal);

        formLayout_9->setWidget(2, QFormLayout::FieldRole, rotate_z_slider_13);

        label_99 = new QLabel(layoutWidget_9);
        label_99->setObjectName(QString::fromUtf8("label_99"));

        formLayout_9->setWidget(3, QFormLayout::LabelRole, label_99);

        zoom_factor_spin_box_13 = new QDoubleSpinBox(layoutWidget_9);
        zoom_factor_spin_box_13->setObjectName(QString::fromUtf8("zoom_factor_spin_box_13"));
        zoom_factor_spin_box_13->setDecimals(5);
        zoom_factor_spin_box_13->setMinimum(0.100000000000000);
        zoom_factor_spin_box_13->setMaximum(10.000000000000000);
        zoom_factor_spin_box_13->setSingleStep(0.100000000000000);
        zoom_factor_spin_box_13->setValue(0.100000000000000);

        formLayout_9->setWidget(3, QFormLayout::FieldRole, zoom_factor_spin_box_13);

        label_100 = new QLabel(layoutWidget_9);
        label_100->setObjectName(QString::fromUtf8("label_100"));

        formLayout_9->setWidget(4, QFormLayout::LabelRole, label_100);

        trans_x_spin_box_9 = new QDoubleSpinBox(layoutWidget_9);
        trans_x_spin_box_9->setObjectName(QString::fromUtf8("trans_x_spin_box_9"));
        trans_x_spin_box_9->setMinimum(-100.000000000000000);
        trans_x_spin_box_9->setMaximum(100.000000000000000);
        trans_x_spin_box_9->setSingleStep(0.100000000000000);

        formLayout_9->setWidget(4, QFormLayout::FieldRole, trans_x_spin_box_9);

        label_101 = new QLabel(layoutWidget_9);
        label_101->setObjectName(QString::fromUtf8("label_101"));

        formLayout_9->setWidget(5, QFormLayout::LabelRole, label_101);

        trans_y_spin_box_9 = new QDoubleSpinBox(layoutWidget_9);
        trans_y_spin_box_9->setObjectName(QString::fromUtf8("trans_y_spin_box_9"));
        trans_y_spin_box_9->setMinimum(-100.000000000000000);
        trans_y_spin_box_9->setMaximum(100.000000000000000);
        trans_y_spin_box_9->setSingleStep(0.100000000000000);

        formLayout_9->setWidget(5, QFormLayout::FieldRole, trans_y_spin_box_9);

        label_102 = new QLabel(layoutWidget_9);
        label_102->setObjectName(QString::fromUtf8("label_102"));

        formLayout_9->setWidget(6, QFormLayout::LabelRole, label_102);

        trans_z_spin_box_9 = new QDoubleSpinBox(layoutWidget_9);
        trans_z_spin_box_9->setObjectName(QString::fromUtf8("trans_z_spin_box_9"));
        trans_z_spin_box_9->setMinimum(-100.000000000000000);
        trans_z_spin_box_9->setMaximum(100.000000000000000);
        trans_z_spin_box_9->setSingleStep(0.100000000000000);

        formLayout_9->setWidget(6, QFormLayout::FieldRole, trans_z_spin_box_9);

        shader_slider_2 = new QSlider(layoutWidget_9);
        shader_slider_2->setObjectName(QString::fromUtf8("shader_slider_2"));
        shader_slider_2->setMinimum(0);
        shader_slider_2->setMaximum(3);
        shader_slider_2->setOrientation(Qt::Horizontal);

        formLayout_9->setWidget(7, QFormLayout::FieldRole, shader_slider_2);

        label_107 = new QLabel(layoutWidget_9);
        label_107->setObjectName(QString::fromUtf8("label_107"));

        formLayout_9->setWidget(7, QFormLayout::LabelRole, label_107);

        rgb_spin_1 = new QDoubleSpinBox(layoutWidget_9);
        rgb_spin_1->setObjectName(QString::fromUtf8("rgb_spin_1"));
        rgb_spin_1->setMaximum(1.000000000000000);
        rgb_spin_1->setSingleStep(0.010000000000000);
        rgb_spin_1->setValue(1.000000000000000);

        formLayout_9->setWidget(8, QFormLayout::LabelRole, rgb_spin_1);

        rgb_spin_3 = new QDoubleSpinBox(layoutWidget_9);
        rgb_spin_3->setObjectName(QString::fromUtf8("rgb_spin_3"));
        rgb_spin_3->setMaximum(1.000000000000000);
        rgb_spin_3->setSingleStep(0.010000000000000);
        rgb_spin_3->setValue(1.000000000000000);

        formLayout_9->setWidget(10, QFormLayout::LabelRole, rgb_spin_3);

        rgb_spin_2 = new QDoubleSpinBox(layoutWidget_9);
        rgb_spin_2->setObjectName(QString::fromUtf8("rgb_spin_2"));
        rgb_spin_2->setMaximum(1.000000000000000);
        rgb_spin_2->setSingleStep(0.010000000000000);
        rgb_spin_2->setValue(1.000000000000000);

        formLayout_9->setWidget(9, QFormLayout::LabelRole, rgb_spin_2);

        rgb_spin_4 = new QDoubleSpinBox(layoutWidget_9);
        rgb_spin_4->setObjectName(QString::fromUtf8("rgb_spin_4"));
        rgb_spin_4->setMaximum(1.000000000000000);
        rgb_spin_4->setSingleStep(0.010000000000000);
        rgb_spin_4->setValue(1.000000000000000);

        formLayout_9->setWidget(11, QFormLayout::LabelRole, rgb_spin_4);

        label_104 = new QLabel(layoutWidget_9);
        label_104->setObjectName(QString::fromUtf8("label_104"));

        formLayout_9->setWidget(8, QFormLayout::FieldRole, label_104);

        label_105 = new QLabel(layoutWidget_9);
        label_105->setObjectName(QString::fromUtf8("label_105"));

        formLayout_9->setWidget(9, QFormLayout::FieldRole, label_105);

        label_106 = new QLabel(layoutWidget_9);
        label_106->setObjectName(QString::fromUtf8("label_106"));

        formLayout_9->setWidget(10, QFormLayout::FieldRole, label_106);

        label_108 = new QLabel(layoutWidget_9);
        label_108->setObjectName(QString::fromUtf8("label_108"));

        formLayout_9->setWidget(11, QFormLayout::FieldRole, label_108);

        scale_factor_slider = new QSlider(layoutWidget_9);
        scale_factor_slider->setObjectName(QString::fromUtf8("scale_factor_slider"));
        scale_factor_slider->setMinimum(0);
        scale_factor_slider->setMaximum(10);
        scale_factor_slider->setSingleStep(0);
        scale_factor_slider->setValue(4);
        scale_factor_slider->setOrientation(Qt::Horizontal);

        formLayout_9->setWidget(12, QFormLayout::LabelRole, scale_factor_slider);

        smoothing_slider = new QSlider(layoutWidget_9);
        smoothing_slider->setObjectName(QString::fromUtf8("smoothing_slider"));
        smoothing_slider->setMinimum(0);
        smoothing_slider->setMaximum(10);
        smoothing_slider->setValue(4);
        smoothing_slider->setOrientation(Qt::Horizontal);

        formLayout_9->setWidget(13, QFormLayout::LabelRole, smoothing_slider);

        shading_slider = new QSlider(layoutWidget_9);
        shading_slider->setObjectName(QString::fromUtf8("shading_slider"));
        shading_slider->setMinimum(0);
        shading_slider->setMaximum(10);
        shading_slider->setValue(6);
        shading_slider->setOrientation(Qt::Horizontal);

        formLayout_9->setWidget(14, QFormLayout::LabelRole, shading_slider);

        label_109 = new QLabel(layoutWidget_9);
        label_109->setObjectName(QString::fromUtf8("label_109"));

        formLayout_9->setWidget(12, QFormLayout::FieldRole, label_109);

        label_110 = new QLabel(layoutWidget_9);
        label_110->setObjectName(QString::fromUtf8("label_110"));

        formLayout_9->setWidget(13, QFormLayout::FieldRole, label_110);

        label_111 = new QLabel(layoutWidget_9);
        label_111->setObjectName(QString::fromUtf8("label_111"));

        formLayout_9->setWidget(14, QFormLayout::FieldRole, label_111);

        tabWidget->addTab(tab_8, QString());
#if QT_CONFIG(shortcut)
        label->setBuddy(rotate_x_slider);
        label_2->setBuddy(rotate_y_slider);
        label_3->setBuddy(rotate_z_slider);
        label_4->setBuddy(zoom_factor_spin_box);
        label_5->setBuddy(trans_x_spin_box);
        label_6->setBuddy(trans_y_spin_box);
        label_7->setBuddy(trans_z_spin_box);
        label_12->setBuddy(rotate_x_slider);
        label_8->setBuddy(rotate_x_slider);
        label_9->setBuddy(rotate_y_slider);
        label_10->setBuddy(rotate_z_slider);
        label_11->setBuddy(zoom_factor_spin_box);
        label_13->setBuddy(rotate_z_slider);
        label_14->setBuddy(rotate_x_slider);
        label_15->setBuddy(rotate_y_slider);
        label_16->setBuddy(rotate_z_slider);
        label_17->setBuddy(zoom_factor_spin_box);
        label_18->setBuddy(trans_x_spin_box);
        label_19->setBuddy(trans_y_spin_box);
        label_20->setBuddy(trans_z_spin_box);
        label_21->setBuddy(rotate_x_slider);
        label_22->setBuddy(zoom_factor_spin_box);
        label_23->setBuddy(rotate_x_slider);
        label_24->setBuddy(rotate_y_slider);
        label_25->setBuddy(rotate_z_slider);
        label_26->setBuddy(rotate_x_slider);
        label_27->setBuddy(rotate_y_slider);
        label_28->setBuddy(rotate_z_slider);
        label_29->setBuddy(zoom_factor_spin_box);
        label_30->setBuddy(trans_x_spin_box);
        label_31->setBuddy(trans_y_spin_box);
        label_32->setBuddy(trans_z_spin_box);
        label_33->setBuddy(trans_z_spin_box);
        label_34->setBuddy(trans_z_spin_box);
        label_35->setBuddy(trans_z_spin_box);
        label_36->setBuddy(trans_z_spin_box);
        label_73->setBuddy(rotate_x_slider);
        label_74->setBuddy(rotate_y_slider);
        label_75->setBuddy(rotate_z_slider);
        label_76->setBuddy(zoom_factor_spin_box);
        label_81->setBuddy(trans_z_spin_box);
        label_82->setBuddy(trans_z_spin_box);
        label_83->setBuddy(trans_z_spin_box);
        label_103->setBuddy(trans_z_spin_box);
        label_125->setBuddy(trans_z_spin_box);
        label_126->setBuddy(trans_z_spin_box);
        label_124->setBuddy(trans_z_spin_box);
        label_139->setBuddy(trans_z_spin_box);
        label_140->setBuddy(trans_z_spin_box);
        label_141->setBuddy(trans_z_spin_box);
        label_123->setBuddy(trans_z_spin_box);
        label_127->setBuddy(trans_z_spin_box);
        label_80->setBuddy(trans_z_spin_box);
        label_112->setBuddy(trans_z_spin_box);
        label_136->setBuddy(trans_z_spin_box);
        label_137->setBuddy(trans_z_spin_box);
        label_119->setBuddy(trans_z_spin_box);
        label_84->setBuddy(trans_z_spin_box);
        label_113->setBuddy(trans_z_spin_box);
        label_77->setBuddy(rotate_x_slider);
        label_78->setBuddy(rotate_y_slider);
        label_79->setBuddy(rotate_z_slider);
        label_128->setBuddy(zoom_factor_spin_box);
        label_129->setBuddy(trans_z_spin_box);
        label_130->setBuddy(trans_z_spin_box);
        label_131->setBuddy(trans_z_spin_box);
        label_132->setBuddy(trans_z_spin_box);
        label_133->setBuddy(trans_z_spin_box);
        label_134->setBuddy(trans_z_spin_box);
        label_135->setBuddy(trans_z_spin_box);
        label_88->setBuddy(trans_x_spin_box);
        label_89->setBuddy(trans_y_spin_box);
        label_90->setBuddy(trans_z_spin_box);
        label_95->setBuddy(zoom_factor_spin_box);
        label_96->setBuddy(rotate_x_slider);
        label_97->setBuddy(rotate_y_slider);
        label_98->setBuddy(rotate_z_slider);
        label_99->setBuddy(zoom_factor_spin_box);
        label_100->setBuddy(trans_x_spin_box);
        label_101->setBuddy(trans_y_spin_box);
        label_102->setBuddy(trans_z_spin_box);
        label_107->setBuddy(zoom_factor_spin_box);
        label_104->setBuddy(trans_y_spin_box);
        label_105->setBuddy(trans_y_spin_box);
        label_106->setBuddy(trans_y_spin_box);
        label_108->setBuddy(trans_y_spin_box);
        label_109->setBuddy(trans_y_spin_box);
        label_110->setBuddy(trans_y_spin_box);
        label_111->setBuddy(trans_y_spin_box);
#endif // QT_CONFIG(shortcut)

        retranslateUi(SideWidget);

        tabWidget->setCurrentIndex(5);
        toolBox_2->setCurrentIndex(0);
        toolBox_3->setCurrentIndex(0);
        shader_combobox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SideWidget);
    } // setupUi

    void retranslateUi(QWidget *SideWidget)
    {
        SideWidget->setWindowTitle(QCoreApplication::translate("SideWidget", "Form", nullptr));
        groupBox->setTitle(QString());
        label->setText(QCoreApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_2->setText(QCoreApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_3->setText(QCoreApplication::translate("SideWidget", "Rotate around z", nullptr));
        label_4->setText(QCoreApplication::translate("SideWidget", "Zoom factor", nullptr));
#if QT_CONFIG(tooltip)
        zoom_factor_spin_box->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        label_5->setText(QCoreApplication::translate("SideWidget", "Translate along x", nullptr));
        label_6->setText(QCoreApplication::translate("SideWidget", "Translate along y", nullptr));
        label_7->setText(QCoreApplication::translate("SideWidget", "Translate along z", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("SideWidget", "Default", nullptr));
        label_12->setText(QCoreApplication::translate("SideWidget", "Curve ", nullptr));
        label_8->setText(QCoreApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_9->setText(QCoreApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_10->setText(QCoreApplication::translate("SideWidget", "Rotate around z", nullptr));
        label_11->setText(QCoreApplication::translate("SideWidget", "Zoom factor", nullptr));
#if QT_CONFIG(tooltip)
        zoom_factor_spin_box_2->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        checkBox->setText(QCoreApplication::translate("SideWidget", "d1 Vissibility", nullptr));
        checkBox_2->setText(QCoreApplication::translate("SideWidget", "d2 Vissibility", nullptr));
        label_13->setText(QCoreApplication::translate("SideWidget", "Vector length", nullptr));
#if QT_CONFIG(tooltip)
        vector_length_spin_box->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("SideWidget", "Curves", nullptr));
        label_14->setText(QCoreApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_15->setText(QCoreApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_16->setText(QCoreApplication::translate("SideWidget", "Rotate around z", nullptr));
        label_17->setText(QCoreApplication::translate("SideWidget", "Zoom factor", nullptr));
#if QT_CONFIG(tooltip)
        zoom_factor_spin_box_3->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        label_18->setText(QCoreApplication::translate("SideWidget", "Translate along x", nullptr));
        label_19->setText(QCoreApplication::translate("SideWidget", "Translate along y", nullptr));
        label_20->setText(QCoreApplication::translate("SideWidget", "Translate along z", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("SideWidget", "Castle", nullptr));
        label_21->setText(QCoreApplication::translate("SideWidget", "Surface", nullptr));
        label_22->setText(QCoreApplication::translate("SideWidget", "Zoom factor", nullptr));
#if QT_CONFIG(tooltip)
        zoom_factor_spin_box_4->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        checkBox_3->setText(QCoreApplication::translate("SideWidget", "TEXTURE", nullptr));
        label_23->setText(QCoreApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_24->setText(QCoreApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_25->setText(QCoreApplication::translate("SideWidget", "Rotate around z", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("SideWidget", "Texture", nullptr));
        label_26->setText(QCoreApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_27->setText(QCoreApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_28->setText(QCoreApplication::translate("SideWidget", "Rotate around z", nullptr));
        label_29->setText(QCoreApplication::translate("SideWidget", "Zoom factor", nullptr));
#if QT_CONFIG(tooltip)
        zoom_factor_spin_box_5->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        label_30->setText(QCoreApplication::translate("SideWidget", "Translate along x", nullptr));
        label_31->setText(QCoreApplication::translate("SideWidget", "Translate along y", nullptr));
        label_32->setText(QCoreApplication::translate("SideWidget", "Translate along z", nullptr));
        label_33->setText(QCoreApplication::translate("SideWidget", "Point index", nullptr));
        label_34->setText(QCoreApplication::translate("SideWidget", "Move around x", nullptr));
        label_35->setText(QCoreApplication::translate("SideWidget", "Move around y", nullptr));
        label_36->setText(QCoreApplication::translate("SideWidget", "Move around z", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QCoreApplication::translate("SideWidget", "CyclicCurve", nullptr));
        groupBox_2->setTitle(QString());
        label_73->setText(QCoreApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_74->setText(QCoreApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_75->setText(QCoreApplication::translate("SideWidget", "Rotate around z", nullptr));
        label_76->setText(QCoreApplication::translate("SideWidget", "Zoom factor", nullptr));
#if QT_CONFIG(tooltip)
        zoom_factor_spin_box_11->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        label_81->setText(QCoreApplication::translate("SideWidget", "Move around x", nullptr));
        label_82->setText(QCoreApplication::translate("SideWidget", "Move around y", nullptr));
        label_83->setText(QCoreApplication::translate("SideWidget", "Move around z", nullptr));
        label_103->setText(QCoreApplication::translate("SideWidget", "Vector length", nullptr));
        checkBox_6->setText(QCoreApplication::translate("SideWidget", "d2", nullptr));
        checkBox_5->setText(QCoreApplication::translate("SideWidget", "d1", nullptr));
        checkBox_move_all_arc->setText(QCoreApplication::translate("SideWidget", "move all", nullptr));
        label_125->setText(QCoreApplication::translate("SideWidget", "Move  y", nullptr));
        label_126->setText(QCoreApplication::translate("SideWidget", "Move z", nullptr));
        label_124->setText(QCoreApplication::translate("SideWidget", "Move  x", nullptr));
        label_139->setText(QCoreApplication::translate("SideWidget", "Trans x", nullptr));
        label_140->setText(QCoreApplication::translate("SideWidget", "Trans y", nullptr));
        label_141->setText(QCoreApplication::translate("SideWidget", "Trans z", nullptr));
        toolBox_2->setItemText(toolBox_2->indexOf(page_4), QCoreApplication::translate("SideWidget", "Show", nullptr));
        comboBox_join->setItemText(0, QCoreApplication::translate("SideWidget", "Join", nullptr));
        comboBox_join->setItemText(1, QCoreApplication::translate("SideWidget", "Merge", nullptr));
        comboBox_join->setItemText(2, QCoreApplication::translate("SideWidget", "Continue", nullptr));

        comboBox_direction->setItemText(0, QCoreApplication::translate("SideWidget", "RIGHT-RIGHT", nullptr));
        comboBox_direction->setItemText(1, QCoreApplication::translate("SideWidget", "RIGHT-LEFT", nullptr));
        comboBox_direction->setItemText(2, QCoreApplication::translate("SideWidget", "LEFT-RIGHT", nullptr));
        comboBox_direction->setItemText(3, QCoreApplication::translate("SideWidget", "LEFT-LEFT", nullptr));

        pushButton_submit->setText(QCoreApplication::translate("SideWidget", "Submit", nullptr));
        label_123->setText(QCoreApplication::translate("SideWidget", "1. Arc index", nullptr));
        label_127->setText(QCoreApplication::translate("SideWidget", "2. Arc index", nullptr));
        toolBox_2->setItemText(toolBox_2->indexOf(page_5), QCoreApplication::translate("SideWidget", "Add new", nullptr));
        label_80->setText(QCoreApplication::translate("SideWidget", "Point index", nullptr));
        label_112->setText(QCoreApplication::translate("SideWidget", "Arc index", nullptr));
        pushButton->setText(QCoreApplication::translate("SideWidget", "Add new arc", nullptr));
        pushButton_reset->setText(QCoreApplication::translate("SideWidget", "Reset", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_6), QCoreApplication::translate("SideWidget", "HermiteArc", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("SideWidget", "New patch", nullptr));
        label_136->setText(QCoreApplication::translate("SideWidget", "1. Patch index", nullptr));
        label_137->setText(QCoreApplication::translate("SideWidget", "2. Patch index", nullptr));
        comboBox_join_2->setItemText(0, QCoreApplication::translate("SideWidget", "Join", nullptr));
        comboBox_join_2->setItemText(1, QCoreApplication::translate("SideWidget", "Merge", nullptr));
        comboBox_join_2->setItemText(2, QCoreApplication::translate("SideWidget", "Extend", nullptr));

        pushButton_submit_2->setText(QCoreApplication::translate("SideWidget", "Submit", nullptr));
        comboBox_direction_2->setItemText(0, QCoreApplication::translate("SideWidget", "East - West", "0"));
        comboBox_direction_2->setItemText(1, QCoreApplication::translate("SideWidget", "West - East", "1"));
        comboBox_direction_2->setItemText(2, QCoreApplication::translate("SideWidget", "North - South", "2"));
        comboBox_direction_2->setItemText(3, QCoreApplication::translate("SideWidget", "South - North", "3"));
        comboBox_direction_2->setItemText(4, QCoreApplication::translate("SideWidget", "NorthEast - SouthWest", "4"));
        comboBox_direction_2->setItemText(5, QCoreApplication::translate("SideWidget", "SouthWest - NorthEast", "5"));
        comboBox_direction_2->setItemText(6, QCoreApplication::translate("SideWidget", "NorthWest - SouthEast", "6"));
        comboBox_direction_2->setItemText(7, QCoreApplication::translate("SideWidget", "SouthEast - NorthWest", "7"));

        comboBox_direction_extend->setItemText(0, QCoreApplication::translate("SideWidget", "North", "0"));
        comboBox_direction_extend->setItemText(1, QCoreApplication::translate("SideWidget", "North-East", "1"));
        comboBox_direction_extend->setItemText(2, QCoreApplication::translate("SideWidget", "East", "2"));
        comboBox_direction_extend->setItemText(3, QCoreApplication::translate("SideWidget", "South-East", "3"));
        comboBox_direction_extend->setItemText(4, QCoreApplication::translate("SideWidget", "South", "4"));
        comboBox_direction_extend->setItemText(5, QCoreApplication::translate("SideWidget", "South-West", "5"));
        comboBox_direction_extend->setItemText(6, QCoreApplication::translate("SideWidget", "West", "6"));
        comboBox_direction_extend->setItemText(7, QCoreApplication::translate("SideWidget", "North-West", "7"));

        label_119->setText(QCoreApplication::translate("SideWidget", "Patch index", nullptr));
        label_84->setText(QCoreApplication::translate("SideWidget", "Point index", nullptr));
        label_113->setText(QCoreApplication::translate("SideWidget", "Vector", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("SideWidget", "Patch Operations", nullptr));
        label_77->setText(QCoreApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_78->setText(QCoreApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_79->setText(QCoreApplication::translate("SideWidget", "Rotate around z", nullptr));
        label_128->setText(QCoreApplication::translate("SideWidget", "Zoom factor", nullptr));
#if QT_CONFIG(tooltip)
        zoom_factor_spin_box_14->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        label_129->setText(QCoreApplication::translate("SideWidget", "Move around x", nullptr));
        label_130->setText(QCoreApplication::translate("SideWidget", "Move around y", nullptr));
        label_131->setText(QCoreApplication::translate("SideWidget", "Move around z", nullptr));
        label_132->setText(QCoreApplication::translate("SideWidget", "Vector length", nullptr));
        label_133->setText(QCoreApplication::translate("SideWidget", "Move x", nullptr));
        label_134->setText(QCoreApplication::translate("SideWidget", "Move y", nullptr));
        label_135->setText(QCoreApplication::translate("SideWidget", "Move z", nullptr));
        label_88->setText(QCoreApplication::translate("SideWidget", "Translate along x", nullptr));
        label_89->setText(QCoreApplication::translate("SideWidget", "Translate along y", nullptr));
        label_90->setText(QCoreApplication::translate("SideWidget", "Translate along z", nullptr));
        label_95->setText(QCoreApplication::translate("SideWidget", "Material of new patch    ", nullptr));
        comboBox_material->setItemText(0, QCoreApplication::translate("SideWidget", "Brass", nullptr));
        comboBox_material->setItemText(1, QCoreApplication::translate("SideWidget", "Gold ", nullptr));
        comboBox_material->setItemText(2, QCoreApplication::translate("SideWidget", "Silver ", nullptr));
        comboBox_material->setItemText(3, QCoreApplication::translate("SideWidget", "Emerald ", nullptr));
        comboBox_material->setItemText(4, QCoreApplication::translate("SideWidget", "Pearl ", nullptr));
        comboBox_material->setItemText(5, QCoreApplication::translate("SideWidget", "Ruby ", nullptr));
        comboBox_material->setItemText(6, QCoreApplication::translate("SideWidget", "Turquoise", nullptr));

        checkBox_d0u->setText(QCoreApplication::translate("SideWidget", "d0/u", nullptr));
        checkBox_d0v->setText(QCoreApplication::translate("SideWidget", "d0/v", nullptr));
        checkBox_d1u->setText(QCoreApplication::translate("SideWidget", "d1/u", nullptr));
        checkBox_d1v->setText(QCoreApplication::translate("SideWidget", "d1/v", nullptr));
        pushButton_reset_2->setText(QCoreApplication::translate("SideWidget", "Reset", nullptr));
        pushButton_newPatch->setText(QCoreApplication::translate("SideWidget", "New patch", nullptr));
        checkBox_vectors->setText(QCoreApplication::translate("SideWidget", "show vectors", nullptr));
        checkBox_move_all->setText(QCoreApplication::translate("SideWidget", "move all", nullptr));
        checkBox_d2u->setText(QCoreApplication::translate("SideWidget", "d2/u", nullptr));
        checkBox_d2v->setText(QCoreApplication::translate("SideWidget", "d2/v", nullptr));
        toolBox_3->setItemText(toolBox_3->indexOf(page), QCoreApplication::translate("SideWidget", "Patch operations", nullptr));
        shader_combobox->setItemText(0, QCoreApplication::translate("SideWidget", "Directional Lighting", nullptr));
        shader_combobox->setItemText(1, QCoreApplication::translate("SideWidget", "Two sided Lighting", nullptr));
        shader_combobox->setItemText(2, QCoreApplication::translate("SideWidget", "Toon vert", nullptr));
        shader_combobox->setItemText(3, QCoreApplication::translate("SideWidget", "Reflected lines", nullptr));

        label_85->setText(QCoreApplication::translate("SideWidget", "Smoothing:", nullptr));
        label_86->setText(QCoreApplication::translate("SideWidget", "Shading:", nullptr));
        alpha_on_checkbox->setText(QCoreApplication::translate("SideWidget", "alpha", nullptr));
        label_87->setText(QCoreApplication::translate("SideWidget", "Toonify", nullptr));
        label_91->setText(QCoreApplication::translate("SideWidget", "A:", nullptr));
        label_92->setText(QCoreApplication::translate("SideWidget", "Reflection Lines", nullptr));
        label_93->setText(QCoreApplication::translate("SideWidget", "B:", nullptr));
        label_94->setText(QCoreApplication::translate("SideWidget", "G:", nullptr));
        label_122->setText(QCoreApplication::translate("SideWidget", "R:", nullptr));
        label_138->setText(QCoreApplication::translate("SideWidget", "Scale factor:", nullptr));
        toolBox_3->setItemText(toolBox_3->indexOf(page_3), QCoreApplication::translate("SideWidget", "Shaders", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_7), QCoreApplication::translate("SideWidget", "HermitePatch", nullptr));
        label_96->setText(QCoreApplication::translate("SideWidget", "Rotate around x", nullptr));
        label_97->setText(QCoreApplication::translate("SideWidget", "Rotate around y", nullptr));
        label_98->setText(QCoreApplication::translate("SideWidget", "Rotate around z", nullptr));
        label_99->setText(QCoreApplication::translate("SideWidget", "Zoom factor", nullptr));
#if QT_CONFIG(tooltip)
        zoom_factor_spin_box_13->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
        label_100->setText(QCoreApplication::translate("SideWidget", "Translate along x", nullptr));
        label_101->setText(QCoreApplication::translate("SideWidget", "Translate along y", nullptr));
        label_102->setText(QCoreApplication::translate("SideWidget", "Translate along z", nullptr));
        label_107->setText(QCoreApplication::translate("SideWidget", "Shader slider", nullptr));
        label_104->setText(QCoreApplication::translate("SideWidget", "Rgb_R", nullptr));
        label_105->setText(QCoreApplication::translate("SideWidget", "Rgb_G", nullptr));
        label_106->setText(QCoreApplication::translate("SideWidget", "Rgb_B", nullptr));
        label_108->setText(QCoreApplication::translate("SideWidget", "Intensity", nullptr));
        label_109->setText(QCoreApplication::translate("SideWidget", "Scale_factor", nullptr));
        label_110->setText(QCoreApplication::translate("SideWidget", "Smoothing", nullptr));
        label_111->setText(QCoreApplication::translate("SideWidget", "Shading", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_8), QCoreApplication::translate("SideWidget", "Shaders", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SideWidget: public Ui_SideWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIDEWIDGET_H
