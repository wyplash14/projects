/****************************************************************************
** Meta object code from reading C++ file 'GLWidget.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.0.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../../RoyalTeam/vege/royal-team-sm/QtFramework/GUI/GLWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GLWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.0.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_cagd__GLWidget_t {
    const uint offsetsAndSize[272];
    char stringdata0[2217];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_cagd__GLWidget_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_cagd__GLWidget_t qt_meta_stringdata_cagd__GLWidget = {
    {
QT_MOC_LITERAL(0, 14), // "cagd::GLWidget"
QT_MOC_LITERAL(15, 13), // "xPointChanged"
QT_MOC_LITERAL(29, 0), // ""
QT_MOC_LITERAL(30, 5), // "value"
QT_MOC_LITERAL(36, 13), // "yPointChanged"
QT_MOC_LITERAL(50, 13), // "zPointChanged"
QT_MOC_LITERAL(64, 20), // "xHermitePointChanged"
QT_MOC_LITERAL(85, 20), // "yHermitePointChanged"
QT_MOC_LITERAL(106, 20), // "zHermitePointChanged"
QT_MOC_LITERAL(127, 19), // "vectorLengthChanged"
QT_MOC_LITERAL(147, 16), // "hp3xPointChanged"
QT_MOC_LITERAL(164, 16), // "hp3yPointChanged"
QT_MOC_LITERAL(181, 16), // "hp3zPointChanged"
QT_MOC_LITERAL(198, 16), // "ha3xPointChanged"
QT_MOC_LITERAL(215, 16), // "ha3yPointChanged"
QT_MOC_LITERAL(232, 16), // "ha3zPointChanged"
QT_MOC_LITERAL(249, 15), // "arcIndexChanged"
QT_MOC_LITERAL(265, 16), // "arcIndexChanged1"
QT_MOC_LITERAL(282, 16), // "arcIndexChanged2"
QT_MOC_LITERAL(299, 18), // "patch_cp_x_changed"
QT_MOC_LITERAL(318, 18), // "patch_cp_y_changed"
QT_MOC_LITERAL(337, 18), // "patch_cp_z_changed"
QT_MOC_LITERAL(356, 17), // "patchIndexChanged"
QT_MOC_LITERAL(374, 18), // "patchIndex1Changed"
QT_MOC_LITERAL(393, 18), // "patchIndex2Changed"
QT_MOC_LITERAL(412, 23), // "pairedDirectionsEnabled"
QT_MOC_LITERAL(436, 7), // "enabled"
QT_MOC_LITERAL(444, 20), // "extDirectionsEnabled"
QT_MOC_LITERAL(465, 13), // "submitEnabled"
QT_MOC_LITERAL(479, 15), // "resetEverything"
QT_MOC_LITERAL(495, 11), // "set_angle_x"
QT_MOC_LITERAL(507, 11), // "set_angle_y"
QT_MOC_LITERAL(519, 11), // "set_angle_z"
QT_MOC_LITERAL(531, 15), // "set_zoom_factor"
QT_MOC_LITERAL(547, 11), // "set_trans_x"
QT_MOC_LITERAL(559, 11), // "set_trans_y"
QT_MOC_LITERAL(571, 11), // "set_trans_z"
QT_MOC_LITERAL(583, 23), // "setParametricCurveIndex"
QT_MOC_LITERAL(607, 5), // "index"
QT_MOC_LITERAL(613, 15), // "setSurfaceIndex"
QT_MOC_LITERAL(629, 23), // "setVisibilityOfTangents"
QT_MOC_LITERAL(653, 10), // "visibility"
QT_MOC_LITERAL(664, 34), // "setVisibilityOfAccelerationVe..."
QT_MOC_LITERAL(699, 14), // "textureEnabled"
QT_MOC_LITERAL(714, 7), // "texture"
QT_MOC_LITERAL(722, 14), // "setTextureType"
QT_MOC_LITERAL(737, 13), // "setHomeworkID"
QT_MOC_LITERAL(751, 2), // "id"
QT_MOC_LITERAL(754, 9), // "set_Scale"
QT_MOC_LITERAL(764, 5), // "scale"
QT_MOC_LITERAL(770, 11), // "_drawCastle"
QT_MOC_LITERAL(782, 10), // "_drawWalls"
QT_MOC_LITERAL(793, 8), // "_drawDor"
QT_MOC_LITERAL(802, 10), // "_drawTower"
QT_MOC_LITERAL(813, 12), // "_drawFigures"
QT_MOC_LITERAL(826, 8), // "_animate"
QT_MOC_LITERAL(835, 12), // "_animateStar"
QT_MOC_LITERAL(848, 9), // "setPointx"
QT_MOC_LITERAL(858, 9), // "setPointy"
QT_MOC_LITERAL(868, 9), // "setPointz"
QT_MOC_LITERAL(878, 13), // "setPointindex"
QT_MOC_LITERAL(892, 16), // "setHermitePointx"
QT_MOC_LITERAL(909, 16), // "setHermitePointy"
QT_MOC_LITERAL(926, 16), // "setHermitePointz"
QT_MOC_LITERAL(943, 20), // "setHermitePointindex"
QT_MOC_LITERAL(964, 21), // "cyclicInterpolEnabled"
QT_MOC_LITERAL(986, 15), // "cyclicD1Enabled"
QT_MOC_LITERAL(1002, 15), // "cyclicD2Enabled"
QT_MOC_LITERAL(1018, 16), // "setRotationAngle"
QT_MOC_LITERAL(1035, 5), // "angle"
QT_MOC_LITERAL(1041, 14), // "setShowVectors"
QT_MOC_LITERAL(1056, 1), // "v"
QT_MOC_LITERAL(1058, 16), // "set_shader_index"
QT_MOC_LITERAL(1075, 16), // "set_toon_color_r"
QT_MOC_LITERAL(1092, 16), // "set_toon_color_g"
QT_MOC_LITERAL(1109, 16), // "set_toon_color_b"
QT_MOC_LITERAL(1126, 16), // "set_toon_color_a"
QT_MOC_LITERAL(1143, 14), // "set_show_alpha"
QT_MOC_LITERAL(1158, 16), // "set_scale_factor"
QT_MOC_LITERAL(1175, 13), // "set_smoothing"
QT_MOC_LITERAL(1189, 11), // "set_shading"
QT_MOC_LITERAL(1201, 32), // "set_hp3_interpolation_visibility"
QT_MOC_LITERAL(1234, 21), // "set_hp3_d1_visibility"
QT_MOC_LITERAL(1256, 21), // "set_hp3_d2_visibility"
QT_MOC_LITERAL(1278, 26), // "set_hp3_vectors_visibility"
QT_MOC_LITERAL(1305, 20), // "set_rotation_angle_x"
QT_MOC_LITERAL(1326, 20), // "set_rotation_angle_y"
QT_MOC_LITERAL(1347, 20), // "set_rotation_angle_z"
QT_MOC_LITERAL(1368, 17), // "set_vector_length"
QT_MOC_LITERAL(1386, 18), // "shaderIndexChanged"
QT_MOC_LITERAL(1405, 24), // "hermitePatchIndexChanged"
QT_MOC_LITERAL(1430, 9), // "set_hp3_x"
QT_MOC_LITERAL(1440, 9), // "set_hp3_y"
QT_MOC_LITERAL(1450, 9), // "set_hp3_z"
QT_MOC_LITERAL(1460, 21), // "set_HermiteArcPoint_x"
QT_MOC_LITERAL(1482, 21), // "set_HermiteArcPoint_y"
QT_MOC_LITERAL(1504, 21), // "set_HermiteArcPoint_z"
QT_MOC_LITERAL(1526, 10), // "saveToFile"
QT_MOC_LITERAL(1537, 12), // "loadFromFile"
QT_MOC_LITERAL(1550, 21), // "set_Hermite_Arc_index"
QT_MOC_LITERAL(1572, 16), // "addNewHermiteArc"
QT_MOC_LITERAL(1589, 23), // "set_Hermite_Arc_index_1"
QT_MOC_LITERAL(1613, 23), // "set_Hermite_Arc_index_2"
QT_MOC_LITERAL(1637, 17), // "set_Method_number"
QT_MOC_LITERAL(1655, 20), // "set_Direction_number"
QT_MOC_LITERAL(1676, 12), // "manage_event"
QT_MOC_LITERAL(1689, 5), // "reset"
QT_MOC_LITERAL(1695, 16), // "updatePatchIndex"
QT_MOC_LITERAL(1712, 25), // "updatePatchSelectedVertex"
QT_MOC_LITERAL(1738, 23), // "set_HermitePatchPoint_x"
QT_MOC_LITERAL(1762, 23), // "set_HermitePatchPoint_y"
QT_MOC_LITERAL(1786, 23), // "set_HermitePatchPoint_z"
QT_MOC_LITERAL(1810, 17), // "updatePatchIndex1"
QT_MOC_LITERAL(1828, 17), // "updatePatchIndex2"
QT_MOC_LITERAL(1846, 15), // "updatePatchDir1"
QT_MOC_LITERAL(1862, 15), // "updatePatchDir2"
QT_MOC_LITERAL(1878, 17), // "updatePatchMethod"
QT_MOC_LITERAL(1896, 15), // "updateDirection"
QT_MOC_LITERAL(1912, 19), // "updateDirectionPair"
QT_MOC_LITERAL(1932, 14), // "updateMaterial"
QT_MOC_LITERAL(1947, 13), // "onPatchSubmit"
QT_MOC_LITERAL(1961, 11), // "addNewPatch"
QT_MOC_LITERAL(1973, 12), // "resetPatches"
QT_MOC_LITERAL(1986, 13), // "showPatch_d0u"
QT_MOC_LITERAL(2000, 13), // "showPatch_d0v"
QT_MOC_LITERAL(2014, 13), // "showPatch_d1u"
QT_MOC_LITERAL(2028, 13), // "showPatch_d1v"
QT_MOC_LITERAL(2042, 13), // "showPatch_d2u"
QT_MOC_LITERAL(2056, 13), // "showPatch_d2v"
QT_MOC_LITERAL(2070, 20), // "setPatchVectorLength"
QT_MOC_LITERAL(2091, 20), // "setPatchVectorNumber"
QT_MOC_LITERAL(2112, 24), // "setPatchRotationalAngleX"
QT_MOC_LITERAL(2137, 24), // "setPatchRotationalAngleY"
QT_MOC_LITERAL(2162, 24), // "setPatchRotationalAngleZ"
QT_MOC_LITERAL(2187, 12), // "set_move_all"
QT_MOC_LITERAL(2200, 16) // "set_move_all_arc"

    },
    "cagd::GLWidget\0xPointChanged\0\0value\0"
    "yPointChanged\0zPointChanged\0"
    "xHermitePointChanged\0yHermitePointChanged\0"
    "zHermitePointChanged\0vectorLengthChanged\0"
    "hp3xPointChanged\0hp3yPointChanged\0"
    "hp3zPointChanged\0ha3xPointChanged\0"
    "ha3yPointChanged\0ha3zPointChanged\0"
    "arcIndexChanged\0arcIndexChanged1\0"
    "arcIndexChanged2\0patch_cp_x_changed\0"
    "patch_cp_y_changed\0patch_cp_z_changed\0"
    "patchIndexChanged\0patchIndex1Changed\0"
    "patchIndex2Changed\0pairedDirectionsEnabled\0"
    "enabled\0extDirectionsEnabled\0submitEnabled\0"
    "resetEverything\0set_angle_x\0set_angle_y\0"
    "set_angle_z\0set_zoom_factor\0set_trans_x\0"
    "set_trans_y\0set_trans_z\0setParametricCurveIndex\0"
    "index\0setSurfaceIndex\0setVisibilityOfTangents\0"
    "visibility\0setVisibilityOfAccelerationVectors\0"
    "textureEnabled\0texture\0setTextureType\0"
    "setHomeworkID\0id\0set_Scale\0scale\0"
    "_drawCastle\0_drawWalls\0_drawDor\0"
    "_drawTower\0_drawFigures\0_animate\0"
    "_animateStar\0setPointx\0setPointy\0"
    "setPointz\0setPointindex\0setHermitePointx\0"
    "setHermitePointy\0setHermitePointz\0"
    "setHermitePointindex\0cyclicInterpolEnabled\0"
    "cyclicD1Enabled\0cyclicD2Enabled\0"
    "setRotationAngle\0angle\0setShowVectors\0"
    "v\0set_shader_index\0set_toon_color_r\0"
    "set_toon_color_g\0set_toon_color_b\0"
    "set_toon_color_a\0set_show_alpha\0"
    "set_scale_factor\0set_smoothing\0"
    "set_shading\0set_hp3_interpolation_visibility\0"
    "set_hp3_d1_visibility\0set_hp3_d2_visibility\0"
    "set_hp3_vectors_visibility\0"
    "set_rotation_angle_x\0set_rotation_angle_y\0"
    "set_rotation_angle_z\0set_vector_length\0"
    "shaderIndexChanged\0hermitePatchIndexChanged\0"
    "set_hp3_x\0set_hp3_y\0set_hp3_z\0"
    "set_HermiteArcPoint_x\0set_HermiteArcPoint_y\0"
    "set_HermiteArcPoint_z\0saveToFile\0"
    "loadFromFile\0set_Hermite_Arc_index\0"
    "addNewHermiteArc\0set_Hermite_Arc_index_1\0"
    "set_Hermite_Arc_index_2\0set_Method_number\0"
    "set_Direction_number\0manage_event\0"
    "reset\0updatePatchIndex\0updatePatchSelectedVertex\0"
    "set_HermitePatchPoint_x\0set_HermitePatchPoint_y\0"
    "set_HermitePatchPoint_z\0updatePatchIndex1\0"
    "updatePatchIndex2\0updatePatchDir1\0"
    "updatePatchDir2\0updatePatchMethod\0"
    "updateDirection\0updateDirectionPair\0"
    "updateMaterial\0onPatchSubmit\0addNewPatch\0"
    "resetPatches\0showPatch_d0u\0showPatch_d0v\0"
    "showPatch_d1u\0showPatch_d1v\0showPatch_d2u\0"
    "showPatch_d2v\0setPatchVectorLength\0"
    "setPatchVectorNumber\0setPatchRotationalAngleX\0"
    "setPatchRotationalAngleY\0"
    "setPatchRotationalAngleZ\0set_move_all\0"
    "set_move_all_arc"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_cagd__GLWidget[] = {

 // content:
       9,       // revision
       0,       // classname
       0,    0, // classinfo
     125,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      26,       // signalCount

 // signals: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,  764,    2, 0x06,    0 /* Public */,
       4,    1,  767,    2, 0x06,    2 /* Public */,
       5,    1,  770,    2, 0x06,    4 /* Public */,
       6,    1,  773,    2, 0x06,    6 /* Public */,
       7,    1,  776,    2, 0x06,    8 /* Public */,
       8,    1,  779,    2, 0x06,   10 /* Public */,
       9,    1,  782,    2, 0x06,   12 /* Public */,
      10,    1,  785,    2, 0x06,   14 /* Public */,
      11,    1,  788,    2, 0x06,   16 /* Public */,
      12,    1,  791,    2, 0x06,   18 /* Public */,
      13,    1,  794,    2, 0x06,   20 /* Public */,
      14,    1,  797,    2, 0x06,   22 /* Public */,
      15,    1,  800,    2, 0x06,   24 /* Public */,
      16,    1,  803,    2, 0x06,   26 /* Public */,
      17,    1,  806,    2, 0x06,   28 /* Public */,
      18,    1,  809,    2, 0x06,   30 /* Public */,
      19,    1,  812,    2, 0x06,   32 /* Public */,
      20,    1,  815,    2, 0x06,   34 /* Public */,
      21,    1,  818,    2, 0x06,   36 /* Public */,
      22,    1,  821,    2, 0x06,   38 /* Public */,
      23,    1,  824,    2, 0x06,   40 /* Public */,
      24,    1,  827,    2, 0x06,   42 /* Public */,
      25,    1,  830,    2, 0x06,   44 /* Public */,
      27,    1,  833,    2, 0x06,   46 /* Public */,
      28,    1,  836,    2, 0x06,   48 /* Public */,
      29,    1,  839,    2, 0x06,   50 /* Public */,

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
      30,    1,  842,    2, 0x0a,   52 /* Public */,
      31,    1,  845,    2, 0x0a,   54 /* Public */,
      32,    1,  848,    2, 0x0a,   56 /* Public */,
      33,    1,  851,    2, 0x0a,   58 /* Public */,
      34,    1,  854,    2, 0x0a,   60 /* Public */,
      35,    1,  857,    2, 0x0a,   62 /* Public */,
      36,    1,  860,    2, 0x0a,   64 /* Public */,
      37,    1,  863,    2, 0x0a,   66 /* Public */,
      39,    1,  866,    2, 0x0a,   68 /* Public */,
      40,    1,  869,    2, 0x0a,   70 /* Public */,
      42,    1,  872,    2, 0x0a,   72 /* Public */,
      43,    1,  875,    2, 0x0a,   74 /* Public */,
      45,    1,  878,    2, 0x0a,   76 /* Public */,
      46,    1,  881,    2, 0x0a,   78 /* Public */,
      48,    1,  884,    2, 0x0a,   80 /* Public */,
      50,    0,  887,    2, 0x0a,   82 /* Public */,
      51,    0,  888,    2, 0x0a,   83 /* Public */,
      52,    0,  889,    2, 0x0a,   84 /* Public */,
      53,    0,  890,    2, 0x0a,   85 /* Public */,
      54,    0,  891,    2, 0x0a,   86 /* Public */,
      55,    0,  892,    2, 0x0a,   87 /* Public */,
      56,    0,  893,    2, 0x0a,   88 /* Public */,
      57,    1,  894,    2, 0x0a,   89 /* Public */,
      58,    1,  897,    2, 0x0a,   91 /* Public */,
      59,    1,  900,    2, 0x0a,   93 /* Public */,
      60,    1,  903,    2, 0x0a,   95 /* Public */,
      61,    1,  906,    2, 0x0a,   97 /* Public */,
      62,    1,  909,    2, 0x0a,   99 /* Public */,
      63,    1,  912,    2, 0x0a,  101 /* Public */,
      64,    1,  915,    2, 0x0a,  103 /* Public */,
      65,    1,  918,    2, 0x0a,  105 /* Public */,
      66,    1,  921,    2, 0x0a,  107 /* Public */,
      67,    1,  924,    2, 0x0a,  109 /* Public */,
      68,    1,  927,    2, 0x0a,  111 /* Public */,
      70,    1,  930,    2, 0x0a,  113 /* Public */,
      72,    1,  933,    2, 0x0a,  115 /* Public */,
      73,    1,  936,    2, 0x0a,  117 /* Public */,
      74,    1,  939,    2, 0x0a,  119 /* Public */,
      75,    1,  942,    2, 0x0a,  121 /* Public */,
      76,    1,  945,    2, 0x0a,  123 /* Public */,
      77,    1,  948,    2, 0x0a,  125 /* Public */,
      78,    1,  951,    2, 0x0a,  127 /* Public */,
      79,    1,  954,    2, 0x0a,  129 /* Public */,
      80,    1,  957,    2, 0x0a,  131 /* Public */,
      81,    1,  960,    2, 0x0a,  133 /* Public */,
      82,    1,  963,    2, 0x0a,  135 /* Public */,
      83,    1,  966,    2, 0x0a,  137 /* Public */,
      84,    1,  969,    2, 0x0a,  139 /* Public */,
      85,    1,  972,    2, 0x0a,  141 /* Public */,
      86,    1,  975,    2, 0x0a,  143 /* Public */,
      87,    1,  978,    2, 0x0a,  145 /* Public */,
      88,    1,  981,    2, 0x0a,  147 /* Public */,
      89,    1,  984,    2, 0x0a,  149 /* Public */,
      90,    1,  987,    2, 0x0a,  151 /* Public */,
      91,    1,  990,    2, 0x0a,  153 /* Public */,
      92,    1,  993,    2, 0x0a,  155 /* Public */,
      93,    1,  996,    2, 0x0a,  157 /* Public */,
      94,    1,  999,    2, 0x0a,  159 /* Public */,
      95,    1, 1002,    2, 0x0a,  161 /* Public */,
      96,    1, 1005,    2, 0x0a,  163 /* Public */,
      97,    0, 1008,    2, 0x0a,  165 /* Public */,
      98,    0, 1009,    2, 0x0a,  166 /* Public */,
      99,    1, 1010,    2, 0x0a,  167 /* Public */,
     100,    0, 1013,    2, 0x0a,  169 /* Public */,
     101,    1, 1014,    2, 0x0a,  170 /* Public */,
     102,    1, 1017,    2, 0x0a,  172 /* Public */,
     103,    1, 1020,    2, 0x0a,  174 /* Public */,
     104,    1, 1023,    2, 0x0a,  176 /* Public */,
     105,    0, 1026,    2, 0x0a,  178 /* Public */,
     106,    0, 1027,    2, 0x0a,  179 /* Public */,
     107,    1, 1028,    2, 0x0a,  180 /* Public */,
     108,    1, 1031,    2, 0x0a,  182 /* Public */,
     109,    1, 1034,    2, 0x0a,  184 /* Public */,
     110,    1, 1037,    2, 0x0a,  186 /* Public */,
     111,    1, 1040,    2, 0x0a,  188 /* Public */,
     112,    1, 1043,    2, 0x0a,  190 /* Public */,
     113,    1, 1046,    2, 0x0a,  192 /* Public */,
     114,    1, 1049,    2, 0x0a,  194 /* Public */,
     115,    1, 1052,    2, 0x0a,  196 /* Public */,
     116,    1, 1055,    2, 0x0a,  198 /* Public */,
     117,    1, 1058,    2, 0x0a,  200 /* Public */,
     118,    1, 1061,    2, 0x0a,  202 /* Public */,
     119,    1, 1064,    2, 0x0a,  204 /* Public */,
     120,    0, 1067,    2, 0x0a,  206 /* Public */,
     121,    0, 1068,    2, 0x0a,  207 /* Public */,
     122,    0, 1069,    2, 0x0a,  208 /* Public */,
     123,    1, 1070,    2, 0x0a,  209 /* Public */,
     124,    1, 1073,    2, 0x0a,  211 /* Public */,
     125,    1, 1076,    2, 0x0a,  213 /* Public */,
     126,    1, 1079,    2, 0x0a,  215 /* Public */,
     127,    1, 1082,    2, 0x0a,  217 /* Public */,
     128,    1, 1085,    2, 0x0a,  219 /* Public */,
     129,    1, 1088,    2, 0x0a,  221 /* Public */,
     130,    1, 1091,    2, 0x0a,  223 /* Public */,
     131,    1, 1094,    2, 0x0a,  225 /* Public */,
     132,    1, 1097,    2, 0x0a,  227 /* Public */,
     133,    1, 1100,    2, 0x0a,  229 /* Public */,
     134,    1, 1103,    2, 0x0a,  231 /* Public */,
     135,    1, 1106,    2, 0x0a,  233 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Bool,   26,
    QMetaType::Void, QMetaType::Bool,   26,
    QMetaType::Void, QMetaType::Bool,   26,
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   41,
    QMetaType::Void, QMetaType::Int,   41,
    QMetaType::Void, QMetaType::Int,   44,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,   47,
    QMetaType::Void, QMetaType::Double,   49,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,   38,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,   38,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,   69,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Double,   71,
    QMetaType::Void, QMetaType::Double,   71,
    QMetaType::Void, QMetaType::Double,   71,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void, QMetaType::Int,   38,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   41,
    QMetaType::Void, QMetaType::Int,   41,
    QMetaType::Void, QMetaType::Int,   41,
    QMetaType::Void, QMetaType::Int,   41,
    QMetaType::Void, QMetaType::Int,   41,
    QMetaType::Void, QMetaType::Int,   41,
    QMetaType::Void, QMetaType::Double,   71,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,   71,
    QMetaType::Void, QMetaType::Int,   71,

       0        // eod
};

void cagd::GLWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<GLWidget *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->xPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 1: _t->yPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 2: _t->zPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 3: _t->xHermitePointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 4: _t->yHermitePointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->zHermitePointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 6: _t->vectorLengthChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 7: _t->hp3xPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->hp3yPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 9: _t->hp3zPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: _t->ha3xPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 11: _t->ha3yPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 12: _t->ha3zPointChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 13: _t->arcIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->arcIndexChanged1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->arcIndexChanged2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->patch_cp_x_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 17: _t->patch_cp_y_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->patch_cp_z_changed((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 19: _t->patchIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->patchIndex1Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->patchIndex2Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->pairedDirectionsEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->extDirectionsEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 24: _t->submitEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->resetEverything((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->set_angle_x((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->set_angle_y((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 28: _t->set_angle_z((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 29: _t->set_zoom_factor((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 30: _t->set_trans_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 31: _t->set_trans_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 32: _t->set_trans_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 33: _t->setParametricCurveIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 34: _t->setSurfaceIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 35: _t->setVisibilityOfTangents((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 36: _t->setVisibilityOfAccelerationVectors((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 37: _t->textureEnabled((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 38: _t->setTextureType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 39: _t->setHomeworkID((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 40: _t->set_Scale((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 41: _t->_drawCastle(); break;
        case 42: _t->_drawWalls(); break;
        case 43: _t->_drawDor(); break;
        case 44: _t->_drawTower(); break;
        case 45: _t->_drawFigures(); break;
        case 46: _t->_animate(); break;
        case 47: _t->_animateStar(); break;
        case 48: _t->setPointx((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 49: _t->setPointy((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 50: _t->setPointz((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 51: _t->setPointindex((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 52: _t->setHermitePointx((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 53: _t->setHermitePointy((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 54: _t->setHermitePointz((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 55: _t->setHermitePointindex((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 56: _t->cyclicInterpolEnabled((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 57: _t->cyclicD1Enabled((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 58: _t->cyclicD2Enabled((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 59: _t->setRotationAngle((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 60: _t->setShowVectors((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 61: _t->set_shader_index((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 62: _t->set_toon_color_r((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 63: _t->set_toon_color_g((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 64: _t->set_toon_color_b((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 65: _t->set_toon_color_a((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 66: _t->set_show_alpha((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 67: _t->set_scale_factor((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 68: _t->set_smoothing((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 69: _t->set_shading((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 70: _t->set_hp3_interpolation_visibility((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 71: _t->set_hp3_d1_visibility((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 72: _t->set_hp3_d2_visibility((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 73: _t->set_hp3_vectors_visibility((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 74: _t->set_rotation_angle_x((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 75: _t->set_rotation_angle_y((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 76: _t->set_rotation_angle_z((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 77: _t->set_vector_length((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 78: _t->shaderIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 79: _t->hermitePatchIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 80: _t->set_hp3_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 81: _t->set_hp3_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 82: _t->set_hp3_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 83: _t->set_HermiteArcPoint_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 84: _t->set_HermiteArcPoint_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 85: _t->set_HermiteArcPoint_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 86: _t->saveToFile(); break;
        case 87: _t->loadFromFile(); break;
        case 88: _t->set_Hermite_Arc_index((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 89: _t->addNewHermiteArc(); break;
        case 90: _t->set_Hermite_Arc_index_1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 91: _t->set_Hermite_Arc_index_2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 92: _t->set_Method_number((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 93: _t->set_Direction_number((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 94: _t->manage_event(); break;
        case 95: _t->reset(); break;
        case 96: _t->updatePatchIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 97: _t->updatePatchSelectedVertex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 98: _t->set_HermitePatchPoint_x((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 99: _t->set_HermitePatchPoint_y((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 100: _t->set_HermitePatchPoint_z((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 101: _t->updatePatchIndex1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 102: _t->updatePatchIndex2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 103: _t->updatePatchDir1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 104: _t->updatePatchDir2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 105: _t->updatePatchMethod((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 106: _t->updateDirection((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 107: _t->updateDirectionPair((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 108: _t->updateMaterial((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 109: _t->onPatchSubmit(); break;
        case 110: _t->addNewPatch(); break;
        case 111: _t->resetPatches(); break;
        case 112: _t->showPatch_d0u((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 113: _t->showPatch_d0v((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 114: _t->showPatch_d1u((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 115: _t->showPatch_d1v((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 116: _t->showPatch_d2u((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 117: _t->showPatch_d2v((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 118: _t->setPatchVectorLength((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 119: _t->setPatchVectorNumber((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 120: _t->setPatchRotationalAngleX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 121: _t->setPatchRotationalAngleY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 122: _t->setPatchRotationalAngleZ((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 123: _t->set_move_all((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 124: _t->set_move_all_arc((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::xPointChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::yPointChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::zPointChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::xHermitePointChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::yHermitePointChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::zHermitePointChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::vectorLengthChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::hp3xPointChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::hp3yPointChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::hp3zPointChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::ha3xPointChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::ha3yPointChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::ha3zPointChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::arcIndexChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::arcIndexChanged1)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::arcIndexChanged2)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::patch_cp_x_changed)) {
                *result = 16;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::patch_cp_y_changed)) {
                *result = 17;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::patch_cp_z_changed)) {
                *result = 18;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::patchIndexChanged)) {
                *result = 19;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::patchIndex1Changed)) {
                *result = 20;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::patchIndex2Changed)) {
                *result = 21;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::pairedDirectionsEnabled)) {
                *result = 22;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::extDirectionsEnabled)) {
                *result = 23;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::submitEnabled)) {
                *result = 24;
                return;
            }
        }
        {
            using _t = void (GLWidget::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&GLWidget::resetEverything)) {
                *result = 25;
                return;
            }
        }
    }
}

const QMetaObject cagd::GLWidget::staticMetaObject = { {
    QMetaObject::SuperData::link<QOpenGLWidget::staticMetaObject>(),
    qt_meta_stringdata_cagd__GLWidget.offsetsAndSize,
    qt_meta_data_cagd__GLWidget,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_cagd__GLWidget_t
, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>
, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<double, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>


>,
    nullptr
} };


const QMetaObject *cagd::GLWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *cagd::GLWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_cagd__GLWidget.stringdata0))
        return static_cast<void*>(this);
    return QOpenGLWidget::qt_metacast(_clname);
}

int cagd::GLWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QOpenGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 125)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 125;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 125)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 125;
    }
    return _id;
}

// SIGNAL 0
void cagd::GLWidget::xPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void cagd::GLWidget::yPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void cagd::GLWidget::zPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void cagd::GLWidget::xHermitePointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void cagd::GLWidget::yHermitePointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void cagd::GLWidget::zHermitePointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void cagd::GLWidget::vectorLengthChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void cagd::GLWidget::hp3xPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void cagd::GLWidget::hp3yPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void cagd::GLWidget::hp3zPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void cagd::GLWidget::ha3xPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void cagd::GLWidget::ha3yPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void cagd::GLWidget::ha3zPointChanged(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void cagd::GLWidget::arcIndexChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void cagd::GLWidget::arcIndexChanged1(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void cagd::GLWidget::arcIndexChanged2(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void cagd::GLWidget::patch_cp_x_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void cagd::GLWidget::patch_cp_y_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void cagd::GLWidget::patch_cp_z_changed(double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void cagd::GLWidget::patchIndexChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void cagd::GLWidget::patchIndex1Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 20, _a);
}

// SIGNAL 21
void cagd::GLWidget::patchIndex2Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void cagd::GLWidget::pairedDirectionsEnabled(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void cagd::GLWidget::extDirectionsEnabled(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void cagd::GLWidget::submitEnabled(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 24, _a);
}

// SIGNAL 25
void cagd::GLWidget::resetEverything(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 25, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
