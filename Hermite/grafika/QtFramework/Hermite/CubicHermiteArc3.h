#ifndef CUBICHERMITEARC3_H
#define CUBICHERMITEARC3_H

#include <Core/Constants.h>
#include <Core/LinearCombination3.h>

namespace cagd
{
    class CubicHermiteArc3: public LinearCombination3
    {

    public:
        CubicHermiteArc3(GLenum data_usage_flag = GL_STATIC_DRAW);

        // inherited pure virtual methods have to be redeclared and defined
        GLboolean BlendingFunctionValues(GLdouble u, RowMatrix<GLdouble> &values) const;
        GLboolean CalculateDerivatives(GLuint max_order_of_derivatives, GLdouble u, Derivatives& d) const;



        // in the case of Hermite projects, one can also introduce the following (more intuitively named) methods (corner_index ∈ {0,1})
        GLboolean SetCorner(GLuint corner_index, GLdouble x, GLdouble y, GLdouble z); // _data[corner_index] = position;
        GLboolean SetCorner(GLuint corner_index, const DCoordinate3 &position);

        GLboolean SetTangent(GLuint corner_index, GLdouble t_x, GLdouble t_y, GLdouble t_z);
        GLboolean SetTangent(GLuint corner_index, const DCoordinate3 &tangent); // _data[corner_index+2] = tangent;

        GLboolean UpdateVertexBufferObjectsOfData(GLenum usage_flag = GL_STATIC_DRAW);
        GLboolean RenderData(GLenum render=GL_LINE_STRIP) const;

    };
}

#endif // CUBICHERMITEARC3_H
