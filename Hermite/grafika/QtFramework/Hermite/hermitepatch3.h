#ifndef HERMITEPATCH3_H
#define HERMITEPATCH3_H


#include "../Core/TensorProductSurfaces3.h"
#include "./BasisFunctions.h"

namespace cagd
{
    class HermitePatch3: public TensorProductSurface3
    {
    public:
        HermitePatch3();

        GLboolean UBlendingFunctionValues(GLdouble u_knot, RowMatrix<GLdouble> &blending_values) const;
        GLboolean VBlendingFunctionValues(GLdouble u_knot, RowMatrix<GLdouble> &blending_values) const;
        GLboolean CalculatePartialDerivatives(GLuint max_order_of_derivatives, GLdouble u, GLdouble v, PartialDerivatives& partial_derivatives) const;

        GLboolean SetCorner(GLuint corner_index, GLdouble x, GLdouble y, GLdouble z);
        GLboolean SetCorner(GLuint corner_index, const DCoordinate3 &position);


        GLboolean SetUvector(GLuint corner_index, GLdouble x, GLdouble y, GLdouble z);
        GLboolean SetUvector(GLuint corner_index, const DCoordinate3 &position);

        GLboolean SetVvector(GLuint corner_index, GLdouble x, GLdouble y, GLdouble z);
        GLboolean SetVvector(GLuint corner_index, const DCoordinate3 &position);


        GLboolean SetTangent(GLuint corner_index, GLdouble t_x, GLdouble t_y, GLdouble t_z);
        GLboolean SetTangent(GLuint corner_index, const DCoordinate3 &tangent);

        GLboolean UpdateVertexBufferObjectsOfData(GLenum usage_flag=GL_STATIC_DRAW);
        GLboolean RenderData(GLenum render=GL_LINE_STRIP) const;

    };
}

#endif // HERMITEPATCH3_H
