#pragma once

#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include "Core/DCoordinates3.h"
#include "Core/Matrices.h"
#include "Core/RealSquareMatrices.h"

using namespace std;
using namespace cagd;


void testDCoordinates3()
{
    ifstream fin("testDCoordinates3.in");
    ofstream fout("testDCoordinates3.out");

    DCoordinate3 defaultDC, coord1(2.3, 1), coord2(20.9, -0.001, 1.16);

    fout<<"Default DC3: "<<defaultDC<<"\n";
    fout<<"Coord1: "<<coord1 << "\n";
    fout<<"Coord2: "<<coord2 << "\n";
    fout<<"a+b: "<<coord1 + coord2<< "\n";
    fout<<"a-b: "<<coord1 - coord2<< "\n";

    coord1 += coord2;
    fout <<"a+=b: "<<coord1<<" ";
    coord1 -= coord2;
    fout<<"a-=b: "<<coord1<<"\n";

    defaultDC = coord1 ^ coord2;
    fout <<"a=b^c: "<<defaultDC << "\n";
    coord1 ^= coord2;
    fout <<"a^=b: "<< coord1 << "\n";

    fout <<"a*2: "<<coord1 * 2<< "\n";
    coord1 *= 2;
    fout <<"a*=: "<< coord1 << "\n";

    fout <<"a/2: "<< coord1 / 2<< "\n";
    coord1 /= 2;
    fout <<"a/=2: "<< coord1 << "\n";


    fout <<"Coord2 length: "<<coord2.length() << "\n";

    fin >> defaultDC;
    fout <<"Read and normalize: "<<defaultDC << " norm--> " << defaultDC.normalize();

    return;
}

void testMatrices()
{
    ifstream fin("testMatrices.in");
    ofstream fout("testMatrices.out");

    Matrix<GLuint> a;
    fout  << a;

    fin >> a;
    fout << a<<"\n";
    fout <<"Matrix dimensions" <<a.GetRowCount()<<" x "<< a.GetColumnCount()<<"\n";

    Matrix<GLuint> b(a);
    fout << "Copy of a :\n" << b;

    a.ResizeRows(2);
    a.ResizeColumns(2);
    fout << "Resize:\n" << a;

    b = a;
    fout << "b=" << b;

    RowMatrix<GLuint> rowMat(2);
    ColumnMatrix<GLuint> columnMat(2);
    a.SetRow(0, rowMat);
    b.SetColumn(1, columnMat);
    fout << "Row 0 set a matrix:" << a;
    fout<<"Column 1 set b matrix:"<< b;

    GLuint &ref = a(0,0);
    ref = 15;
    GLuint val = a(1,1);
    val = 15;
    fout << "x(0,0) and x(1,1) to 15:\n" << a;



    TriangularMatrix<GLuint> x;
    fout << "Triang test:" << x;

    fin >> x;
    fout << x;
    fout << "Size:" << x.GetRowCount() << "\n";

    GLuint &ref1 = x(2,1);
    GLuint y = x(0,0);
    ref1 = 5;
    y = 5;
    fout << "Ref:" << x;
    fout << "Element " << y << std::endl;

    if(x.ResizeRows(2) == GL_TRUE) fout << "true"; else fout << "false";
    fout << endl << x;
    if(x.ResizeRows(6) == GL_TRUE) fout << "true"; else fout << "false";
    fout << endl << x;

    return;
}

void testRealSquareMatrices()
{
    ifstream fin("testRealSquareMatrices.in");
    ofstream fout("testRealSquareMatrices.out");

    RealSquareMatrix x;
    fout << "Default constructor:\n" << x;

    fin >> x;
    fout << x;
    fout << "Matrix dimension: " << x.GetRowCount()<<" x "<< x.GetColumnCount() << "\n";

    RealSquareMatrix a(x);
    fout << "Copy constructor:\n" << a;

    RowMatrix<double> rowMat(x.GetRowCount());
    ColumnMatrix<double> columnMat(x.GetColumnCount());
    x.SetRow(1, rowMat);
    x.SetColumn(1, columnMat);
    fout << "Set row/column:" << x;

    a = x;
    RealSquareMatrix A(3);
                A(0, 0) = 8;
                A(0, 1) = 1;
                A(0, 2) = 6;

                A(1, 0) = 3;
                A(1, 1) = 5;
                A(1, 2) = 7;

                A(2, 0) = 4;
                A(2, 1) = 9;
                A(2, 2) = 2;

                ColumnMatrix<GLdouble> B(3);
                B[0] = -1.0;
                B[1] =  1.0;
                B[2] = -1.0;

                ColumnMatrix<GLdouble> X;

                if (!A.SolveLinearSystem(B, X))
                {
                    throw exception("Could not solve the linear system!");
                }

                fout << "The solution is " << endl << X << endl;



    x.ResizeColumns(3);
    fout << "Resize:\n" << x;



    return;
}
