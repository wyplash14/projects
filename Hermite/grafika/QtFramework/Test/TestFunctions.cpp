#include <cmath>
#include "TestFunctions.h"
#include "../Core/Constants.h"

using namespace cagd;
using namespace std;

GLdouble spiral_on_cone::u_min = -TWO_PI;
GLdouble spiral_on_cone::u_max = +TWO_PI;

DCoordinate3 spiral_on_cone::d0(GLdouble u)
{
    return DCoordinate3(u * cos(u), u * sin(u), u);
}

DCoordinate3 spiral_on_cone::d1(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(c - u * s, s + u * c, 1.0);
}

DCoordinate3 spiral_on_cone::d2(GLdouble u)
{
    GLdouble c = cos(u), s = sin(u);
    return DCoordinate3(-2.0 * s - u * c, 2.0 * c - u * s, 0);
}

GLdouble torus::u_min = 0.0;
GLdouble torus::u_max =6*PI;

DCoordinate3 torus::d0(GLdouble u)
{
    GLdouble c = cos(2.0 * u / 3.0), s = sin(2.0 * u / 3.0);
        return DCoordinate3((2.0 + c) * cos(u), (2.0 + c) * sin(u), s);
}

DCoordinate3 torus::d1(GLdouble u)
{
    GLdouble c = cos(2.0 * u / 3.0), s = sin(2.0 * u / 3.0);
        return DCoordinate3(-2.0/3.0 * s * cos(u) - sin(u) * (c + 2),
                            (c + 2) * cos(u) - 2.0/3.0 * s * sin(u),
                            2.0/3.0 * c);
}

DCoordinate3 torus::d2(GLdouble u)
{
    GLdouble c = cos(2.0 * u / 3.0), s = sin(2.0 * u / 3.0);
       return DCoordinate3(4.0/3.0 * s * sin(u) + (-13.0/9.0 * c - 2) * cos(u),
                           -4.0/3.0 * s * cos(u) - 1.0/9.0 * sin(u) * (13.0 * c + 18),
                           -4.0/9.0 * s);
}



GLdouble lissajou::u_min = -1.0;
GLdouble lissajou::u_max = +1.0;

DCoordinate3 lissajou::d0(GLdouble u)
{
    return DCoordinate3(sin(5.0*u + 1.5707), sin(4.0*u), u);
}

DCoordinate3 lissajou::d1(GLdouble u)
{
    return DCoordinate3(5.0*cos(5.0*u + 1.5707), 4.0*cos(4.0*u), 1.0);
}

DCoordinate3 lissajou::d2(GLdouble u)
{
    return DCoordinate3(-25.0*sin(5.0*u + 1.5707), -16.0*sin(4.0*u), 0.0);
}

GLdouble hypo::u_min = -7.0;
GLdouble hypo::u_max = +7.0;

DCoordinate3 hypo::d0(GLdouble u)
{
    return DCoordinate3(2.0*cos(u)+5.0*cos(2.0/3.0*u), 2.0*sin(u)-5.0*sin(2.0/3.0*u), u);
}

DCoordinate3 hypo::d1(GLdouble u)
{
    return DCoordinate3(-2.0*sin(u)-5.0*sin(2.0/3.0*u), 2.0*cos(u)-5.0*cos(2.0/3.0*u), 1.0);
}

DCoordinate3 hypo::d2(GLdouble u)
{
    return DCoordinate3(-2.0*cos(u)-5.0*2.0/3.0*cos(2.0/3.0*u), -2.0*sin(u)+5.0*2.0/3.0*sin(2.0/3.0*u), 0.0);
}

GLdouble cyclo::u_min = 0.0;
GLdouble cyclo::u_max = 2*PI;

DCoordinate3 cyclo::d0(GLdouble u)
{
    return DCoordinate3(2.0*(u-sin(u)), 2.0*(1.0-cos(u)), u);
}

DCoordinate3 cyclo::d1(GLdouble u)
{
    return DCoordinate3(2.0*(1.0-cos(u)), 2.0*sin(u), 1.0);
}

DCoordinate3 cyclo::d2(GLdouble u)
{
    return DCoordinate3(sin(u), 2.0*cos(u), 0.0);
}

GLdouble ellipse::u_min = -6.0;
GLdouble ellipse::u_max = +6.0;

DCoordinate3 ellipse::d0(GLdouble u)
{
    return DCoordinate3(3.0 * cos(u), 4.0 * sin(u), u);
}

DCoordinate3 ellipse::d1(GLdouble u)
{
    return DCoordinate3(-3.0 * sin(u), 4.0 * cos(u), 1);
}

DCoordinate3 ellipse::d2(GLdouble u)
{
    return DCoordinate3(-3.0 * cos(u), -4.0 * sin(u), 0);
}

GLdouble helix::u_min = 0.0;
GLdouble helix::u_max = 75.39;

DCoordinate3 helix::d0(GLdouble u)
{
    return DCoordinate3(2.0*cos(u),2.0*sin(u), -u/5.0);
}

DCoordinate3 helix::d1(GLdouble u)
{
    return DCoordinate3(-2.0*sin(u),2.0*cos(u), -1.0/5.0);
}

DCoordinate3 helix::d2(GLdouble u)
{
    return DCoordinate3(-2.0*cos(u),-2.0*sin(u), 0);
}


GLdouble firstSurface::u_min = 0;
GLdouble firstSurface::u_max = PI;
GLdouble firstSurface::v_min = 0;
GLdouble firstSurface::v_max = TWO_PI;

DCoordinate3
firstSurface::d00(GLdouble u, GLdouble v)
{

  return DCoordinate3(sin(u) * cos(v), sin(u) * sin(v), cos(u));
}

DCoordinate3
firstSurface::d10(GLdouble u, GLdouble v)
{

  return DCoordinate3(cos(v) * cos(u), cos(u) * sin(v), -1 * sin(u));
}

DCoordinate3
firstSurface::d01(GLdouble u, GLdouble v)
{

  return DCoordinate3(-1 * sin(v) * sin(u), sin(u) * cos(v), 0);
}

//-----------------------------

GLdouble secondSurface::u_min = -0;
GLdouble secondSurface::u_max = 3;
GLdouble secondSurface::v_min = 0;
GLdouble secondSurface::v_max = TWO_PI;

DCoordinate3
secondSurface::d00(GLdouble u, GLdouble v)
{

  return DCoordinate3(2 * sin(v), 2 * cos(v), u);
}

DCoordinate3
secondSurface::d10(GLdouble u, GLdouble v)
{

  return DCoordinate3(0, 0, 1);
}

DCoordinate3
secondSurface::d01(GLdouble u, GLdouble v)
{

  return DCoordinate3(2 * cos(v), -2 * sin(v), 0);
}

//--------------------------------------

GLdouble thirdSurface::u_min = -1;
GLdouble thirdSurface::u_max = 1;
GLdouble thirdSurface::v_min = -1;
GLdouble thirdSurface::v_max = 1;

DCoordinate3
thirdSurface::d00(GLdouble u, GLdouble v)
{

  return DCoordinate3(u, v, u * v);
}

DCoordinate3
thirdSurface::d10(GLdouble u, GLdouble v)
{

  return DCoordinate3(1, 0, v);
}

DCoordinate3
thirdSurface::d01(GLdouble u, GLdouble v)
{

  return DCoordinate3(0, -2 * sin(v), 2 * cos(v));
}

//--------------------------------------

GLdouble forthSurface::u_min = -PI;
GLdouble forthSurface::u_max = PI;
GLdouble forthSurface::v_min = -PI;
GLdouble forthSurface::v_max = PI;

DCoordinate3
forthSurface::d00(GLdouble u, GLdouble v)
{

  return DCoordinate3(cos(u) * cos(v), u, cos(u) * sin(v));
}

DCoordinate3
forthSurface::d10(GLdouble u, GLdouble v)
{

  return DCoordinate3(cos(v) * -1 * sin(u), 1, -sin(u) * sin(v));
}

DCoordinate3
forthSurface::d01(GLdouble u, GLdouble v)
{

  return DCoordinate3(cos(u) * -1 * sin(v), 0, cos(u) * cos(v));
}

//--------------------------------

GLdouble fifthSurface::u_min = 0;
GLdouble fifthSurface::u_max = PI;
GLdouble fifthSurface::v_min = 0;
GLdouble fifthSurface::v_max = PI;

DCoordinate3
fifthSurface::d00(GLdouble u, GLdouble v)
{

  return DCoordinate3(sin(2 * u) * cos(v) * cos(v),
                      sin(u) * sin(2 * v) / 2,
                      cos(2 * u) * cos(v) * cos(v));
}

DCoordinate3
fifthSurface::d10(GLdouble u, GLdouble v)
{

  return DCoordinate3(cos(v) * cos(v) * 2 * cos(2 * u),
                      sin(2 * v) / 2 * cos(u),
                      cos(v) * cos(v) * -2 * sin(2 * u));
}

DCoordinate3
fifthSurface::d01(GLdouble u, GLdouble v)
{

  return DCoordinate3(sin(2 * u) * -2 * cos(v) * sin(v),
                      sin(u) * cos(2 * v),
                      cos(2 * u) * -2 * cos(v) * sin(v));
}

