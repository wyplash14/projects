#include "MainWindow.h"

namespace cagd
{
    MainWindow::MainWindow(QWidget *parent): QMainWindow(parent)
    {
        setupUi(this);

    /*

      the structure of the main window's central widget

     *---------------------------------------------------*
     |                 central widget                    |
     |                                                   |
     |  *---------------------------*-----------------*  |
     |  |     rendering context     |   scroll area   |  |
     |  |       OpenGL widget       | *-------------* |  |
     |  |                           | | side widget | |  |
     |  |                           | |             | |  |
     |  |                           | |             | |  |
     |  |                           | *-------------* |  |
     |  *---------------------------*-----------------*  |
     |                                                   |
     *---------------------------------------------------*

    */
        _side_widget = new SideWidget(this);

        _tab_widget=new QTabWidget(_side_widget);

        _scroll_area = new QScrollArea(this);
        _scroll_area->setWidget(_side_widget);
        _scroll_area->setSizePolicy(_side_widget->sizePolicy());
        _scroll_area->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        _gl_widget = new GLWidget(this);

        centralWidget()->setLayout(new QHBoxLayout());
        centralWidget()->layout()->addWidget(_gl_widget);
        centralWidget()->layout()->addWidget(_scroll_area);

       connect(_side_widget->tabWidget,SIGNAL(currentChanged(int)),_gl_widget,SLOT(setHomeworkID(int)));

        // creating a signal slot mechanism between the rendering context and the side widget
        connect(_side_widget->rotate_x_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));
        connect(_side_widget->zoom_factor_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->rotate_x_slider_2, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider_2, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider_2, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));
        connect(_side_widget->zoom_factor_spin_box_2, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->rotate_x_slider_3, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider_3, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider_3, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));
        connect(_side_widget->zoom_factor_spin_box_3, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->trans_x_spin_box_2, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
        connect(_side_widget->trans_y_spin_box_2, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
        connect(_side_widget->trans_z_spin_box_2, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

        connect(_side_widget->trans_x_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
        connect(_side_widget->trans_y_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
        connect(_side_widget->trans_z_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

        connect(_side_widget->curve_slider,SIGNAL(valueChanged(int)),_gl_widget,SLOT(setParametricCurveIndex(int)));
        connect(_side_widget->checkBox,SIGNAL(stateChanged(int)),_gl_widget,SLOT(setVisibilityOfTangents(int)));
        connect(_side_widget->checkBox_2,SIGNAL(stateChanged(int)),_gl_widget,SLOT(setVisibilityOfAccelerationVectors(int)));
        connect(_side_widget->vector_length_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_Scale(double)));


        connect(_side_widget->rotate_x_slider_4, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider_4, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider_4, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));
        connect(_side_widget->zoom_factor_spin_box_4, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));
        connect(_side_widget->curve_slider_2,SIGNAL(valueChanged(int)),_gl_widget,SLOT(setSurfaceIndex(int)));
        connect(_side_widget->checkBox_3,SIGNAL(stateChanged(int)),_gl_widget,SLOT(textureEnabled(int)));
        connect(_side_widget->texture_slider,SIGNAL(valueChanged(int)),_gl_widget,SLOT(setTextureType(int)));


        connect(_side_widget->rotate_x_slider_5, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider_5, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider_5, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));
        connect(_side_widget->zoom_factor_spin_box_5, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->trans_x_spin_box_3, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_x(double)));
        connect(_side_widget->trans_y_spin_box_3, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_y(double)));
        connect(_side_widget->trans_z_spin_box_3, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_trans_z(double)));

        connect(_side_widget->point_index_spin_box, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setPointindex(double)));
        connect(_side_widget->point_index_spin_box_x, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setPointx(double)));
        connect(_side_widget->point_index_spin_box_y, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setPointy(double)));
        connect(_side_widget->point_index_spin_box_z, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setPointz(double)));


        connect(_gl_widget,SIGNAL(xPointChanged(double)),_side_widget->point_index_spin_box_x,SLOT(setValue(double)));
        connect(_gl_widget,SIGNAL(yPointChanged(double)),_side_widget->point_index_spin_box_y,SLOT(setValue(double)));
        connect(_gl_widget,SIGNAL(zPointChanged(double)),_side_widget->point_index_spin_box_z,SLOT(setValue(double)));


        connect(_side_widget->rotate_x_slider_11, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider_11, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider_11, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));
        connect(_side_widget->zoom_factor_spin_box_11, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));




        connect(_side_widget->point_index_spin_box_3, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setHermitePointindex(double)));


        connect(_side_widget->checkBox_4,SIGNAL(stateChanged(int)),_gl_widget,SLOT(cyclicInterpolEnabled(int)));
        connect(_side_widget->checkBox_5,SIGNAL(stateChanged(int)),_gl_widget,SLOT(cyclicD1Enabled(int)));
        connect(_side_widget->checkBox_6,SIGNAL(stateChanged(int)),_gl_widget,SLOT(cyclicD2Enabled(int)));

       // connect(_side_widget->rotate_vector,SIGNAL(valueChanged(int)),SLOT(setRotationAngle(int)));

        connect(_side_widget->rotate_x_slider_12, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider_12, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider_12, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));
        connect(_side_widget->zoom_factor_spin_box_12, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));
        connect(_side_widget->checkBox_7,SIGNAL(stateChanged(int)),_gl_widget,SLOT(set_hp3_d1_visibility(int)));
        connect(_side_widget->checkBox_8,SIGNAL(stateChanged(int)),_gl_widget,SLOT(set_hp3_interpolation_visibility(int)));
        connect(_side_widget->checkBox_9,SIGNAL(stateChanged(int)),_gl_widget,SLOT(set_hp3_d2_visibility(int)));

        connect(_side_widget->shader_slider_2, SIGNAL(valueChanged(int)), _gl_widget, SLOT(shaderIndexChanged(int)));

        connect(_side_widget->rotate_x_slider_13, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_x(int)));
        connect(_side_widget->rotate_y_slider_13, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_y(int)));
        connect(_side_widget->rotate_z_slider_13, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_angle_z(int)));
        connect(_side_widget->zoom_factor_spin_box_13, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_zoom_factor(double)));

        connect(_side_widget->horizontalSlider_x, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_rotation_angle_x(int)));
        connect(_side_widget->horizontalSlider_y, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_rotation_angle_y(int)));
        connect(_side_widget->horizontalSlider_z, SIGNAL(valueChanged(int)), _gl_widget, SLOT(set_rotation_angle_z(int)));
        connect(_side_widget->vectorSpinBox, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_vector_length(double)));

        connect(_gl_widget,SIGNAL(vectorLengthChanged(double)),_side_widget->vectorSpinBox, SLOT(setValue(double)));

        connect(_side_widget->scale_factor_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setScaleFactor(int)));
        connect(_side_widget->smoothing_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setSmoothing(int)));
        connect(_side_widget->shading_slider, SIGNAL(valueChanged(int)), _gl_widget, SLOT(setShading(int)));

         connect(_side_widget->rgb_spin_1, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setRgb1(double)));
         connect(_side_widget->rgb_spin_2, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setRgb2(double)));
         connect(_side_widget->rgb_spin_3, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setRgb3(double)));
         connect(_side_widget->rgb_spin_4, SIGNAL(valueChanged(double)), _gl_widget, SLOT(setRgb4(double)));

         connect(_side_widget->spinBox,SIGNAL(valueChanged(int)),_gl_widget, SLOT( hermitePatchIndexChanged(int)));
         connect(_side_widget->checkBox_10,SIGNAL(stateChanged(int)),_gl_widget,SLOT(set_hp3_vectors_visibility(int)));


         connect(_gl_widget,SIGNAL(hp3xPointChanged(double)),_side_widget->vectorSpinBox_3, SLOT(setValue(double)));
         connect(_gl_widget,SIGNAL(hp3yPointChanged(double)),_side_widget->vectorSpinBox_4, SLOT(setValue(double)));
         connect(_gl_widget,SIGNAL(hp3zPointChanged(double)),_side_widget->vectorSpinBox_5, SLOT(setValue(double)));
         connect(_side_widget->vectorSpinBox_3, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_hp3_x(double)));
         connect(_side_widget->vectorSpinBox_4, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_hp3_y(double)));
         connect(_side_widget->vectorSpinBox_5, SIGNAL(valueChanged(double)), _gl_widget, SLOT(set_hp3_z(double)));
    }

    //--------------------------------
    // implementation of private slots
    //--------------------------------
    void MainWindow::on_action_Quit_triggered()
    {
        qApp->exit(0);
    }
}
