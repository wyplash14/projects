#include "GLWidget.h"
#include "Core/Materials.h"
#include "Core/Constants.h"
#if !defined(__APPLE__)
#include <GL/GLU.h>
#include <math.h>
#endif

#include <iostream>
using namespace std;

#include <Core/Exceptions.h>
#include <Test/TestFunctions-1.h>
#include <Test/TestFunctions.h>
namespace cagd
{
    //--------------------------------
    // special and default constructor
    //--------------------------------
    GLWidget::GLWidget(QWidget *parent): QOpenGLWidget(parent)
    {
        _timer=new QTimer(this);
        _timer->setInterval(0);
        connect(_timer,SIGNAL(timeout()),this,SLOT(_animate()));

    }
     //-----------------------------------------------------------------------------------
    // this virtual function is called once before the first call to paintGL() or resizeGL()
    //--------------------------------------------------------------------------------------
    void GLWidget::initializeGL()
    {
        // creating a perspective projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = static_cast<double>(width()) / static_cast<double>(height());
        _z_near = 1.0;
        _z_far  = 1000.0;
        _fovy   = 45.0;

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // setting the model view matrix
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        _eye[0] = _eye[1] = 0.0; _eye[2] = 6.0;
        _center[0] = _center[1] = _center[2] = 0.0;
        _up[0] = _up[2] = 0.0; _up[1] = 1.0;

        gluLookAt(_eye[0], _eye[1], _eye[2], _center[0], _center[1], _center[2], _up[0], _up[1], _up[2]);

        // enabling the depth test
        glEnable(GL_DEPTH_TEST);

        // setting the background color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // initial values of transformation parameters
        _angle_x = _angle_y = _angle_z = 0.0;
        _trans_x = _trans_y = _trans_z = 0.0;
        _zoom = 1.0;

        // ...

        try
        {
            // initializing the OpenGL Extension Wrangler library
            GLenum error = glewInit();

            if (error != GLEW_OK)
            {
                throw Exception("Could not initialize the OpenGL Extension Wrangler Library!");
            }

            if (!glewIsSupported("GL_VERSION_2_0"))
            {
                throw Exception("Your graphics card is not compatible with OpenGL 2.0+! "
                                "Try to update your driver or buy a new graphics adapter!");
            }

            // create and store your geometry in display lists or vertex buffer objects
            // ...
            //testDCoordinates3();
            //testMatrices();
            //testRealSquareMatrices();



            switch (_homework_id) {
                case 1:
                    if(!_createAllParametricCurvesAndTheirImages())
                    {
                        throw Exception("Could not create all parametric curves!");
                    }

                break;
                case 3:
                if(!_createAllParametricSurfaceAndTheirImages())
                {
                throw Exception("Could not create all parametric curves!");
                 }
                 if(!_loadTextures()){
                     throw Exception("Could not load all textures!");
                 }
                    break;
                case 4:

                    if (!_initializeCyclicCurve()){
                        throw Exception("Could not create the cyclic curve");
                    }
                    break;
                case 5:
                     if (!_initializeHermiteArc()){
                        throw Exception("Could not create the hermite curve");
                       }
                break;
                case 6:
                    if (!_initializeHermitePatch()){
                        throw Exception("Could not create the hermite patch");
                    }
                break;
                 case 7:
                    if (!_mouse.LoadFromOFF("Models/mouse.off",GL_TRUE)){
                        throw Exception("Could not load the model file");
                    }
                    loadShaders();

                case 2:
                    if (!_cube.LoadFromOFF("Models/cube.off",GL_TRUE)){
                        throw Exception("Could not load the model file");
                    }
                    if(!_cube.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)){
                        throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
                    }

                    if (!_cone.LoadFromOFF("Models/cone.off",GL_TRUE)){
                        throw Exception("Could not load the model file");
                    }
                    if(!_cone.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)){
                        throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
                    }

                    if (!_dragon.LoadFromOFF("Models/dragon.off",GL_TRUE)){
                        throw Exception("Could not load the model file");
                    }
                    if(!_dragon.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)){
                        throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
                    }

                    if (!_spot.LoadFromOFF("Models/icosahedron.off",GL_TRUE)){
                        throw Exception("Could not load the model file");
                    }
                    if(!_spot.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)){
                        throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
                    }

                    if (!_mouse.LoadFromOFF("Models/mouse.off",GL_TRUE)){
                        throw Exception("Could not load the model file");
                    }
                    if(!_mouse.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)){
                        throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
                    }

                    if (!_star.LoadFromOFF("Models/star.off",GL_TRUE)){
                        throw Exception("Could not load the model file");
                    }
                    if(!_star.UpdateVertexBufferObjects(GL_DYNAMIC_DRAW)){
                        throw Exception("Could not update the vertex buffer objects of the triangulated mesh!");
                    }

//                    _angle2=0.0;
//                    _angle=0.0;
//                    _timer->start();

                    HCoordinate3 direction(0.0,0.0,1.0,0.0);
                    Color4 ambient(0.4,0.4,0.4,1.0);
                    Color4 diffuse(0.8,0.8,0.8,1.0);
                    Color4 specular(1.0,1.0,1.0,1.0);
                    _dl=new (nothrow) DirectionalLight(GL_LIGHT0,direction,ambient,diffuse,specular);
                    if(!_dl){
                        throw Exception("Could not create the directional light object!");
                    }
//                    glEnable(GL_LIGHTING);
//                    glEnable(GL_NORMALIZE);
                break;

            }

        }
        catch (Exception &e)
        {
            cout << e << endl;
        }


        glEnable(GL_POINT_SMOOTH);
        glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);

        glEnable(GL_LINE_SMOOTH);
        glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);

        glEnable(GL_POLYGON_SMOOTH);
        glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);

        glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
        glEnable(GL_DEPTH_TEST);



    }

    //-----------------------
    // the rendering function
    //-----------------------

    bool GLWidget::_renderSelectedParametricCurve()
    {

        if (!_image_of_pc[_selected_pc])
        {
             return false;
        }

         glColor3f(1.0f,0.0f,0.0f);
        _image_of_pc[_selected_pc]->RenderDerivatives(0, GL_LINE_STRIP);
         glPointSize(5.0f);
        if (_show_tangents)
        {
            glColor3f(0.0f,0.5f,0.0f);
            _image_of_pc[_selected_pc]->RenderDerivatives(1,GL_LINES);
            _image_of_pc[_selected_pc]->RenderDerivatives(1,GL_POINTS);
        }

        if (_show_acceleration_vectors)
        {
            glColor3f(0.0f,0.0f,1.0f);
            _image_of_pc[_selected_pc]->RenderDerivatives(2,GL_LINES);
            _image_of_pc[_selected_pc]->RenderDerivatives(2,GL_POINTS);
        }

       glPointSize(1.0f);

       update();
        return true;
    }


    bool GLWidget::_renderSelectedParametricSurface(){
        if (!_image_of_ps[_selected_ps])
        {
             return false;
        }


        if(_texture_enabled)
           glEnable(GL_TEXTURE_2D);
        else
           glDisable(GL_TEXTURE_2D);


        _dl->Enable();
        glEnable(GL_LIGHTING);
         glEnable(GL_LIGHT0);
         texture[_selected_texture]->bind();
         if (_image_of_ps[_selected_ps]) {

           _image_of_ps[_selected_ps]->Render();
         }
         glDisable(GL_LIGHT0);
         glDisable(GL_LIGHTING);
         _dl->Disable();


         update();
         return true;

    }

    bool GLWidget::_renderCyclicCurve(){

          glPointSize(10.0f);
            glBegin(GL_POINTS);
            glColor3f(0.0, 1.0f, 1.0f);
            glVertex3f((*_cyc3)[_point_ind][0], (*_cyc3)[_point_ind][1], (*_cyc3)[_point_ind][2]);
            glEnd();
            glPointSize(2);
          glColor3f(1.0f, 0.5f, 0.0f);
          _cyc3->RenderData(GL_LINE_LOOP);
          if (!_image_of_cyc3->RenderDerivatives(0, GL_LINE_LOOP)) {
           return false;
          }
          if(_show_interpol){
          glColor3f(1.0, 0.0, 1.0);
          _image_of_cyc3_interpol->RenderDerivatives(0, GL_LINE_LOOP);
          }

           if(_show_cyclic_d1){
          glColor3f(0.0f, 1.0f, 0.0f);
          _image_of_cyc3->RenderDerivatives(1, GL_LINES);
            }

           if(_show_cyclic_d2){
          glColor3f(0.0f, 0.0f, 1.0f);
          _image_of_cyc3->RenderDerivatives(2, GL_LINES);
          glPointSize(1.0f);
           }

    }

    bool GLWidget::_renderHermiteArc(){

          glPointSize(10.0f);
          glBegin(GL_POINTS);
          glColor3f(0.0, 1.0f, 1.0f);
          glVertex3f((*_ch3)[_hermite_point_ind][0], (*_ch3)[_hermite_point_ind][1], (*_ch3)[_hermite_point_ind][2]);
          glEnd();

          glPointSize(2);

          glPointSize(10.0f);
          glColor3f(1.0f, 0.5f, 0.0f);
          _ch3->RenderData(GL_LINE_STRIP);
          if (!_image_of_ch3->RenderDerivatives(0, GL_LINE_STRIP)) {
           return false;
          }

         if(_show_hermite_interpol){
          glColor3f(1.0, 0.0, 1.0);
          _image_of_ch3_interpol->RenderDerivatives(0, GL_LINE_STRIP);

          }

         if(_show_cyclic_hermite_d1){
           glPointSize(1.0f);
          glColor3f(0.0f, 1.0f, 0.0f);
          _image_of_ch3->RenderDerivatives(1, GL_LINES);
         }

         if(_show_cyclic_hermite_d2){
          glColor3f(0.0f, 0.0f, 1.0f);
          _image_of_ch3->RenderDerivatives(2, GL_LINES);
          glPointSize(1.0f);
         }
    }

    bool GLWidget::_renderHermitePatch(){
        glEnable(GL_LIGHTING);
            glEnable(GL_LIGHT0);
            glEnable(GL_NORMALIZE);
        //    _dl->Enable();


            if(_image_of_hp3_before_interp)
            {
                MatFBRuby.Apply();
                _image_of_hp3_before_interp->Render();
            }

            if(_image_of_hp3_after_interp)
            {
                glEnable(GL_BLEND);
                glDepthMask(GL_FALSE);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE);
                if(_show_hermite_patch_interpol){
               MatFBTurquoise.Apply();
               _image_of_hp3_after_interp->Render();
                }
                glDepthMask(GL_TRUE);
                glDisable(GL_BLEND);
            }

            glDisable(GL_LIGHTING);
            glDisable(GL_LIGHT0);
            glDisable(GL_NORMALIZE);

            for(GLuint i = 0; i < _u_lines->GetColumnCount(); i++)
            {
                if((*_u_lines)[i])
                {
                    if(_show_hermite_patch_d1){
                    (*_u_lines)[i]->RenderDerivatives(0, GL_LINE_STRIP);
                    (*_u_lines)[i]->RenderDerivatives(1, GL_LINES);
                    }
                }
            }

            for(GLuint i = 0; i < _v_lines->GetColumnCount(); i++)
            {
                if((*_v_lines)[i])
                {
                     if(_show_hermite_patch_d2){
                    (*_v_lines)[i]->RenderDerivatives(0, GL_LINE_STRIP);
                    glColor3f(0.0f, 1.0f, 0.0f);
                    (*_v_lines)[i]->RenderDerivatives(2, GL_LINES);
                    }
                }
            }

        //    _dl->Disable();

            if(_show_vectors){

            glPointSize(5.0f);
            glColor3f(1.0f,1.0f,1.0f);
             _hp3->RenderData();

            }
             glBegin(GL_POINTS);

             switch (_hp3_index){

             case 0:
                 glColor3f(0.0, 1.0f, 1.0f);
                 glPointSize(5.0f);
                  glVertex3f((*_hp3_origin)(0, 0)[0], (*_hp3_origin)(0,0)[1], (*_hp3_origin)(0,0)[2]);
                 break;
             case 1:
                 glColor3f(0.0, 1.0f, 1.0f);
                 glPointSize(5.0f);
                  glVertex3f((*_hp3_origin)(0, 1)[0], (*_hp3_origin)(0,1)[1], (*_hp3_origin)(0,1)[2]);

                  break;
             case 2:
                 glColor3f(0.0, 1.0f, 1.0f);
                 glPointSize(5.0f);
                 glVertex3f((*_hp3_origin)(1, 0)[0], (*_hp3_origin)(1,0)[1], (*_hp3_origin)(1,0)[2]);

                  break;
             default:
                 glColor3f(0.0, 1.0f, 1.0f);
                 glPointSize(5.0f);
                   glVertex3f((*_hp3_origin)(1,1)[0], (*_hp3_origin)(1,1)[1], (*_hp3_origin)(1,1)[2]);
                 break;
             }
             glEnd();

return true;


    }

    void GLWidget::set_hp3_interpolation_visibility(int value)
    {
        _show_hermite_patch_interpol = value == Qt::Checked;
        update();
    }

    void GLWidget::set_hp3_d1_visibility(int value)
    {
        _show_hermite_patch_d1 = value == Qt::Checked;
        update();
    }

    void GLWidget::set_hp3_d2_visibility(int value)
    {
        _show_hermite_patch_d2 = value == Qt::Checked;
        update();
    }

    bool GLWidget::_loadTextures(){
        texture.ResizeColumns(10);
        try{
            texture[0]=new QOpenGLTexture(QImage("Textures/periodic_texture_00.jpg").mirrored());
            texture[0]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[0]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[1]=new QOpenGLTexture(QImage("Textures/periodic_texture_01.jpg").mirrored());
            texture[1]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[1]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[2]=new QOpenGLTexture(QImage("Textures/periodic_texture_02.jpg").mirrored());
            texture[2]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[2]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[3]=new QOpenGLTexture(QImage("Textures/periodic_texture_03.jpg").mirrored());
            texture[3]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[3]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[4]=new QOpenGLTexture(QImage("Textures/periodic_texture_04.jpg").mirrored());
            texture[4]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[4]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[5]=new QOpenGLTexture(QImage("Textures/periodic_texture_05.jpg").mirrored());
            texture[5]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[5]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[6]=new QOpenGLTexture(QImage("Textures/periodic_texture_06.jpg").mirrored());
            texture[6]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[6]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[7]=new QOpenGLTexture(QImage("Textures/periodic_texture_07.jpg").mirrored());
            texture[7]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[7]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[8]=new QOpenGLTexture(QImage("Textures/periodic_texture_08.jpg").mirrored());
            texture[8]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[8]->setMagnificationFilter(QOpenGLTexture::Linear);

            texture[9]=new QOpenGLTexture(QImage("Textures/periodic_texture_09.jpg").mirrored());
            texture[9]->setMinificationFilter(QOpenGLTexture::LinearMipMapLinear);
            texture[9]->setMagnificationFilter(QOpenGLTexture::Linear);

        }
        catch (Exception &e)
        {

            cout << e << endl;
            return false;
        }


        return true;

    }






    void GLWidget::paintGL()
    {
        // clears the color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // stores/duplicates the original model view matrix
        glPushMatrix();

        // applying transformations
        glRotatef(_angle_x, 1.0, 0.0, 0.0);
        glRotatef(_angle_y, 0.0, 1.0, 0.0);
        glRotatef(_angle_z, 0.0, 0.0, 1.0);
        glTranslated(_trans_x, _trans_y, _trans_z);
        glScaled(_zoom, _zoom, _zoom);

         switch (_homework_id){
            case 1:
             _renderSelectedParametricCurve();
             break;
           case 2:

                _drawCastle();
             break;
           case 3:

              _renderSelectedParametricSurface();

             break;
           case 4:
              _renderCyclicCurve();
                break;
           case 5:
               _renderHermiteArc();
                break;
           case 6:
             _renderHermitePatch();
                break;
           case 7:

             if (_selectedShader==1) {
                         _shaders[1].Enable();
                         _shaders[1].SetUniformVariable1f("scale_factor", _scale_factor);
                         _shaders[1].SetUniformVariable1f("smoothing", _smoothing);
                         _shaders[1].SetUniformVariable1f("shading", _shading);
                         _shaders[1].Disable();
             }
             if(_selectedShader==2){
               _shaders[2].Enable();
               _shaders[2].SetUniformVariable4f(
                 "default_outline_color", _rgb1, _rgb2, _rgb3, _rgb4);
               _shaders[2].Disable();
             }

             glPushMatrix();
             _shaders[_selectedShader].Enable();
             MatFBSilver.Apply();
             _mouse.Render();   
             _shaders[_selectedShader].Disable();
             glPopMatrix();



             break;
            default:

             glColor3f(1.0f, 1.0f, 1.0f);
             glBegin(GL_LINES);
                 glVertex3f(0.0f, 0.0f, 0.0f);
                 glVertex3f(1.1f, 0.0f, 0.0f);

                 glVertex3f(0.0f, 0.0f, 0.0f);
                 glVertex3f(0.0f, 1.1f, 0.0f);

                 glVertex3f(0.0f, 0.0f, 0.0f);
                 glVertex3f(0.0f, 0.0f, 1.1f);
             glEnd();

             glBegin(GL_TRIANGLES);
                 // attributes
                 glColor3f(1.0f, 0.0f, 0.0f);
                 // associated with position
                 glVertex3f(1.0f, 0.0f, 0.0f);

                 // attributes
                 glColor3f(0.0, 1.0, 0.0);
                 // associated with position
                 glVertex3f(0.0, 1.0, 0.0);

                 // attributes
                 glColor3f(0.0f, 0.0f, 1.0f);
                 // associated with position
                 glVertex3f(0.0f, 0.0f, 1.0f);
             glEnd();
             update();

         }


        // pops the current matrix stack, replacing the current matrix with the one below it on the stack,
        // i.e., the original model view matrix is restored
        glPopMatrix();
        glDepthMask(GL_TRUE);
        glDisable(GL_BLEND);
    }


    //----------------------------------------------------------------------------
    // when the main window is resized one needs to redefine the projection matrix
    //----------------------------------------------------------------------------
    void GLWidget::resizeGL(int w, int h)
    {
        // setting the new size of the rendering context
        glViewport(0, 0, w, h);

        // redefining the projection matrix
        glMatrixMode(GL_PROJECTION);

        glLoadIdentity();

        _aspect = static_cast<double>(w) / static_cast<double>(h);

        gluPerspective(_fovy, _aspect, _z_near, _z_far);

        // switching back to the model view matrix
        glMatrixMode(GL_MODELVIEW);

        update();
    }

    //-----------------------------------
    // implementation of the public slots
    //-----------------------------------

    void GLWidget::_animate(){
        GLfloat *vertex = _mouse.MapVertexBuffer(GL_READ_WRITE);
          GLfloat *normal = _mouse.MapNormalBuffer(GL_READ_ONLY);

          _angle += DEG_TO_RADIAN;
          if(_angle >= TWO_PI) _angle -= TWO_PI;

          GLfloat scale = sin(_angle) / 3000.0;
          for(GLuint i = 0; i < _mouse.VertexCount(); ++i)
          {
              for (GLuint coordinate = 0; coordinate < 3; ++coordinate, ++vertex, ++normal)
                  *vertex += scale * (*normal);
          }

          _mouse.UnmapVertexBuffer();
          _mouse.UnmapNormalBuffer();

          update();
    }

    void GLWidget::_animateStar(){
        GLfloat *vertex = _star.MapVertexBuffer(GL_READ_WRITE);
          GLfloat *normal = _star.MapNormalBuffer(GL_READ_ONLY);

          _angle2 += DEG_TO_RADIAN;
          if(_angle2 >= TWO_PI) _angle2 -= TWO_PI;

          GLfloat scale = sin(_angle2) / 3000.0;
          for(GLuint i = 0; i < _star.VertexCount(); ++i)
          {
              for (GLuint coordinate = 0; coordinate < 3; ++coordinate, ++vertex, ++normal)
                  *vertex += scale * (*normal);
          }

          _star.UnmapVertexBuffer();
          _star.UnmapNormalBuffer();

          update();
    }


    void GLWidget::_drawCastle(){
        if(_dl){
            _dl->Enable();
            _drawWalls();
            _drawDor();
            _drawTower();
            _drawFigures();
            _dl->Disable();
        }

    }


    void GLWidget::_drawFigures(){
        glPushMatrix();
        MatFBRuby.Apply();
        glTranslated(-8,11,9);
        glRotatef(-90, 1.0, 0.0, 0.0);
        glRotatef(30, 0.0, 0.0, 1.0);
        glScaled(40, 40, 40);
        _dragon.Render();
        glPopMatrix();


        glPushMatrix();
        glTranslated(20,27,0);
        glRotatef(-90, 1.0, 0.0, 0.0);
        glRotatef(-40, 0.0, 1.0, 0.0);
          glRotatef(60, 0.0, 1.0, 0.0);
        glScaled(20, 20, 20);
        _dragon.Render();
        glPopMatrix();


       glPushMatrix();
       MatFBEmerald.Apply();
       glTranslated(6,-22,10);
       glRotatef(-60, 1.0, 0.0, 0.0);
       glScaled(51, 51, 51);
       _spot.Render();
        glPopMatrix();


        glPushMatrix();
        MatFBPearl.Apply();
        glTranslated(8,2,20);
         glScaled(4, 4, 4);
        _animate();
        _mouse.Render();
        glPopMatrix();

        _animateStar();
        glPushMatrix();
        MatFBGold.Apply();
        glTranslated(20,30,-15);
        glScaled(20, 20, 20);
        _star.Render();
        glPopMatrix();

        glPushMatrix();
        glTranslated(-20,20,-15);
        glScaled(10, 10, 10);
        _star.Render();
        glPopMatrix();

        glPushMatrix();
        glTranslated(-20,-10,30);
        glScaled(15, 15, 15);
        _star.Render();
        glPopMatrix();

        glPushMatrix();
        glTranslated(50,-10,15);
        glScaled(25, 25, 25);
        _star.Render();
        glPopMatrix();


    }
    void GLWidget::_drawTower(){
        MatFBSilver.Apply();
        glPushMatrix();
        glTranslated(-4, 0,4);
        glRotatef(90, 0.0, 1.0, 0.0);
        for(GLuint k=1;k<=13;k++){
        for(GLuint i=1;i<8;i++){
                glTranslated(_trans_x+1.0, _trans_y, _trans_z);
               _cube.Render();
        }
        glTranslated(_trans_x, _trans_y+1.0, _trans_z);
         _cube.Render();
        for(GLuint i=1;i<7;i++){
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
               _cube.Render();
        }
         glTranslated(_trans_x-1.0, _trans_y, _trans_z);
        }


        glPopMatrix();


        glPushMatrix();
        glTranslated(-4, 0,-4);

        for(GLuint k=1;k<=13;k++){
        for(GLuint i=1;i<8;i++){
                glTranslated(_trans_x+1.0, _trans_y, _trans_z);
               _cube.Render();
        }
        glTranslated(_trans_x, _trans_y+1.0, _trans_z);
         _cube.Render();
        for(GLuint i=1;i<7;i++){
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
               _cube.Render();
        }
         glTranslated(_trans_x-1.0, _trans_y, _trans_z);
        }


        glPopMatrix();


        glPushMatrix();
        glTranslated(-4, 0,3);
        for(GLuint k=1;k<=13;k++){
        for(GLuint i=1;i<4;i++){
                glTranslated(_trans_x+1.0, _trans_y, _trans_z);
               _cube.Render();
        }
        glTranslated(_trans_x, _trans_y+1.0, _trans_z);
         _cube.Render();
        for(GLuint i=1;i<3;i++){
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
               _cube.Render();
        }
         glTranslated(_trans_x-1.0, _trans_y, _trans_z);
        }


        glPopMatrix();


        glPushMatrix();
        glTranslated(4, 0,1);
        glRotatef(90, 0.0, 1.0, 0.0);

        for(GLuint k=1;k<=13;k++){
        for(GLuint i=1;i<5;i++){
                glTranslated(_trans_x+1.0, _trans_y, _trans_z);
               _cube.Render();
        }
        glTranslated(_trans_x, _trans_y+1.0, _trans_z);
         _cube.Render();
        for(GLuint i=1;i<4;i++){
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
               _cube.Render();
        }
         glTranslated(_trans_x-1.0, _trans_y, _trans_z);
        }


        glPopMatrix();

        glPushMatrix();
        glTranslated(-1, 0,0);
        for(GLuint k=1;k<=13;k++){
        for(GLuint i=1;i<5;i++){
                glTranslated(_trans_x+1.0, _trans_y, _trans_z);
               _cube.Render();
        }
        glTranslated(_trans_x, _trans_y+1.0, _trans_z);
         _cube.Render();
        for(GLuint i=1;i<4;i++){
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
               _cube.Render();
        }
         glTranslated(_trans_x-1.0, _trans_y, _trans_z);
        }

       glPopMatrix();

        glPushMatrix();
        glTranslated(-1, 0,3);
        glRotatef(90, 0.0, 1.0, 0.0);
        for(GLuint k=1;k<=13;k++){
        for(GLuint i=1;i<3;i++){
                glTranslated(_trans_x+1.0, _trans_y, _trans_z);
               _cube.Render();
        }
        glTranslated(_trans_x, _trans_y+1.0, _trans_z);
         _cube.Render();
        for(GLuint i=1;i<2;i++){
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
               _cube.Render();
        }
         glTranslated(_trans_x-1.0, _trans_y, _trans_z);

        }
        glPopMatrix();

         glPushMatrix();
         MatFBTurquoise.Apply();
         glTranslated(0,20,0);
         glRotatef(-90, 1.0, 0.0, 0.0);
         glScaled(13, 13, 13);
         _cone.Render();
         glPopMatrix();


         MatFBSilver.Apply();
         glPushMatrix();
         glTranslated(10, 0,10);
         glRotatef(90, 0.0, 1.0, 0.0);
         for(GLuint k=1;k<=30;k++){
         for(GLuint i=1;i<4;i++){
                 glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                _cube.Render();
         }
         glTranslated(_trans_x, _trans_y+1.0, _trans_z);
          _cube.Render();
         for(GLuint i=1;i<3;i++){
              glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                _cube.Render();
         }
          glTranslated(_trans_x-1.0, _trans_y, _trans_z);

         }
         glPopMatrix();

         glPushMatrix();
         glTranslated(11, 0,10);
         glRotatef(90, 0.0, 1.0, 0.0);
         for(GLuint k=1;k<=30;k++){
         for(GLuint i=1;i<4;i++){
                 glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                _cube.Render();
         }
         glTranslated(_trans_x, _trans_y+1.0, _trans_z);
          _cube.Render();
         for(GLuint i=1;i<3;i++){
              glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                _cube.Render();
         }
          glTranslated(_trans_x-1.0, _trans_y, _trans_z);

         }
         glPopMatrix();

         glPushMatrix();
         glTranslated(12, 0,10);
         glRotatef(90, 0.0, 1.0, 0.0);
         for(GLuint k=1;k<=30;k++){
         for(GLuint i=1;i<4;i++){
                 glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                _cube.Render();
         }
         glTranslated(_trans_x, _trans_y+1.0, _trans_z);
          _cube.Render();
         for(GLuint i=1;i<3;i++){
              glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                _cube.Render();
         }
          glTranslated(_trans_x-1.0, _trans_y, _trans_z);

         }
         glPopMatrix();

         glPushMatrix();
         MatFBTurquoise.Apply();
         glTranslated(11,33,8);
         glRotatef(-90, 1.0, 0.0, 0.0);
         glScaled(5, 5, 5);
         _cone.Render();
         glPopMatrix();


    }
    void GLWidget::_drawDor(){


            MatFBSilver.Apply();
            glPushMatrix();
            glTranslated(5, 0,17.0);
            glRotatef(-90, 0.0, 1.0, 0.0);
            for(GLuint k=1;k<=4;k++){
            for(GLuint i=1;i<4;i++){
                    glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                   _cube.Render();
            }
            glTranslated(_trans_x, _trans_y+1.0, _trans_z);
             _cube.Render();
            for(GLuint i=1;i<3;i++){
                 glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                   _cube.Render();
            }
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
            }

            glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
            for(GLuint i=1;i<3;i++){
                    glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                   _cube.Render();
            }
            glPopMatrix();


            glPushMatrix();
            glTranslated(11, 0,17.0);
            glRotatef(-90, 0.0, 1.0, 0.0);
            for(GLuint k=1;k<=4;k++){
            for(GLuint i=1;i<4;i++){
                    glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                   _cube.Render();
            }
            glTranslated(_trans_x, _trans_y+1.0, _trans_z);
             _cube.Render();
            for(GLuint i=1;i<3;i++){
                 glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                   _cube.Render();
            }
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
            }

            glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
            for(GLuint i=1;i<3;i++){
                    glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                   _cube.Render();
            }
            glPopMatrix();


            glPushMatrix();
            glTranslated(5, 0,19.0);
            for(GLuint k=1;k<=5;k++){
            for(GLuint i=1;i<6;i++){
                    glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                   _cube.Render();
            }
            glTranslated(_trans_x, _trans_y+1.0, _trans_z);
             _cube.Render();
            for(GLuint i=1;i<5;i++){
                 glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                   _cube.Render();
            }
             glTranslated(_trans_x-1.0, _trans_y, _trans_z);
            }

            glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
            for(GLuint i=1;i<4;i++){
                    glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                   _cube.Render();
            }

            glPopMatrix();


    }

    void GLWidget::_drawWalls(){

           MatFBSilver.Apply();

           glPushMatrix();
           glTranslated(_trans_x-1.0, _trans_y, _trans_z+1.0);
           for(GLuint k=1;k<=5;k++){
           for(GLuint i=1;i<20;i++){
                   glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
           glTranslated(_trans_x, _trans_y+1.0, _trans_z);
            _cube.Render();
           for(GLuint i=1;i<19;i++){
                glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
            glTranslated(_trans_x-1.0, _trans_y, _trans_z);
           }

           glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
           for(GLuint i=1;i<11;i++){
                   glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                  _cube.Render();
           }

           glPopMatrix();

           glPushMatrix();
           glRotatef(-90, 0.0, 1.0, 0.0);
           for(GLuint k=1;k<=5;k++){
           for(GLuint i=1;i<10;i++){
                   glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
           glTranslated(_trans_x, _trans_y+1.0, _trans_z);
            _cube.Render();
           for(GLuint i=1;i<9;i++){
                glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
            glTranslated(_trans_x-1.0, _trans_y, _trans_z);
           }
           glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
           for(GLuint i=1;i<6;i++){
                   glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                  _cube.Render();
           }

           glPopMatrix();

           glPushMatrix();
           glRotatef(-90, 0.0, 1.0, 0.0);
           glTranslated(0,0,-18);
           for(GLuint k=1;k<=5;k++){
           for(GLuint i=1;i<18;i++){
                   glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
           glTranslated(_trans_x, _trans_y+1.0, _trans_z);
            _cube.Render();
           for(GLuint i=1;i<17;i++){
                glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
            glTranslated(_trans_x-1.0, _trans_y, _trans_z);
           }
           glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
           for(GLuint i=1;i<10;i++){
                   glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                  _cube.Render();
           }

           glPopMatrix();


           glPushMatrix();
           glTranslated(-9,0,9);
           for(GLuint k=1;k<=5;k++){
           for(GLuint i=1;i<10;i++){
                   glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
           glTranslated(_trans_x, _trans_y+1.0, _trans_z);
            _cube.Render();
           for(GLuint i=1;i<9;i++){
                glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
            glTranslated(_trans_x-1.0, _trans_y, _trans_z);
           }
           glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
           for(GLuint i=1;i<6;i++){
                   glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                  _cube.Render();
           }

           glPopMatrix();



           glPushMatrix();
           glTranslated(-8,0,8);
           glRotatef(-90, 0.0, 1.0, 0.0);
           for(GLuint k=1;k<=5;k++){
           for(GLuint i=1;i<10;i++){
                   glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
           glTranslated(_trans_x, _trans_y+1.0, _trans_z);
            _cube.Render();
           for(GLuint i=1;i<9;i++){
                glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
            glTranslated(_trans_x-1.0, _trans_y, _trans_z);
           }
           glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
           for(GLuint i=1;i<6;i++){
                   glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                  _cube.Render();
           }
           glPopMatrix();


           glPushMatrix();
           glTranslated(-9.0, _trans_y,17.0);
           for(GLuint k=1;k<=5;k++){
           for(GLuint i=1;i<27;i++){
                   glTranslated(_trans_x+1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
           glTranslated(_trans_x, _trans_y+1.0, _trans_z);
            _cube.Render();
           for(GLuint i=1;i<26;i++){
                glTranslated(_trans_x-1.0, _trans_y, _trans_z);
                  _cube.Render();
           }
            glTranslated(_trans_x-1.0, _trans_y, _trans_z);
           }

           glTranslated(_trans_x-1.0, _trans_y+1.0, _trans_z);
           for(GLuint i=1;i<14;i++){
                   glTranslated(_trans_x+2.0, _trans_y, _trans_z);
                  _cube.Render();
           }

            glPopMatrix();

    }

    void GLWidget::set_angle_x(int value)
    {
        if (_angle_x != value)
        {
            _angle_x = value;
            update();
        }
    }

    void GLWidget::set_angle_y(int value)
    {
        if (_angle_y != value)
        {
            _angle_y = value;
            update();
        }
    }

    void GLWidget::set_angle_z(int value)
    {
        if (_angle_z != value)
        {
            _angle_z = value;
            update();
        }
    }

    void GLWidget::set_zoom_factor(double value)
    {
        if (_zoom != value)
        {
            _zoom = value;
            update();
        }
    }

    void GLWidget::set_trans_x(double value)
    {
        if (_trans_x != value)
        {
            _trans_x = value;
            update();
        }
    }

    void GLWidget::set_trans_y(double value)
    {
        if (_trans_y != value)
        {
            _trans_y = value;
            update();
        }
    }

    void GLWidget::set_trans_z(double value)
    {
        if (_trans_z != value)
        {
            _trans_z = value;
            update();
        }
    }

    void GLWidget::setParametricCurveIndex(int index){
        this->_selected_pc=index;
    }

    void GLWidget::setSurfaceIndex(int index){
        this->_selected_ps=index;
    }

    void GLWidget::setTextureType(int index){
        this->_selected_texture=index;

    }

    void GLWidget::setVisibilityOfTangents(int visibility){
        if(visibility==Qt::Checked)
            this->_show_tangents=true;
        else
            this->_show_tangents=false;
    }

    void GLWidget::textureEnabled(int tex){
        if(tex==Qt::Checked){
            this->_texture_enabled=true;
        }
        else{
            this->_texture_enabled=false;
        }
    }

    void GLWidget::setVisibilityOfAccelerationVectors(int visibility){
        if(visibility==Qt::Checked)
            this->_show_acceleration_vectors=true;
        else
            this->_show_acceleration_vectors=false;
    }

    void GLWidget::set_hp3_vectors_visibility(int v){
        if(v==Qt::Checked){
           _show_vectors=true;

        }
        else{
            _show_vectors=false;
        }

      update();

    }

    void GLWidget::cyclicInterpolEnabled(int value){
        if(value==Qt::Checked){
            this->_show_interpol=true;
            this->_show_hermite_interpol=true;
        }
        else {
            this->_show_interpol=false;
            this->_show_hermite_interpol=false;
        }
        if (_homework_id==4) updateCyclicCurve();
        else updateHermiteArc();
    }

    void GLWidget::cyclicD1Enabled(int value){
        if(value==Qt::Checked){
            this->_show_cyclic_d1=true;
            this->_show_cyclic_hermite_d1=true;
        }
        else{
            this->_show_cyclic_d1=false;
            this->_show_cyclic_hermite_d1=false;
        }
        update();
    }

    void GLWidget::cyclicD2Enabled(int value){
        if(value==Qt::Checked){
            this->_show_cyclic_d2=true;
            this->_show_cyclic_hermite_d2=true;
        }
        else{
            this->_show_cyclic_d2=false;
            this->_show_cyclic_hermite_d2=false;
        }
        update();
    }





   void GLWidget::setPointindex(double value){
         if(int(value) <= int(2*_n)){
             _point_ind=int(value);

             emit xPointChanged((*_cyc3)[_point_ind][0]);
             emit yPointChanged((*_cyc3)[_point_ind][1]);
             emit zPointChanged((*_cyc3)[_point_ind][2]);

        }
    }

   void GLWidget::setPointx(double value){
       (*_cyc3)[_point_ind][0] = value;
       updateCyclicCurve();
   }

   void GLWidget::setPointy(double value){
       (*_cyc3)[_point_ind][1] = value;
       updateCyclicCurve();
   }

   void GLWidget::setPointz(double value){
       (*_cyc3)[_point_ind][2] = value;
       updateCyclicCurve();
   }


   void GLWidget::set_hp3_x(double v){
       switch (_hp3_index){
       case 0:
           (*_hp3)(0, 0)[0]=v;

           break;
       case 1:
           (*_hp3)(0, 1)[0]=v;

           break;
       case 2:
           (*_hp3)(1, 0)[0]=v;

            break;
       default:
           (*_hp3)(1, 1)[0]=v;

           break;
       }
       updateHermitePatch();
   }
   void GLWidget::set_hp3_y(double v){
       switch (_hp3_index){
       case 0:
           (*_hp3)(0, 0)[1]=v;

           break;
       case 1:
           (*_hp3)(0, 1)[1]=v;

           break;
       case 2:
           (*_hp3)(1, 0)[1]=v;

            break;
       default:
           (*_hp3)(1, 1)[1]=v;
           break;
       }
       updateHermitePatch();
   }
   void GLWidget::set_hp3_z(double v){
       switch (_hp3_index){
       case 0:
           (*_hp3)(0, 0)[2]=v;
           break;
       case 1:
           (*_hp3)(0, 1)[2]=v;
           break;
       case 2:
           (*_hp3)(1, 0)[2]=v;
            break;
       default:
           (*_hp3)(1, 1)[2]=v;
           break;
       }
       updateHermitePatch();
   }


   bool GLWidget::updateHermitePatch(){
       _hp3->UpdateVertexBufferObjectsOfData();
       update();
       return true;
   }

   void GLWidget::setHermitePointindex(double value){
//       vector_length=std::sqrt(
//             ((*_ch3)[_hermite_point_ind+2][0]- (*_ch3)[_hermite_point_ind][0])*((*_ch3)[_hermite_point_ind+2][0]- (*_ch3)[_hermite_point_ind][0])+
//             ((*_ch3)[_hermite_point_ind+2][1]- (*_ch3)[_hermite_point_ind][1])*((*_ch3)[_hermite_point_ind+2][1]- (*_ch3)[_hermite_point_ind][1])+
//             ((*_ch3)[_hermite_point_ind+2][2]- (*_ch3)[_hermite_point_ind][2])*((*_ch3)[_hermite_point_ind+2][2]- (*_ch3)[_hermite_point_ind][2])
//               );
//       emit vectorLengthChanged(vector_length/starting_length);
         if(int(value) < 4){
             _hermite_point_ind=int(value);

             DCoordinate3& cp = (*_ch3)[_hermite_point_ind];
             emit xHermitePointChanged(cp[0]);
             emit yHermitePointChanged(cp[1]);
             emit zHermitePointChanged(cp[2]);
        update();
        }
    }

   void GLWidget::shaderIndexChanged(int value){
       if(int(value)<4){
           _selectedShader=value;
       }
       cout<<_selectedShader;
       update();
   }

   void GLWidget::setHermitePointx(double value){
       (*_ch3)[_hermite_point_ind+2][0] = value;
       updateHermiteArc();
   }

   void GLWidget::setHermitePointy(double value){
       (*_ch3)[_hermite_point_ind+2][1] = value;
       updateHermiteArc();
   }

   void GLWidget::setHermitePointz(double value){
       (*_ch3)[_hermite_point_ind+2][2] = value;
       updateHermiteArc();
   }





    bool GLWidget::updateCyclicCurve(){
           GLenum usage_flag=GL_STATIC_DRAW;
           _knot_vector.ResizeRows(2 * _n + 1);

           _cyc3->UpdateVertexBufferObjectsOfData();
           CyclicCurve3 temp=*_cyc3;
           temp.UpdateVertexBufferObjectsOfData();

            GLdouble pointValue = 0;

             GLdouble step = TWO_PI / (2 * _n + 1);
            for (GLuint i = 0; i < 2 * _n + 1; i++) {
              _knot_vector(i) = pointValue;
              pointValue += step;
            }

           ColumnMatrix<DCoordinate3> _dataPointsToInterpolate(2 * _n + 1);


           for (GLuint i = 0; i <= 2 * _n; i++) {
             _dataPointsToInterpolate(i) = temp[i];
           }



           temp.UpdateDataForInterpolation(_knot_vector, _dataPointsToInterpolate);
            if(_image_of_cyc3_interpol){

                 _image_of_cyc3_interpol->DeleteVertexBufferObjects();
            }

           _image_of_cyc3_interpol = temp.GenerateImage(2, 100);
           _image_of_cyc3_interpol->UpdateVertexBufferObjects(1,usage_flag);

          _image_of_cyc3->DeleteVertexBufferObjects();
          _image_of_cyc3 = _cyc3->GenerateImage(2, 100);
          _image_of_cyc3->UpdateVertexBufferObjects(1,usage_flag);


        update();

        return true;
    }


    bool GLWidget::updateHermiteArc(){
        GLenum usage_flag=GL_STATIC_DRAW;
        _knot_vector.ResizeRows(4);

        _ch3->UpdateVertexBufferObjectsOfData();
        CubicHermiteArc3 temp=*_ch3;
        temp.UpdateVertexBufferObjectsOfData();

         GLdouble pointValue = 0;

          GLdouble step = TWO_PI / (4);
         for (GLuint i = 0; i < 4; i++) {
           _knot_vector(i) = pointValue;
           pointValue += step;
         }

        ColumnMatrix<DCoordinate3> _dataPointsToInterpolate(4);


        for (GLuint i = 0; i < 4; i++) {
          _dataPointsToInterpolate(i) = temp[i];
        }


        temp.UpdateDataForInterpolation(_knot_vector, _dataPointsToInterpolate);
         if(_image_of_ch3_interpol){

              _image_of_ch3_interpol->DeleteVertexBufferObjects();
         }

        _image_of_ch3_interpol = temp.GenerateImage(2, 100);
        _image_of_ch3_interpol->UpdateVertexBufferObjects(1,usage_flag);

       _image_of_ch3->DeleteVertexBufferObjects();
       _image_of_ch3 = _ch3->GenerateImage(2, 100);
       _image_of_ch3->UpdateVertexBufferObjects(1,usage_flag);


     update();

     return true;
    }





    bool GLWidget::_createAllParametricCurvesAndTheirImages(){
        _pc.ResizeColumns(_pc_count);
        RowMatrix<ParametricCurve3::Derivative> derivative(3);
        derivative[0]=spiral_on_cone::d0;
        derivative[1]=spiral_on_cone::d1;
        derivative[2]=spiral_on_cone::d2;

        _pc[0]=nullptr;
        _pc[0]=new (nothrow) ParametricCurve3(derivative,spiral_on_cone::u_min,spiral_on_cone::u_max);

        if(!_pc[0]){
             throw Exception("Couldn't build the Parametric Curve");
        }

        derivative[0]=torus::d0;
        derivative[1]=torus::d1;
        _pc[1]=nullptr;
        _pc[1]=new (nothrow) ParametricCurve3(derivative,torus::u_min,torus::u_max);

        derivative[0]=lissajou::d0;
        derivative[1]=lissajou::d1;
        derivative[2]=lissajou::d2;
        _pc[2]=nullptr;
        _pc[2]=new (nothrow) ParametricCurve3(derivative,lissajou::u_min,lissajou::u_max);


        derivative[0]=hypo::d0;
        derivative[1]=hypo::d1;
        derivative[2]=hypo::d2;
        _pc[3]=nullptr;
        _pc[3]=new (nothrow) ParametricCurve3(derivative,hypo::u_min,hypo::u_max);

        derivative[0]=cyclo::d0;
        derivative[1]=cyclo::d1;
        derivative[2]=cyclo::d2;
        _pc[4]=nullptr;
        _pc[4]=new (nothrow) ParametricCurve3(derivative,cyclo::u_min,cyclo::u_max);


        derivative[0]=ellipse::d0;
        derivative[1]=ellipse::d1;
        derivative[2]=ellipse::d2;
        _pc[5]=nullptr;
        _pc[5]=new (nothrow) ParametricCurve3(derivative,ellipse::u_min,ellipse::u_max);


        derivative[0]=helix::d0;
        _pc[6]=nullptr;
        _pc[6]=new (nothrow) ParametricCurve3(derivative,helix::u_min,helix::u_max);


        _image_of_pc.ResizeColumns(_pc_count);
        GLuint div_point_count=200;
        GLenum usage_flag=GL_STATIC_DRAW;


        for (GLuint i = 0; i < _pc_count; i++){
               _image_of_pc[i]=nullptr;
               _image_of_pc[i]=_pc[i]->GenerateImage(div_point_count,usage_flag);

               if (!_image_of_pc[i] || !_image_of_pc[i]->UpdateVertexBufferObjects(_scale,usage_flag))
               {
                   _destroyAllExistingParametricCurvesAndTheirImages();
                   return false;
               }
           }

       return true;
   }


    bool GLWidget::_createAllParametricSurfaceAndTheirImages(){
      _ps.ResizeColumns(_ps_count);

      TriangularMatrix<ParametricSurface3::PartialDerivative> derivative(3);
          derivative(0, 0) = firstSurface::d00;
          derivative(1, 0) = firstSurface::d10;
          derivative(1, 1) = firstSurface::d01;
          _ps[0] = new ParametricSurface3(derivative,
                                       firstSurface::u_min,
                                       firstSurface::u_max,
                                       firstSurface::v_min,
                                       firstSurface::v_max);

          derivative(0, 0) = secondSurface::d00;
          derivative(1, 0) = secondSurface::d10;
          derivative(1, 1) = secondSurface::d01;
          _ps[1] = new ParametricSurface3(derivative,
                                       secondSurface::u_min,
                                       secondSurface::u_max,
                                       secondSurface::v_min,
                                       secondSurface::v_max);


          derivative(0, 0) = thirdSurface::d00;
          derivative(1, 0) = thirdSurface::d10;
          derivative(1, 1) = thirdSurface::d01;
          _ps[2] = new ParametricSurface3(derivative,
                                       thirdSurface::u_min,
                                       thirdSurface::u_max,
                                       thirdSurface::v_min,
                                       thirdSurface::v_max);

          derivative(0, 0) = forthSurface::d00;
          derivative(1, 0) = forthSurface::d10;
          derivative(1, 1) = forthSurface::d01;
          _ps[3] = new ParametricSurface3(derivative,
                                       forthSurface::u_min,
                                       forthSurface::u_max,
                                       forthSurface::v_min,
                                       forthSurface::v_max
                                        );

          derivative(0, 0) = fifthSurface::d00;
          derivative(1, 0) = fifthSurface::d10;
          derivative(1, 1) = fifthSurface::d01;
          _ps[4] = new ParametricSurface3(derivative,
                                       fifthSurface::u_min,
                                       fifthSurface::u_max,
                                       fifthSurface::v_min,
                                       fifthSurface::v_max);


            _image_of_ps.ResizeColumns(_ps_count);
            GLuint div_point_count=200;


            for (GLuint i = 0; i < _ps_count; i++) {
              if (_ps[i]) {
               _image_of_ps[i] = _ps[i]->GenerateImage(
                  div_point_count, div_point_count);
              }
              if (!_image_of_ps[i] || !_image_of_ps[i]->UpdateVertexBufferObjects())
              {
                  _destroyAllExistingParametricCurvesAndTheirImages();
                  return false;
              }
            }
          return true;

    }

    bool GLWidget::_initializeCyclicCurve(){
        GLenum usage_flag=GL_STATIC_DRAW;
        _cyc3=new CyclicCurve3(_n);

        if(!_cyc3){
            return false;
        }

        GLdouble step = TWO_PI / (2 * _n + 1);
          for (GLuint i = 0; i <= 2 * _n; i++) {
            GLdouble u = i * step;
            DCoordinate3& cp = (*_cyc3)[i];
            cp[0] = cos(u);
            cp[1] = sin(u);
            cp[2] = -2.0 + 4.0 * (GLdouble)rand() / RAND_MAX;
          }


          _cyc3->UpdateVertexBufferObjectsOfData();

          _image_of_cyc3 = _cyc3->GenerateImage(2, 100);  



            if (!_image_of_cyc3) {
                return false;
            }
            if (!_image_of_cyc3->UpdateVertexBufferObjects(_scale,usage_flag)) {
              return false;
            }

            DCoordinate3& cp = (*_cyc3)[0];
            emit xPointChanged(cp[0]);
            emit yPointChanged(cp[1]);
            emit zPointChanged(cp[2]);


            return true;

    }


    void GLWidget::set_rotation_angle_x(int value){
        int r=std::sqrt(
              ((*_ch3)[_hermite_point_ind+2][0]- (*_ch3)[_hermite_point_ind][0])*((*_ch3)[_hermite_point_ind+2][0]- (*_ch3)[_hermite_point_ind][0])+
              ((*_ch3)[_hermite_point_ind+2][1]- (*_ch3)[_hermite_point_ind][1])*((*_ch3)[_hermite_point_ind+2][1]- (*_ch3)[_hermite_point_ind][1])+
              ((*_ch3)[_hermite_point_ind+2][2]- (*_ch3)[_hermite_point_ind][2])*((*_ch3)[_hermite_point_ind+2][2]- (*_ch3)[_hermite_point_ind][2])
                );
        (*_ch3)[_hermite_point_ind+2][1] = (*_ch3)[_hermite_point_ind][1]+r*cos(value*DEG_TO_RADIAN);
        (*_ch3)[_hermite_point_ind+2][2] = (*_ch3)[_hermite_point_ind][2]+r*sin(value*DEG_TO_RADIAN);
        updateHermiteArc();
    }
    void GLWidget::set_rotation_angle_y(int value){
        int r=std::sqrt(
              ((*_ch3)[_hermite_point_ind+2][0]- (*_ch3)[_hermite_point_ind][0])*((*_ch3)[_hermite_point_ind+2][0]- (*_ch3)[_hermite_point_ind][0])+
              ((*_ch3)[_hermite_point_ind+2][1]- (*_ch3)[_hermite_point_ind][1])*((*_ch3)[_hermite_point_ind+2][1]- (*_ch3)[_hermite_point_ind][1])+
              ((*_ch3)[_hermite_point_ind+2][2]- (*_ch3)[_hermite_point_ind][2])*((*_ch3)[_hermite_point_ind+2][2]- (*_ch3)[_hermite_point_ind][2])
                );
        (*_ch3)[_hermite_point_ind+2][0] = (*_ch3)[_hermite_point_ind][0]+r*cos(value*DEG_TO_RADIAN);
        (*_ch3)[_hermite_point_ind+2][2] = (*_ch3)[_hermite_point_ind][2]+r*sin(value*DEG_TO_RADIAN);
        updateHermiteArc();

    }
    void GLWidget::set_rotation_angle_z(int value){
        int r=std::sqrt(
              ((*_ch3)[_hermite_point_ind+2][0] - (*_ch3)[_hermite_point_ind][0])*((*_ch3)[_hermite_point_ind+2][0] - (*_ch3)[_hermite_point_ind][0])+
              ((*_ch3)[_hermite_point_ind+2][1] - (*_ch3)[_hermite_point_ind][1])*((*_ch3)[_hermite_point_ind+2][1] - (*_ch3)[_hermite_point_ind][1])+
              ((*_ch3)[_hermite_point_ind+2][2] - (*_ch3)[_hermite_point_ind][2])*((*_ch3)[_hermite_point_ind+2][2] - (*_ch3)[_hermite_point_ind][2])
                );
        (*_ch3)[_hermite_point_ind+2][0] = (*_ch3)[_hermite_point_ind][0]+r*cos(value*DEG_TO_RADIAN);
        (*_ch3)[_hermite_point_ind+2][1] = (*_ch3)[_hermite_point_ind][1]+r*sin(value*DEG_TO_RADIAN);
        updateHermiteArc();

    }

    void GLWidget::set_vector_length(double value){
       // if(value>1){

            cout<<vector_length<<" ";
            (*_ch3)[_hermite_point_ind+2][0]  /= vector_length;
            (*_ch3)[_hermite_point_ind+2][1] /= vector_length;
            (*_ch3)[_hermite_point_ind+2][2] /= vector_length;
            (*_ch3)[_hermite_point_ind+2][0]  *= value;
            (*_ch3)[_hermite_point_ind+2][1]  *= value;
            (*_ch3)[_hermite_point_ind+2][2]  *= value;

//       }
//        else{
//            (*_ch3)[_hermite_point_ind+2][0] *= value;
//            (*_ch3)[_hermite_point_ind+2][1] *= value;
//            (*_ch3)[_hermite_point_ind+2][2] *= value;
//            (*_ch3)[_hermite_point_ind+2][0] /= vector_length;
//            (*_ch3)[_hermite_point_ind+2][1] /= vector_length;
//            (*_ch3)[_hermite_point_ind+2][2] /= vector_length;
//       }
        vector_length=value;
        updateHermiteArc();



    }


    void GLWidget::setRotationAngle(int angle){
         _angle=angle;
    }


    bool GLWidget::_initializeHermiteArc(){
        GLenum usage_flag=GL_STATIC_DRAW;
        _ch3=new CubicHermiteArc3();

        if(!_ch3){
            return false;
        }
        //cp1
        _ch3->SetCorner(0, -1.0, 0.0, 0.0);
        _ch3->SetCorner(1, +1.0, 0.0, 0.0);
        _ch3->SetTangent(0, 0.0, 1.0, 0.0);
        _ch3->SetTangent(1, 0.0, -1.0, 0.0);



          _ch3->UpdateVertexBufferObjectsOfData();

          _image_of_ch3 = _ch3->GenerateImage(2, 100);
           if (!_image_of_ch3) {
                return false;
            }
            if (!_image_of_ch3->UpdateVertexBufferObjects(_scale,usage_flag)) {
              return false;
            }

            starting_length=std::sqrt(
                  ((*_ch3)[_hermite_point_ind+2][0]- (*_ch3)[_hermite_point_ind][0])*((*_ch3)[_hermite_point_ind+2][0]- (*_ch3)[_hermite_point_ind][0])+
                  ((*_ch3)[_hermite_point_ind+2][1]- (*_ch3)[_hermite_point_ind][1])*((*_ch3)[_hermite_point_ind+2][1]- (*_ch3)[_hermite_point_ind][1])+
                  ((*_ch3)[_hermite_point_ind+2][2]- (*_ch3)[_hermite_point_ind][2])*((*_ch3)[_hermite_point_ind+2][2]- (*_ch3)[_hermite_point_ind][2])
                    );

            vector_length=1;
            DCoordinate3& cp = (*_ch3)[2];

            return true;

    }

    bool GLWidget::_initializeHermitePatch(){
        _hp3 = new HermitePatch3();
        _hp3_origin = new HermitePatch3();
           if(!_hp3){
               return false;
           }

           // define the control net of bic. Hermite patch
           _hp3->SetCorner(2,-1,-1,0);
           _hp3->SetCorner(3,1,-1,0);
           _hp3->SetCorner(0,-1,1,0);
           _hp3->SetCorner(1,1,1,0);

           _hp3->SetUvector(2,0,0,2);
           _hp3->SetUvector(3,0,0,-2);
           _hp3->SetUvector(0,0,0,2);
           _hp3->SetUvector(1,0,0,-4);

           _hp3->SetVvector(2,2,0,2);
           _hp3->SetVvector(3,2,0,-2);
           _hp3->SetVvector(0,2,0,2);
           _hp3->SetVvector(1,2,0,-4);

           _hp3->SetTangent(2,1,1,2);
           _hp3->SetTangent(3,1,2,0);
           _hp3->SetTangent(0,2,1,0);
           _hp3->SetTangent(1,2,2,0);
            _hp3->UpdateVertexBufferObjectsOfData();

            _hp3_origin->SetCorner(2,-1,-1,0);
            _hp3_origin->SetCorner(3,1,-1,0);
            _hp3_origin->SetCorner(0,-1,1,0);
            _hp3_origin->SetCorner(1,1,1,0);

            _hp3_origin->SetUvector(2,0,0,1);
            _hp3_origin->SetUvector(3,0,0,1);
            _hp3_origin->SetUvector(0,0,0,1);
            _hp3_origin->SetUvector(1,0,0,1);

            _hp3_origin->SetVvector(2,2,0,0);
            _hp3_origin->SetVvector(3,2,0,0);
            _hp3_origin->SetVvector(0,2,0,0);
            _hp3_origin->SetVvector(1,2,0,0);

            _hp3_origin->SetTangent(2,1,1,2);
            _hp3_origin->SetTangent(3,1,2,0);
            _hp3_origin->SetTangent(0,2,1,0);
            _hp3_origin->SetTangent(1,2,2,0);
            _hp3_origin->UpdateVertexBufferObjectsOfData();

            emit hp3xPointChanged((*_hp3_origin)(0,0)[0]);
            emit hp3yPointChanged((*_hp3_origin)(0,0)[1]);
            emit hp3zPointChanged((*_hp3_origin)(0,0)[2]);

           _u_lines = _hp3->GenerateUIsoparametricLines(15, 2, 30);
           _v_lines = _hp3->GenerateVIsoparametricLines(4, 2, 30);

           for(GLuint i = 0; i < _u_lines->GetColumnCount(); i++)
           {

               if((*_u_lines)[i])
               {
                   (*_u_lines)[i]->UpdateVertexBufferObjects(GL_STATIC_DRAW);
               }
           }

           for(GLuint i = 0; i < _v_lines->GetColumnCount(); i++)
           {
               if((*_v_lines)[i])
               {
                   (*_v_lines)[i]->UpdateVertexBufferObjects(GL_STATIC_DRAW);
               }
           }

           // generate the mesh of the surface patch
           _image_of_hp3_before_interp = _hp3->GenerateImage(30, 30, GL_STATIC_DRAW);

           if(_image_of_hp3_before_interp)
           {
               _image_of_hp3_before_interp->UpdateVertexBufferObjects();
           }

           // define an interpolation problem:
           // 1: create a knot vector in u−direction
           RowMatrix<GLdouble> u_knot_vector(4);
           u_knot_vector(0) = 0.0;
           u_knot_vector(1) = 1.0 / 3.0;
           u_knot_vector(2) = 2.0 / 3.0;
           u_knot_vector(3) = 1.0;

           // 2: create a knot vector in v−direction
           ColumnMatrix<GLdouble> v_knot_vector(4);
           v_knot_vector(0) = 0.0;
           v_knot_vector(1) = 1.0 / 3.0;
           v_knot_vector(2) = 2.0 / 3.0;
           v_knot_vector(3) = 1.0;

           // 3: define a matrix of datapoints, e.g. set them to the original controlpoints
           Matrix<DCoordinate3> data_points_to_interpolate(4, 4);
           for(GLuint row = 0; row < 4; ++row)
           {
               for(GLuint column = 0; column < 4; ++column)
               {
                   _hp3->GetData(row, column, data_points_to_interpolate(row, column));
               }
           }

           // 4: solve the interpolation problem and generate the mesh of the interpolating patch
           if(_hp3->UpdateDataForInterpolation(u_knot_vector, v_knot_vector, data_points_to_interpolate))
           {
               _image_of_hp3_after_interp = _hp3->GenerateImage(30, 30, GL_STATIC_DRAW);

               if(_image_of_hp3_after_interp)
               {
                   _image_of_hp3_after_interp ->UpdateVertexBufferObjects();
               }
           }



           return true;
    }

    void GLWidget::_destroyAllExistingParametricCurvesAndTheirImages()
    {
        for (GLuint i = 0; i < _pc_count; i++)
        {
             if (_pc[i])
             {
                 delete _pc[i]; _pc[i] = nullptr;
             }

             if (_image_of_pc[i])
             {
                 delete _image_of_pc [i]; _image_of_pc[i] = nullptr;
             }
        }
    }

    void GLWidget::_destroyAllExistingParametricSurfaceAndTheirImages()
    {
        for (GLuint i = 0; i < _ps_count; i++)
        {
             if (_ps[i])
             {
                 delete _ps[i]; _ps[i] = nullptr;
             }

             if (_image_of_ps[i])
             {
                 delete _image_of_ps [i]; _image_of_ps[i] = nullptr;
             }
        }
    }

    void GLWidget::setHomeworkID(int id){
        cout<<id;
        this->_homework_id=id;
    }



   void GLWidget::set_Scale(double value){
       if (_scale != value)
       {
           _scale = value;
           _image_of_pc[_selected_pc]->UpdateVertexBufferObjects(_scale,GL_STATIC_DRAW);
       }
    }


   // shader setup
   void GLWidget::loadShaders()
   {



        try {
          if (!_shaders[0].InstallShaders("Shaders/directional_light.vert",
                                          "Shaders/directional_light.frag")) {
            throw Exception("Directional light failed to load.");
          }

          if (!_shaders[1].InstallShaders("Shaders/reflection_lines.vert",
                                          "Shaders/reflection_lines.frag")) {
            throw Exception("Reflection lines failed to load.");
          }
          if (!_shaders[2].InstallShaders("Shaders/toon.vert",
                                          "Shaders/toon.frag")) {
            throw Exception("Toon failed to load.");
          }

          if (!_shaders[3].InstallShaders("Shaders/two_sided_lighting.vert",
                                          "Shaders/two_sided_lighting.frag")) {
            throw Exception("Two sided lighting failed to load.");
          }
        } catch (Exception& e) {
          cerr << e << endl;
        }
   }



   void GLWidget::hermitePatchIndexChanged(int v){
       _hp3_index=v;
       switch (_hp3_index){

       case 0:
           emit hp3xPointChanged((*_hp3_origin)(0,0)[0]);
           emit hp3yPointChanged((*_hp3_origin)(0,0)[1]);
           emit hp3zPointChanged((*_hp3_origin)(0,0)[2]);
           break;
       case 1:
           emit hp3xPointChanged((*_hp3_origin)(0,1)[0]);
           emit hp3yPointChanged((*_hp3_origin)(0,1)[1]);
           emit hp3zPointChanged((*_hp3_origin)(0,1)[2]);

            break;
       case 2:
           emit hp3xPointChanged((*_hp3_origin)(1,0)[0]);
           emit hp3yPointChanged((*_hp3_origin)(1,0)[1]);
           emit hp3zPointChanged((*_hp3_origin)(1,0)[2]);

            break;
       default:
           emit hp3xPointChanged((*_hp3_origin)(1,1)[0]);
           emit hp3yPointChanged((*_hp3_origin)(1,1)[1]);
           emit hp3zPointChanged((*_hp3_origin)(1,1)[2]);
           break;
       }

       update();
   }



   void GLWidget::setScaleFactor(int value){
       _scale_factor=value;
       update();
   }

   void GLWidget::setSmoothing(int value){
       _smoothing=value;
       update();
   }

   void GLWidget::setShading(int value){
        _shading=value;
        update();
   }

   void GLWidget::setRgb1(double v){
    _rgb1=v;
    update();
   }
   void GLWidget::setRgb2(double v){
    _rgb2=v;
    update();
   }
   void GLWidget::setRgb3(double v){
    _rgb3=v;
    update();
   }
   void GLWidget::setRgb4(double v){
    _rgb4=v;
    update();
   }


    GLWidget::~GLWidget(){
        if (_homework_id == 1)
           {
               _destroyAllExistingParametricCurvesAndTheirImages();
           }
        if (_homework_id==2){
            delete _dl;
            _dl=nullptr;
        }
        if(_homework_id==3){
           _destroyAllExistingParametricSurfaceAndTheirImages();
        if(_homework_id==4){
            _cyc3->DeleteVertexBufferObjectsOfData();
            _image_of_cyc3->DeleteVertexBufferObjects();
            _image_of_cyc3_interpol->DeleteVertexBufferObjects();
        }
    }
   }




}
