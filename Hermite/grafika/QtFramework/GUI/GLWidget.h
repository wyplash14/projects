#pragma once

#include <GL/glew.h>
#include <GL/GLU.h>
#include <QOpenGLWidget>
#include <Parametric/ParametricCurves3.h>
#include "Parametric/ParametricSurfaces3.h"
#include <QTimer>
#include "Core/TriangulatedMeshes3.h"
#include "Core/Lights.h"
#include <QOpenGLTexture>
#include <Cyclic/CyclicCurve3.h>
#include "Hermite/CubicHermiteArc3.h"
#include "Hermite/hermitepatch3.h"
#include "Core/ShaderPrograms.h"

namespace cagd
{
    class GLWidget: public QOpenGLWidget
    {
        Q_OBJECT

    private:

        int _homework_id=5;

        // variables defining the projection matrix
        double       _aspect;            // aspect ratio of the rendering window
        double       _fovy;              // field of view in direction y
        double       _z_near, _z_far;    // distance of near and far clipping planes

        // variables defining the model-view matrix
        double       _eye[3], _center[3], _up[3];

        // variables needed by transformations
        int         _angle_x, _angle_y, _angle_z;
        double      _zoom;
        double      _trans_x, _trans_y, _trans_z;
        int _point_ind=0;
        int _hermite_point_ind=0;

        // your other declarations
        RowMatrix<ParametricCurve3*> _pc;
        RowMatrix<ParametricSurface3*> _ps;
        RowMatrix<GenericCurve3*> _image_of_pc;
        RowMatrix<TriangulatedMesh3*> _image_of_ps;
        GLuint _pc_count=7;
        GLuint _ps_count=5;
        int _selected_pc=0;
        int _selected_ps=0;
        int _selected_texture=0;
        bool _show_tangents=false;
        bool _show_acceleration_vectors=false;
        bool _texture_enabled=false;
        double _scale=1.0;
        GLuint _n=2;
        DirectionalLight *_dl=nullptr;

        CyclicCurve3* _cyc3 = nullptr;
        GenericCurve3* _image_of_cyc3;
        GenericCurve3* _image_of_cyc3_interpol=nullptr;
        ColumnMatrix<GLdouble> _knot_vector;
        bool _show_interpol=false;
        bool _show_cyclic_d1=false;
        bool _show_cyclic_d2=false;

        //Hermite Arc
        CubicHermiteArc3* _ch3 = nullptr;
        GenericCurve3* _image_of_ch3;
        GenericCurve3* _image_of_ch3_interpol=nullptr;
        bool _show_hermite_interpol=false;
        bool _show_cyclic_hermite_d1=false;
        bool _show_cyclic_hermite_d2=false;
        double vector_length;
        double starting_length;

        //HermitePatch
        TriangulatedMesh3 *_image_of_hp3_before_interp = nullptr, *_image_of_hp3_after_interp = nullptr;
        HermitePatch3* _hp3=nullptr;
        HermitePatch3* _hp3_origin=nullptr;
        TriangulatedMesh3* _image_of_hp3;
        RowMatrix<GenericCurve3*>* _u_lines;
        RowMatrix<GenericCurve3*>* _v_lines;
        RowMatrix<GLdouble> u_knot_vector;
        RowMatrix<GLdouble> v_knot_vector;
        TriangulatedMesh3* _before_interpolation;
        TriangulatedMesh3* _after_interpolation;
        bool _show_hermite_patch_interpol=false;
        bool _show_hermite_patch_d1=false;
        bool _show_hermite_patch_d2=false;
        bool _show_vectors=false;
        int _hp3_index=0;


        //Shaders
        void loadShaders();
        int _selectedShader=0;
        ShaderProgram _shaders[4];
        int _scale_factor=4;
        int _smoothing=4;
        int _shading=6;
        double _rgb1=1.0f;
        double _rgb2=1.0f;
        double _rgb3=1.0f;
        double _rgb4=1.0f;



        QTimer *_timer;
        GLfloat _angle;
        GLfloat _angle2;

        RowMatrix<QOpenGLTexture*> texture;

        TriangulatedMesh3 _cube;
        TriangulatedMesh3 _cone;
        TriangulatedMesh3 _dragon;
        TriangulatedMesh3 _spot;
        TriangulatedMesh3 _mouse;
        TriangulatedMesh3 _star;



        bool _createAllParametricCurvesAndTheirImages();
        bool _createAllParametricSurfaceAndTheirImages();
        void _destroyAllExistingParametricCurvesAndTheirImages();
        void _destroyAllExistingParametricSurfaceAndTheirImages();
        bool _renderSelectedParametricCurve();
        bool _renderSelectedParametricSurface();
        bool _loadTextures();
        bool _initializeCyclicCurve();
        bool _initializeHermiteArc();
        bool _initializeHermitePatch();
        bool _renderHermitePatch();
        bool _renderHermiteArc();
        bool _renderCyclicCurve();
        bool updateCyclicCurve();
        bool updateHermiteArc();
        bool updateHermitePatch();


    public:
        // special and default constructor
        // the format specifies the properties of the rendering window
        GLWidget(QWidget* parent = 0);

        // redeclared virtual functions
        void initializeGL();
        void paintGL();
        void resizeGL(int w, int h);
        virtual ~GLWidget();

    public slots:
        // public event handling methods/slots
        void set_angle_x(int value);
        void set_angle_y(int value);
        void set_angle_z(int value);

        void set_zoom_factor(double value);

        void set_trans_x(double value);
        void set_trans_y(double value);
        void set_trans_z(double value);
        void setParametricCurveIndex(int index);
        void setSurfaceIndex(int index);
        void setVisibilityOfTangents(int visibility);
        void setVisibilityOfAccelerationVectors(int visibility);
        void textureEnabled(int texture);
        void setTextureType(int);
        void setHomeworkID(int id);
        void set_Scale(double scale);
        void _drawCastle();
        void _drawWalls();
        void _drawDor();
        void _drawTower();
        void _drawFigures();
        void _animate();
        void _animateStar();
        void setPointx(double value);
        void setPointy(double value);
        void setPointz(double value);
        void setPointindex(double index);
        void setHermitePointx(double value);
        void setHermitePointy(double value);
        void setHermitePointz(double value);
        void setHermitePointindex(double index);

        void cyclicInterpolEnabled(int value);
        void cyclicD1Enabled(int value);
        void cyclicD2Enabled(int value);
        void setRotationAngle(int angle);


        void setScaleFactor(int value);
        void setSmoothing(int value);
        void setShading(int value);

        void setRgb1(double v);
        void setRgb2(double v);
        void setRgb3(double v);
        void setRgb4(double v);


        void set_hp3_interpolation_visibility(int v);
        void set_hp3_d1_visibility(int v);
        void set_hp3_d2_visibility(int v);
        void set_hp3_vectors_visibility(int v);


        void set_rotation_angle_x(int value);
        void set_rotation_angle_y(int value);
        void set_rotation_angle_z(int value);

        void set_vector_length(double value);
        void shaderIndexChanged(int value);
        void hermitePatchIndexChanged(int v);

        void set_hp3_x(double v);
        void set_hp3_y(double v);
        void set_hp3_z(double v);

     signals:
        void xPointChanged(double value);
        void yPointChanged(double value);
        void zPointChanged(double value);
        void xHermitePointChanged(double value);
        void yHermitePointChanged(double value);
        void zHermitePointChanged(double value);
        void vectorLengthChanged(double value);
        void hp3xPointChanged(double value);
        void hp3yPointChanged(double value);
        void hp3zPointChanged(double value);


    };
}
